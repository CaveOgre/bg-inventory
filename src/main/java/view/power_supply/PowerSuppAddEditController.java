package view.power_supply;

import view.Check;
import model.PowerSupply;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class PowerSuppAddEditController {

    @FXML
    private TextField powerSupplyField;

    @FXML
    private TextField phoneField;

    @FXML
    private Label powerSupplyLabel;

    @FXML
    private Label phoneLabel;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;

    private DbConnection dbConnection = new DbConnection();

    private static Stage stage;
    private static PowerSupply powerSupply;
    private static boolean isUpdate = false;

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setPowerSupply(PowerSupply outPowerSupply) {
        powerSupply = outPowerSupply;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    private String oldPowerSupply = "";
    private String oldPhone = "";

    @FXML
    private void initialize() {
        if (isUpdate) {
            oldPowerSupply = powerSupply.getPowerSuppName();
            oldPhone = powerSupply.getPhone();
            powerSupplyField.setText(oldPowerSupply);
            phoneField.setText(oldPhone);
        }
    }

    @FXML
    private void okAction() {
        String supply = powerSupplyField.getText().trim();
        String phone = phoneField.getText().trim();
        if (isFieldsRight(supply, phone)) {
            if (isUpdate)
                updatePowerSupply(supply, phone);
            else addPowerSupply(supply, phone);
        } else {
            powerSupplyField.setText(supply);
            phoneField.setText(phone);
        }
    }

    private void addPowerSupply(String supplyToAdd, String phoneToAdd) {
        Connection conn = null;
        PreparedStatement preState = null;

        String query = "INSERT INTO power_supply (powerSupName, phone) VALUES(?, ?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, supplyToAdd);
            preState.setString(2, phoneToAdd);
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при редактировании поставщика электричества.", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }

    }

    private void updatePowerSupply(String supplyToEdit, String phoneToEdit) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "UPDATE power_supply SET powerSupName = ?, phone = ? WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, supplyToEdit);
            preState.setString(2, phoneToEdit);
            preState.setInt(3, powerSupply.getId());
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при редактировании поставщика электричества.", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private boolean isFieldsRight(String supply, String phone) {
        String powSuppLabel = powerSupplyLabel.getText().trim();
        String phoneLabelText = phoneLabel.getText();
        if (!Check.isFieldNull(supply)) {
            if (!oldPowerSupply.equals(supply)) {
                if (!Check.isNewPowerSupp(supply)) {
                    AlertMessage.showSameName(powSuppLabel);
                    return false;
                }
            }
            if (!Check.isFieldNull(phone)) {
                if (!oldPhone.equals(phone)){
                    if (!Check.isNewPhone(phone)){
                        AlertMessage.showSameName(phoneLabelText);
                        return false;
                    }
                }
                return true;
            } else {
                AlertMessage.showEmptyFieldMessage(phoneLabelText);
                return false;
            }

        } else {
            AlertMessage.showEmptyFieldMessage(powSuppLabel);
            return false;
        }
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}
