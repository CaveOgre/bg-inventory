package view.power_supply;

import view.Check;
import mainpackage.Main;
import view.AlertMessage;
import model.PowerSupply;
import dbpackage.DbConnection;
import javafx.collections.FXCollections;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.collections.ObservableList;
import view.Query;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class PowerSupplyController {

    @FXML
    private ListView<PowerSupply> powerSupplyListView;

    @FXML
    private Button addBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button delBtn;

    @FXML
    private Button exitBtn;

    private ObservableList<PowerSupply> powerSupplyList;

    private DbConnection dbConnection = new DbConnection();

    private static Stage stage;

    public static void setStage(Stage outStage) {
        stage = outStage;
    }

    @FXML
    private void initialize() {
        powerSupplyList = FXCollections.observableArrayList();
        loadPowerSupplyList();
        powerSupplyListView.setItems(powerSupplyList);
    }

    private void loadPowerSupplyList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM power_supply";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int id = resSet.getInt("id");
                if (id != 0)
                    powerSupplyList.add(new PowerSupply(id, resSet.getString("powerSupName"),
                            resSet.getString("phone")));
            }
            conn.close();
            resSet.close();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка поставщиков электричества", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    @FXML
    private void goToAddView() {
        int selectedIndex = powerSupplyListView.getSelectionModel().getSelectedIndex();
        String title = "Добавить";

        Stage stage = createNewWindow(title);
        PowerSuppAddEditController.setStage(stage);

        assert stage != null;
        stage.showAndWait();

        initialize();
        powerSupplyListView.getSelectionModel().select(selectedIndex);
    }

    @FXML
    private void goToEditView() {
        if (Check.isSelectedListView(powerSupplyListView)) {
            int selectedIndex = powerSupplyListView.getSelectionModel().getSelectedIndex();
            String title = "Редактировать";

            PowerSupply powerSupplyToEdit = powerSupplyListView.getSelectionModel().getSelectedItem();
            PowerSuppAddEditController.setPowerSupply(powerSupplyToEdit);
            PowerSuppAddEditController.setIsUpdate(true);

            Stage stage = createNewWindow(title);
            PowerSuppAddEditController.setStage(stage);

            assert stage != null;
            stage.showAndWait();
            PowerSuppAddEditController.setIsUpdate(false);

            initialize();
            powerSupplyListView.getSelectionModel().select(selectedIndex);
        } else AlertMessage.showNotSelectedElement();
    }

    @FXML
    private void deletePowerSup() {
        if (Check.isSelectedListView(powerSupplyListView)) {
            int selectedIndex = powerSupplyListView.getSelectionModel().getSelectedIndex();
            PowerSupply powerSupp = powerSupplyListView.getSelectionModel().getSelectedItem();
            Connection conn = null;
            PreparedStatement preState = null;
            final String query = "DELETE FROM power_supply WHERE id = ?";
            String updateQuer = "UPDATE power_point SET powerSupply_id = 0 WHERE powerSupply_id = ?";
            Query.updateDeleteItem(updateQuer, powerSupp.getId());
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setInt(1, powerSupp.getId());
                preState.executeUpdate();
                initialize();
                powerSupplyListView.getSelectionModel().select(Check.selectIndex(selectedIndex));
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при удалении поставщика электричества", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        } else AlertMessage.showNotSelectedElement();
    }

    private Stage createNewWindow(String title) {
        String view = "/PowerSuppAddEdit.fxml";
        String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        PowerSuppAddEditController.setStage(stage);

        return stage;
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}
