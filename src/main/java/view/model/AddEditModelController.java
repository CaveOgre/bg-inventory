package view.model;

import view.Check;
import model.Model;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;


public class AddEditModelController {
    @FXML
    private ComboBox<String> vendorNameBox;

    @FXML
    private Label vendorNameLabel;

    @FXML
    private Label modelNameLabel;

    @FXML
    private TextField modelNameField;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;

    private ObservableList<String> vendorNameList;

    private DbConnection dbConnection = new DbConnection();

    private static Stage stage;
    private static Model deviceModel;
    private static boolean isUpdate;

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setDeviceModel(Model outDeviceModel) {
        deviceModel = outDeviceModel;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    private String oldVendorName = "";
    private String oldModelName = "";

    @FXML
    private void initialize() {
        vendorNameList = FXCollections.observableArrayList();
        loadVendorList();
        vendorNameBox.setItems(vendorNameList);
        if (isUpdate) {
            oldVendorName = findVendorName(deviceModel.getVendorId());
            oldModelName = deviceModel.getModelName();
            vendorNameBox.getSelectionModel().select(oldVendorName);
            modelNameField.setText(oldModelName);
        }
    }

    @FXML
    private void okAction() {
        String vendorName = vendorNameBox.getEditor().getText().trim();
        String modelName = modelNameField.getText().trim();
        if (isFieldsRight(vendorName, modelName)) {
            if (isUpdate) {
                editModel(vendorName, modelName);
                closeWindow();
            } else {
                addModel(vendorName, modelName);
                closeWindow();
            }
        } else {
            vendorNameBox.getEditor().setText(vendorName);
            modelNameField.setText(modelName);
        }
    }

    public void addModel(String vendorName, String modelName) {
        Connection conn = null;
        PreparedStatement preState = null;
        int vendorId = findVendorId(vendorName);
        String query = "INSERT INTO model (vendor_id, modelName) VALUES (?, ?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, vendorId);
            preState.setString(2, modelName);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении модели устройства", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void editModel(String vendorName, String modelName) {
        Connection conn = null;
        PreparedStatement preState = null;
        int modelId = findVendorId(vendorName);
        String query = "UPDATE model SET vendor_id = ?, modelName = ? WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, modelId);
            preState.setString(2, modelName);
            preState.setInt(3, deviceModel.getId());
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при редактировании модели устройства", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void loadVendorList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT vendorName FROM vendor";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                vendorNameList.add(resSet.getString("vendorName"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка производителей устройства", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private String findVendorName(int vendorId) {
        String vendorName = "";
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT vendorName FROM vendor WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, vendorId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                vendorName = resSet.getString("vendorName");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске имени производителя устройства", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return vendorName;
    }

    private int findVendorId(String vendorName) {
        int vendorId = 0;
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT id FROM vendor WHERE vendorName = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, vendorName);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                vendorId = resSet.getInt("id");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске id поставшика электричества", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return vendorId;
    }

    private boolean isFieldsRight(String vendorName, String modelName) {
        if (!Check.isFieldNull(vendorName)) {
            if (!oldVendorName.equals(vendorName)) {
                if (Check.isNewVendorName(vendorName)) {
                    if (AlertMessage.isOkNewValue(vendorNameLabel.getText()))
                        createNewVendor(vendorName);
                    else return false;
                }
            }
            if (!Check.isFieldNull(modelName)) {
                if (!oldModelName.equals(modelName)) {
                    if (!Check.isNewModelName(vendorName, modelName)) {
                        AlertMessage.showSameName(modelNameLabel.getText());
                        return false;
                    }
                }
                return true;
            } else {
                AlertMessage.showEmptyFieldMessage(modelNameLabel.getText());
                return false;
            }
        } else {
            AlertMessage.showEmptyFieldMessage(vendorNameLabel.getText());
            return false;
        }
    }

    private void createNewVendor(String vendorName) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO vendor (vendorName) VALUES (?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, vendorName);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении производителя устройства" +
                    "во время изменения модели", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }

}