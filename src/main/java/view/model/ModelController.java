package view.model;

import view.Check;
import model.Model;
import mainpackage.Main;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import view.Query;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class ModelController {

    @FXML
    private ListView<Model> modelListView;

    @FXML
    private Button addBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button delBtn;

    @FXML
    private Button exitBtn;

    private DbConnection dbConnection = new DbConnection();
    private ObservableList<Model> modelList;

    private static Stage stage;

    public static void setStage(Stage outStage) {
        stage = outStage;
    }

    @FXML
    private void initialize() {
        modelList = FXCollections.observableArrayList();
        loadModelList();

        modelListView.setItems(modelList);
    }

    @FXML
    private void goToAddView() {
        int selectedIndex = modelListView.getSelectionModel().getSelectedIndex();
        String title = "Добавление";

        Stage stage = createNewWindow(title);
        AddEditModelController.setStage(stage);

        assert stage != null;
        stage.showAndWait();

        initialize();
        modelListView.getSelectionModel().select(selectedIndex);
    }

    @FXML
    private void goToEditView() {
        if (Check.isSelectedListView(modelListView)) {
            int selectedIndex = modelListView.getSelectionModel().getSelectedIndex();
            String title = "Редактировать";

            Model deviceModel = modelListView.getSelectionModel().getSelectedItem();
            AddEditModelController.setDeviceModel(deviceModel);
            AddEditModelController.setIsUpdate(true);

            Stage stage = createNewWindow(title);
            AddEditModelController.setStage(stage);

            assert stage != null;
            stage.showAndWait();
            AddEditModelController.setIsUpdate(false);

            initialize();
            modelListView.getSelectionModel().select(selectedIndex);
        } else AlertMessage.showNotSelectedElement();
    }

    @FXML
    private void deleteModel() {
        if (Check.isSelectedListView(modelListView)) {
            int selectedIndex = modelListView.getSelectionModel().getSelectedIndex();
            Model deviceModel = modelListView.getSelectionModel().getSelectedItem();
            Connection conn = null;
            PreparedStatement preState = null;
            String query = "DELETE FROM model WHERE id = ?";
            String updateQuery = "UPDATE device SET model_id = 0 WHERE model_id = ?";
            Query.updateDeleteItem(updateQuery, deviceModel.getId());
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setInt(1, deviceModel.getId());
                preState.executeUpdate();
                initialize();
                modelListView.getSelectionModel().select(Check.selectIndex(selectedIndex));
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при удалении модели устройства", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        } else AlertMessage.showNotSelectedElement();
    }

    private void loadModelList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM model";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int modelId = resSet.getInt("id");
                if (modelId != 0)
                    modelList.add(new Model(modelId, resSet.getInt("vendor_id"),
                            resSet.getString("modelName")));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка типа питания устройства", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private Stage createNewWindow(String title) {
        String view = "/AddEditModel.fxml";
        String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        AddEditModelController.setStage(stage);

        return stage;
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }

}
