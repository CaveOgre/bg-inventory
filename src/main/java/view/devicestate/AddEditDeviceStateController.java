package view.devicestate;

import view.Check;
import view.AlertMessage;
import model.DeviceState;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class AddEditDeviceStateController {

    @FXML
    private Label stateLabel;

    @FXML
    private TextField stateField;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;

    private DbConnection dbConnection = new DbConnection();

    private static Stage stage;
    private static DeviceState deviceState;
    private static boolean isUpdate;

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setDeviceState(DeviceState outState) {
        deviceState = outState;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    private String oldDeviceState = "";

    @FXML
    private void initialize() {
        if (isUpdate)
            oldDeviceState = deviceState.getState();
        stateField.setText(oldDeviceState);
    }

    @FXML
    private void okAction() {
        String state = stateField.getText().trim();
        if (isFieldRight(state)) {
            if (isUpdate) {
                editState(state);
                closeWindow();
            } else {
                addState(state);
                closeWindow();
            }
        } else stateField.setText(state);
    }

    public void addState(String state) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO device_state (state) VALUES (?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, state);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении состояния оборудования", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void editState(String state) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "UPDATE device_state SET state = ? WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, state);
            preState.setInt(2, deviceState.getId());
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при редактировании состояния оборудования", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private boolean isFieldRight(String state) {
        if (!Check.isFieldNull(state)) {
            if (!oldDeviceState.equals(state))
                if (!Check.isNewState(state)) {
                    AlertMessage.showSameName(stateLabel.getText());
                    return false;
                }
            return true;
        } else {
            AlertMessage.showEmptyFieldMessage(stateLabel.getText());
            return false;
        }
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}
