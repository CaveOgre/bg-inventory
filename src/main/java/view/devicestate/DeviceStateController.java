package view.devicestate;

import view.Check;
import mainpackage.Main;
import view.AlertMessage;
import model.DeviceState;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import view.Query;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class DeviceStateController {

    @FXML
    private ListView<DeviceState> stateListView;

    @FXML
    private Button addBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button delBtn;

    @FXML
    private Button exitBtn;

    private DbConnection dbConnection = new DbConnection();
    private ObservableList<DeviceState> stateDeviceList;

    private static Stage stage;

    public static void setStage(Stage outStage) {
        stage = outStage;
    }

    @FXML
    private void initialize() {
        stateDeviceList = FXCollections.observableArrayList();
        loadDeviceStateList();

        stateListView.setItems(stateDeviceList);
    }

    @FXML
    private void goToAddView() {
        int selectedIndex = stateListView.getSelectionModel().getSelectedIndex();
        String title = "Добавление";

        Stage stage = createNewWindow(title);
        AddEditDeviceStateController.setStage(stage);

        assert stage != null;
        stage.showAndWait();

        initialize();
        stateListView.getSelectionModel().select(selectedIndex);
    }

    @FXML
    private void goToEditView() {
        if (Check.isSelectedListView(stateListView)) {
            int selectedIndex = stateListView.getSelectionModel().getSelectedIndex();
            String title = "Редактирование";

            DeviceState deviceState = stateListView.getSelectionModel().getSelectedItem();
            AddEditDeviceStateController.setDeviceState(deviceState);
            AddEditDeviceStateController.setIsUpdate(true);

            Stage stage = createNewWindow(title);
            AddEditDeviceStateController.setStage(stage);

            assert stage != null;
            stage.showAndWait();
            AddEditDeviceStateController.setIsUpdate(false);

            initialize();
            stateListView.getSelectionModel().select(selectedIndex);
        } else AlertMessage.showNotSelectedElement();
    }

    @FXML
    private void deleteDeviceState() {
        if (Check.isSelectedListView(stateListView)) {
            Connection conn = null;
            PreparedStatement preState = null;
            String query = "DELETE FROM device_state WHERE id = ?";
            String updateQuery = "UPDATE device SET deviceState_id = 0 WHERE deviceState_id = ?";
            int selectedIndex = stateListView.getSelectionModel().getSelectedIndex();
            DeviceState deviceState = stateListView.getSelectionModel().getSelectedItem();
            Query.updateDeleteItem(updateQuery, deviceState.getId());
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setInt(1, deviceState.getId());
                preState.executeUpdate();
                initialize();
                stateListView.getSelectionModel().select(Check.selectIndex(selectedIndex));
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при удалении состояния устройства", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        } else AlertMessage.showNotSelectedElement();
    }

    private void loadDeviceStateList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM device_state";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int id = resSet.getInt("id");
                if (id != 0) {
                    stateDeviceList.add(new DeviceState(resSet.getInt("id"),
                            resSet.getString("state")));
                }
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка состояний устройства", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private Stage createNewWindow(String title) {
        String view = "/AddEditDeviceState.fxml";
        String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        AddEditDeviceStateController.setStage(stage);

        return stage;
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }

}
