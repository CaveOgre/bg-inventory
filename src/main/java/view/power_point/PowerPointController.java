package view.power_point;

import view.Check;
import mainpackage.Main;
import model.PowerPoint;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import view.Query;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class PowerPointController {

    @FXML
    private ListView<PowerPoint> powerPointView;

    @FXML
    private Button addBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button delBtn;

    @FXML
    private Button exitBtn;

    private DbConnection dbConnection = new DbConnection();

    private ObservableList<PowerPoint> powerPointList;

    private static Stage stage;

    public static void setStage(Stage outStage) {
        stage = outStage;
    }

    @FXML
    private void initialize() {
        powerPointList = FXCollections.observableArrayList();
        loadPowerPointList();
        powerPointView.setItems(powerPointList);
    }

    private void loadPowerPointList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM power_point ORDER BY pointName";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int powerPointId = resSet.getInt("id");
                if (powerPointId != 0) {
                    powerPointList.add(new PowerPoint(powerPointId,
                            resSet.getString("pointName"), resSet.getInt("powerSupply_id")));
                }
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка пунктов питания", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    @FXML
    private void goToAddView() {
        int selectedIndex = powerPointView.getSelectionModel().getSelectedIndex();
        String title = "Добавление";

        Stage stage = createNewWindow(title);

        assert stage != null;
        stage.showAndWait();

        initialize();
        powerPointView.getSelectionModel().select(selectedIndex);
    }

    @FXML
    private void goToEditView() {
        if (Check.isSelectedListView(powerPointView)) {
            int selectedIndex = powerPointView.getSelectionModel().getSelectedIndex();
            String title = "Редактирование";

            PowerPoint powerPoint = powerPointView.getSelectionModel().getSelectedItem();
            AddEditPowerPointController.setPowerPoint(powerPoint);
            AddEditPowerPointController.setIsUpdate(true);

            Stage stage = createNewWindow(title);

            assert stage != null;
            stage.showAndWait();
            AddEditPowerPointController.setIsUpdate(false);

            initialize();
            powerPointView.getSelectionModel().select(selectedIndex);
        } else AlertMessage.showNotSelectedElement();
    }

    @FXML
    private void deletePowerPoint() {
        if (Check.isSelectedListView(powerPointView)) {
            Connection conn = null;
            PreparedStatement preState = null;
            String query = "DELETE FROM power_point WHERE id = ?";
            String updateQuery = "UPDATE area SET powerPoint_id = 0 WHERE powerPoint_id = ?";
            int selectedIndex = powerPointView.getSelectionModel().getSelectedIndex();
            PowerPoint powerPoint = powerPointView.getSelectionModel().getSelectedItem();
            Query.updateDeleteItem(updateQuery, powerPoint.getId());
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setInt(1, powerPoint.getId());
                preState.executeUpdate();
                initialize();
                powerPointView.getSelectionModel().select(Check.selectIndex(selectedIndex));
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при удалении точки питания", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        } else AlertMessage.showNotSelectedElement();
    }

    private Stage createNewWindow(String title) {
        String view = "/AddEditPowerPoint.fxml";
        String errorText = "Ошибка при создани окна \"" + title + "\"";
        URL toView = AddEditPowerPointController.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, errorText);
        AddEditPowerPointController.setStage(stage);

        return stage;
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}
