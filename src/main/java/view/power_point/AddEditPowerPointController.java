package view.power_point;

import view.Check;
import model.PowerPoint;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class AddEditPowerPointController {

    @FXML
    private ComboBox<String> powerSupplyBox;

    @FXML
    private Label powerSupLabel;

    @FXML
    private Label powerPointLabel;

    @FXML
    private TextField pointNameField;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;

    private ObservableList<String> powerSupList;

    private DbConnection dbConnection = new DbConnection();

    private static Stage stage;
    private static PowerPoint powerPoint;
    private static boolean isUpdate;

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setPowerPoint(PowerPoint outPowerPoint) {
        powerPoint = outPowerPoint;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    private String oldPowerSupply = "";
    private String oldPointName = "";

    @FXML
    private void initialize() {
        powerSupList = FXCollections.observableArrayList();
        loadPowerSupList();
        powerSupplyBox.setItems(powerSupList);
        if (isUpdate) {
            oldPowerSupply = findPowerSupName(powerPoint.getPowerSupplyId());
            oldPointName = powerPoint.getPointName();
            powerSupplyBox.getSelectionModel().select(oldPowerSupply);
            pointNameField.setText(oldPointName);
        }
    }

    @FXML
    private void okAction() {
        String powerSuppName = powerSupplyBox.getEditor().getText();
        String pointName = pointNameField.getText();
        if (isFieldsRight(powerSuppName, pointName)) {
            if (isUpdate) {
                updatePoint(pointName, powerSuppName);
            } else addPoint(pointName, powerSuppName);
        } else {
            powerSupplyBox.getEditor().setText(powerSuppName);
            pointNameField.setText(pointName);
        }
    }

    private void addPoint(String pointName, String powerSupName) {
        Connection conn = null;
        PreparedStatement preState = null;
        int powerSupId = findPowerSuppId(powerSupName);
        String query = "INSERT INTO power_point (pointName, powerSupply_id) VALUES (?, ?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, pointName);
            preState.setInt(2, powerSupId);
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении точки питания", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void updatePoint(String pointName, String powerSupName) {
        Connection conn = null;
        PreparedStatement preState = null;
        int powerSupId = findPowerSuppId(powerSupName);
        String query = "UPDATE power_point SET pointName = ?, powerSupply_id = ? WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, pointName);
            preState.setInt(2, powerSupId);
            preState.setInt(3, powerPoint.getId());
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при редактировании точки питания", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void loadPowerSupList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT powerSupName FROM power_supply";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                powerSupList.add(resSet.getString("powerSupName"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка поставщиков электричества", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private String findPowerSupName(int powerSupId) {
        String powerSupName = "";
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT powerSupName FROM power_supply WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, powerSupId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                powerSupName = resSet.getString("powerSupName");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске имени поставщика электричества", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return powerSupName;
    }

    private int findPowerSuppId(String powerSupName) {
        int powerSuppId = 0;
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT id FROM power_supply WHERE powerSupName = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, powerSupName);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                powerSuppId = resSet.getInt("id");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске id поставшика электричества", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return powerSuppId;
    }

    private boolean isFieldsRight(String powerSupName, String pointName) {
        if (!Check.isFieldNull(powerSupName)) {
            if (!oldPowerSupply.equals(powerSupName)) {
                if (Check.isNewPowerSupp(powerSupName)) {
                    if (AlertMessage.isOkNewValue(powerSupLabel.getText())) {
                        String phone = AlertMessage.showPhoneDialog();
                        if (phone != null)
                            createNewPowerSup(powerSupName, phone);
                        else return false;
                    } else return false;
                }
            }
            if (!Check.isFieldNull(pointName)) {
                if (!oldPointName.equals(pointName)) {
                    int powerSupId = findPowerSuppId(powerSupName);
                    if (!Check.isNewPowerPoint(pointName, powerSupId)) {
                        AlertMessage.showSameName(powerSupLabel.getText());
                        return false;
                    }
                }
                return true;
            } else {
                AlertMessage.showEmptyFieldMessage(powerPointLabel.getText());
                return false;
            }
        } else {
            AlertMessage.showEmptyFieldMessage(powerSupLabel.getText());
            return false;
        }
    }

    private void createNewPowerSup(String powerSupName, String phone) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO power_supply (powerSupName, phone) VALUES (?, ?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, powerSupName);
            preState.setString(2, phone);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении поставщика электричества " +
                    "во время изменения точки питания", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}
