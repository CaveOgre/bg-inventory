package view.area;

import view.Check;
import model.Area;
import model.PowerPoint;
import mainpackage.Main;
import model.PowerSupply;
import view.AlertMessage;
import dbpackage.DbConnection;
import view.DeviceTreeController;


import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.beans.value.ChangeListener;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;

import java.sql.ResultSet;
import java.sql.Connection;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.sql.PreparedStatement;
import java.util.function.UnaryOperator;

public class AddEditAreaController {

    @FXML
    private TextField areaNameBox;

    @FXML
    private ComboBox<String> areaStateBox;

    @FXML
    private ComboBox<String> cityNameBox;

    @FXML
    private ComboBox<String> fStreetBox;

    @FXML
    private TextField fStreetBuilding;

    @FXML
    private ComboBox<String> sStreetBox;

    @FXML
    private CheckBox crossroadBox;

    @FXML
    private TextField sStreetBuilding;

    @FXML
    private ComboBox<String> powerSuppBox;

    @FXML
    private ComboBox<String> powerPointBox;

    @FXML
    private ComboBox<String> subNetBox;

    @FXML
    private ComboBox<String> stageBox;

    @FXML
    private ComboBox<String> yearBox;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;

    private static final String AREALABEL = "название площадки";
    private static final String AREASTATELABEL = "состояние пдощадки";
    private static final String CITYLABEL = "город";
    private static final String FSTREETLABEL = "улица 1";
    private static final String SSTREETLABEL = "улица 2";
    private static final String POWERSUPLABEL = "поставщик услуг";
    private static final String POWERPOINTLABEL = "пункт питания";
    private static final String BGSTAGELABEL = "этап";
    private static final String YEARLABEL = "год";
    private static final String SUBNETLABLE = "подсеть";

    private DbConnection dbConnection = new DbConnection();
    private ObservableList<String> areaStateList;
    private ObservableList<String> cityNameList;
    private ObservableList<String> streetList;
    private ObservableList<String> powerSuppList;
    private ObservableList<String> powerPointList;
    private ObservableList<String> subNetList;

    private String oldAreaName = "";
    private String oldAreaState = "";
    private String oldCityName = "";
    private String oldFStreet = "";
    private String oldFBuilding = "";
    private boolean oldIsCrossroad;
    private String oldSStreet = "";
    private String oldSBuilding = "";
    private String oldPowerSupply = "";
    private String oldPowerPoint = "";
    private String oldBgStage = "";
    private String oldYear = "";

    private static Stage stage;
    private static Area globalArea;
    private static boolean isUpdate;

    public static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setGlobalArea(Area outArea) {
        globalArea = outArea;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    private ChangeListener<String> powerSupListener = (observable, oldPowerSup, newPowerSup) -> {
        if (newPowerSup != null) {
            int powerSupId = getPowerSupId(newPowerSup);
            loadPowerPointBySupplyId(powerSupId);
            powerPointBox.setItems(powerPointList);
            powerPointBox.getSelectionModel().selectFirst();
        }
    };

    private int getPowerSupId(String powerSupp) {
        int powerSupId = 0;
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT id FROM power_supply WHERE powerSupName = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, powerSupp);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                powerSupId = resSet.getInt("id");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске поставщика электричества", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return powerSupId;
    }

    private void loadPowerPointBySupplyId(int supplyId) {
        powerPointList = FXCollections.observableArrayList();
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT pointName FROM power_point WHERE powerSupply_id = ? ORDER BY pointName";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, supplyId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                String pointName = resSet.getString("pointName");
                if (!pointName.equals("не задано"))
                    powerPointList.add(pointName);
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка пунктов питания", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
    }

    private ChangeListener<String> cityBoxListener = (observable, oldCity, newCity) -> {
        int cityId = 0;
        if (newCity != null)
            cityId = getCityId(newCity);
        loadStreetListByCityId(cityId);
        fStreetBox.setItems(streetList);
        fStreetBox.getSelectionModel().selectFirst();
        sStreetBox.setItems(streetList);
    };

    private int getCityId(String cityName) {
        int cityId = 0;
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT id FROM city WHERE cityName = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, cityName);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                cityId = resSet.getInt("id");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске Id города", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return cityId;
    }

    private void loadStreetListByCityId(int cityId) {
        streetList = FXCollections.observableArrayList();
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT streetName FROM street WHERE city_id = ? ORDER BY streetName";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, cityId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                streetList.add(resSet.getString("streetName"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка улиц", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
    }

    private ChangeListener<Boolean> checkBoxListener = (observable, oldValue, newValue) -> {
        if (crossroadBox.isSelected())
            setSStreetDisable(false);
        else setSStreetDisable(true);
    };

    private void setSStreetDisable(boolean value) {
        sStreetBox.setDisable(value);
        sStreetBuilding.setDisable(value);
    }

    @FXML
    private void initialize() {
        removeBoxListeners();
        setSStreetDisable(true);

        loadData();
        setListsToBoxes();

        setIpAddressFilter();

        if (isUpdate) {
            setOldData();
            if (crossroadBox.isSelected())
                setSStreetDisable(false);
        }
        addBoxListeners();
    }

    private void setOldData() {
        oldAreaName = globalArea.getAreaName();
        oldAreaState = getAreaState(globalArea.getAreaStateId());
        oldCityName = getCityName(globalArea.getCityId());
        oldFStreet = getStreetName(globalArea.getFStreetId());
        if (globalArea.getFStreetBuilding() != null)
            oldFBuilding = globalArea.getFStreetBuilding();
        oldIsCrossroad = globalArea.getCrossroad();
        oldSStreet = getStreetName(globalArea.getSStreetId());
        if (globalArea.getSStreetBuilding() != null)
            oldSBuilding = globalArea.getSStreetBuilding();
        PowerPoint powerPoint = getPowerPoint(globalArea.getPowerPointId());
        oldPowerPoint = powerPoint.getPointName();
        PowerSupply powerSupply = getPowerSupply(powerPoint.getPowerSupplyId());
        oldPowerSupply = powerSupply.getPowerSuppName();
        oldBgStage = String.valueOf(globalArea.getBgStage());
        oldYear = String.valueOf(globalArea.getYear());
        String oldSubnet = getSubnet(globalArea.getId());


        areaNameBox.setText(oldAreaName);
        areaStateBox.getSelectionModel().select(oldAreaState);
        cityNameBox.getSelectionModel().select(oldCityName);
        fStreetBox.getSelectionModel().select(oldFStreet);
        fStreetBuilding.setText(oldFBuilding);
        crossroadBox.setSelected(oldIsCrossroad);
        sStreetBox.getSelectionModel().select(oldSStreet);
        sStreetBuilding.setText(oldSBuilding);
        powerSuppBox.getSelectionModel().select(oldPowerSupply);
        powerPointBox.getSelectionModel().select(oldPowerPoint);
        stageBox.getSelectionModel().select(oldBgStage);
        yearBox.getSelectionModel().select(oldYear);
        subNetBox.getSelectionModel().select(oldSubnet);
    }

    private String getAreaState(int areaStateId) {
        String areaState = "";
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT areaState FROM area_state WHERE id =?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, areaStateId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                areaState = resSet.getString("areaState");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске состояния площадки", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return areaState;
    }

    private String getCityName(int cityId) {
        String cityName = "";
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT cityName FROM city WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, cityId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                cityName = resSet.getString("cityName");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске названия города", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return cityName;
    }

    private String getStreetName(int streetId) {
        String street = "";
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT streetName FROM street WHERE id = ? ORDER BY streetName";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, streetId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                street = resSet.getString("streetName");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске названия улица", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return street;
    }

    private PowerPoint getPowerPoint(int powerPointId) {
        PowerPoint powerPoint = null;
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM power_point WHERE id = ? ORDER BY pointName";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, powerPointId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                powerPoint = new PowerPoint(resSet.getInt("id"), resSet.getString("pointName"),
                        resSet.getInt("powerSupply_id"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске точки питания", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return powerPoint;
    }

    private PowerSupply getPowerSupply(int powerSupId) {
        PowerSupply powerSup = null;
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM power_supply WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, powerSupId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                powerSup = new PowerSupply(resSet.getInt("id"),
                        resSet.getString("powerSupName"), resSet.getString("phone"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске имени поставщика электричества", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return powerSup;
    }

    private String getSubnet(int areaId) {
        String subnet = "";
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT INET_NTOA(sub_network) as subnet FROM network WHERE area_id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, areaId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                subnet = resSet.getString("subnet");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске подсети", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return subnet;
    }

    private void loadData() {
        loadCityNameList();
        loadStreetNameList();
        loadPowerSuppNameList();
        loadAreaState();
        loadSubNetList();
    }

    private void setListsToBoxes() {
        areaStateBox.setItems(areaStateList);
        cityNameBox.setItems(cityNameList);
        fStreetBox.setItems(streetList);
        sStreetBox.setItems(streetList);
        powerSuppBox.setItems(powerSuppList);
        fillBgStageData();
        subNetBox.setItems(subNetList);
    }

    private void loadAreaState() {
        areaStateList = FXCollections.observableArrayList();
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT areaState FROM area_state";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                areaStateList.add(resSet.getString("areaState"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка состояний площадок", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private void loadCityNameList() {
        cityNameList = FXCollections.observableArrayList();
        Connection conn = null;
        ResultSet resSet = null;
        cityNameList.add("Калининград");
        String query = "SELECT*FROM city ORDER BY cityName";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int cityId = resSet.getInt("id");
                String cityName = resSet.getString("cityName");
                if (cityId != 0 && ! cityName.equals("Калининград"))
                    cityNameList.add(cityName);
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка городов", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private void loadStreetNameList() {
        streetList = FXCollections.observableArrayList();
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT DISTINCT streetName FROM street ORDER BY streetName";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                String streetName = resSet.getString("streetName");
                if (!streetName.equals("не задано"))
                    streetList.add(streetName);
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка улиц", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
    }

    private void loadSubNetList() {
        subNetList = FXCollections.observableArrayList();
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT INET_NTOA(sub_network) as subnet FROM network";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                String subnet = resSet.getString("subnet");
                subNetList.add(subnet);
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка подсетей", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private void loadPowerSuppNameList() {
        powerSuppList = FXCollections.observableArrayList();
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM power_supply ORDER BY powerSupName";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int powerSupId = resSet.getInt("id");
                if (powerSupId != 0)
                    powerSuppList.add(resSet.getString("powerSupName"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка поставщика электричества", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private void fillBgStageData() {
        ObservableList<String> stageList = FXCollections.observableArrayList();
        ObservableList<String> yearList = FXCollections.observableArrayList();
        stageBox.setVisibleRowCount(5);
        yearBox.setVisibleRowCount(5);
        for (int i = 1; i <= 20; i++)
            stageList.add(String.valueOf(i));
        for (int j = 2014; j <= 2030; j++)
            yearList.add(String.valueOf(j));
        stageBox.setItems(stageList);
        yearBox.setItems(yearList);
    }

    private void removeBoxListeners() {
        cityNameBox.getSelectionModel().selectedItemProperty().removeListener(cityBoxListener);
        crossroadBox.selectedProperty().removeListener(checkBoxListener);
        powerSuppBox.getEditor().textProperty().removeListener(powerSupListener);
    }

    private void addBoxListeners() {
        cityNameBox.getSelectionModel().selectedItemProperty().addListener(cityBoxListener);
        crossroadBox.selectedProperty().addListener(checkBoxListener);
        powerSuppBox.getEditor().textProperty().addListener(powerSupListener);
    }

    @FXML
    private void okAction() {
        ArrayList<String> areaList = new ArrayList<>();
        areaList = fillAreaList(areaList);
        if (!isOldData(areaList) && isFielsdRight(areaList)) {
            String queryForSave = "UPDATE area SET areaName = ?, city_id = ?, fStreet_id = ?, fStreetBuilding = ?, " +
                    "crossroad = ?, sStreet_id = ?, sStreetBuilding = ?, powerPoint_id = ?, bgStage = ?, " +
                    "year = ?, areaState_id = ? WHERE id = ?";
            String queryForAdd = "INSERT INTO area " +
                    "(areaName, city_id, fStreet_id, fStreetBuilding, crossroad, sStreet_id, sStreetBuilding, " +
                    "powerPoint_id, bgStage, year, areaState_id) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
            if (isUpdate) {
                saveArea(queryForSave, true);
            } else saveArea(queryForAdd, false);
        }
    }

    private void saveArea(String query, boolean isToSave) {
        String areaName = areaNameBox.getText().trim();
        String cityName = cityNameBox.getValue().trim();
        int cityId = getCityId(cityName);
        String fStreetName = fStreetBox.getEditor().getText().trim();
        int fStreetId = getStreetId(fStreetName);
        String fBuilding = fStreetBuilding.getText().trim();
        boolean isCrossroad = crossroadBox.isSelected();
        String sStreetName = sStreetBox.getEditor().getText().trim();
        int sStreetId = getStreetId(sStreetName);
        String sBuilding = sStreetBuilding.getText().trim();
        String powerPoint = powerPointBox.getEditor().getText().trim();
        int powerPointId = getPowerPointId(powerPoint);
        int bgStage = Integer.valueOf(stageBox.getValue().trim());
        short year = Short.valueOf(yearBox.getValue().trim());
        String areaState = areaStateBox.getValue().trim();
        int areaStateId = getAreaStateId(areaState);
        Connection conn = null;
        PreparedStatement preState = null;
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, areaName);
            preState.setInt(2, cityId);
            preState.setInt(3, fStreetId);
            preState.setString(4, fBuilding);
            preState.setBoolean(5, isCrossroad);
            preState.setInt(6, sStreetId);
            preState.setString(7, sBuilding);
            preState.setInt(8, powerPointId);
            preState.setInt(9, bgStage);
            preState.setShort(10, year);
            preState.setInt(11, areaStateId);
            if (isToSave)
                preState.setInt(12, globalArea.getId());
            preState.executeUpdate();
            saveSubnet(isToSave, conn);
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при сохранении площадки", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private int getStreetId(String streetName) {
        int streetId = 0;
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT id FROM street WHERE streetName = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, streetName);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                streetId = resSet.getInt("id");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске Id улицы", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return streetId;
    }

    private int getPowerPointId(String powerPoinName) {
        int powerPointId = 0;
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT id FROM power_point WHERE pointName = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, powerPoinName);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                powerPointId = resSet.getInt("id");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске id точки питания", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return powerPointId;
    }

    private int getAreaStateId(String areaState) {
        int areaStateId = 0;
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT id FROM area_state WHERE areaState = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, areaState);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                areaStateId = resSet.getInt("id");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске id состояния площадки", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return areaStateId;
    }

    //TODO: problem with old ipaddress -
    private void saveSubnet(boolean isToSave, Connection conn) {
        String subnet = subNetBox.getEditor().getText().trim();
        if (isToSave) {
            addNewSubnet(subnet, globalArea.getId());
        } else {
            int areaId = getSavedAreaId(conn);
            addNewSubnet(subnet, areaId);
        }
    }

    private int getSavedAreaId(Connection conn) {
        int areaId = 0;
        ResultSet resSet = null;
        String query = "SELECT last_insert_id()";
        try {
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                areaId = resSet.getInt("last_insert_id()");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при получении ID последней добавленной площадки", e);
        } finally {
            dbConnection.closeConnection(resSet);
        }
        return areaId;
    }

    private boolean isOldData(ArrayList<String> areaList) {
        ArrayList<String> oldData = new ArrayList<>();

        oldData.add(oldAreaName);
        oldData.add(oldAreaState);
        oldData.add(oldCityName);
        oldData.add(oldFStreet);
        oldData.add(oldFBuilding);
        oldData.add(String.valueOf(oldIsCrossroad));
        oldData.add(oldSStreet);
        oldData.add(oldSBuilding);
        oldData.add(oldPowerSupply);
        oldData.add(oldPowerPoint);
        oldData.add(oldBgStage);
        oldData.add(oldYear);

        return areaList.equals(oldData);
    }

    private ArrayList<String> fillAreaList(ArrayList<String> areaList) {
        String areaName = areaNameBox.getText().trim();
        String cityName = cityNameBox.getValue().trim();
        int cityId = getCityId(cityName);
        String fStreetName = fStreetBox.getEditor().getText().trim();
        int fStreetId = getStreetId(fStreetName);
        String fBuilding = fStreetBuilding.getText().trim();
        boolean isCrossroad = crossroadBox.isSelected();
        String sStreetName = sStreetBox.getEditor().getText().trim();
        int sStreetId = getStreetId(sStreetName);
        String sBuilding = sStreetBuilding.getText().trim();
        String powerPoint = powerPointBox.getEditor().getText().trim();
        int powerPointId = getPowerPointId(powerPoint);
        int bgStage = Integer.valueOf(stageBox.getValue().trim());
        short year = Short.valueOf(yearBox.getValue().trim());
        String areaState = areaStateBox.getValue().trim();
        int areaStateId = getAreaStateId(areaState);

        areaList.add(areaName);
        areaList.add(String.valueOf(cityId));
        areaList.add(String.valueOf(fStreetId));
        areaList.add(fBuilding);
        areaList.add(String.valueOf(isCrossroad));
        areaList.add(String.valueOf(sStreetId));
        areaList.add(sBuilding);
        areaList.add(String.valueOf(powerPointId));
        areaList.add(String.valueOf(bgStage));
        areaList.add(String.valueOf(year));
        areaList.add(String.valueOf(areaStateId));

        return areaList;
    }

    private boolean isFielsdRight(ArrayList<String> areaList) {
        String areaName = areaList.get(0);
        return isAreaDataRight(areaName) && isAreaStateRight() && isAddressDataRight()
                && isPowerSuppDataRight() && isStageRight() && isSubnetRight();
    }

    private boolean isAreaDataRight(String areaName) {
        if (!Check.isFieldNull(areaName)) {
            if (!Check.isNewAreaName(areaName) && !areaName.equals(oldAreaName)) {
                AlertMessage.showSameName(AREALABEL);
                return false;
            }
            return true;
        } else {
            AlertMessage.showEmptyFieldMessage(AREALABEL);
            return false;
        }
    }

    private boolean isAddressDataRight() {
        if (!Check.isComboBoxNull(cityNameBox)) {
            String cityName = cityNameBox.getValue().trim();
            return isFStreetBoxRight(cityName);
        } else {
            AlertMessage.showEmptyFieldMessage(CITYLABEL);
            return false;
        }
    }

    private boolean isFStreetBoxRight(String cityName) {
        String fStreet = fStreetBox.getEditor().getText().trim();
        int cityId;
        if (!Check.isFieldNull(fStreet)) {
            cityId = getCityId(cityName);
            if (Check.isNewStreet(fStreet, cityId))
                createNewStreet(fStreet, cityId);
            return !crossroadBox.isSelected() || isSStreetBoxNull(cityName);
        } else {
            AlertMessage.showEmptyFieldMessage(FSTREETLABEL);
            return false;
        }
    }

    private boolean isSStreetBoxNull(String cityName) {
        String sStreet = sStreetBox.getEditor().getText().trim();
        int cityId;
        if (!Check.isFieldNull(sStreet)) {
            cityId = getCityId(cityName);
            if (Check.isNewStreet(sStreet, cityId))
                createNewStreet(sStreet, cityId);
            return true;
        } else {
            AlertMessage.showEmptyFieldMessage(SSTREETLABEL);
            return false;
        }
    }

    private void createNewStreet(String streetName, int cityId) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO street (city_id, streetName) VALUES (?,?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, cityId);
            preState.setString(2, streetName);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении новой улицы", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private boolean isPowerSuppDataRight() {
        String powerSupName = powerSuppBox.getEditor().getText().trim();
        if (!Check.isFieldNull(powerSupName)) {
            if (Check.isNewPowerSupp(powerSupName)) {
                if (AlertMessage.isOkNewValue(POWERSUPLABEL)) {
                    String phone = AlertMessage.showPhoneDialog();
                    if (phone != null)
                        createNewPowerSupp(powerSupName, phone);
                } else return false;
            }
            int powerSupId = getPowerSupId(powerSupName);
            return isPowerPointBoxRight(powerSupId);
        } else {
            AlertMessage.showEmptyFieldMessage(POWERSUPLABEL);
            return false;
        }
    }

    private void createNewPowerSupp(String powerSupName, String phone) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO power_supply (powerSupName, phone) VALUES(?,?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, powerSupName);
            preState.setString(2, phone);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении поставщика электричества", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private boolean isPowerPointBoxRight(int powerSupId) {
        String powerPoint = powerPointBox.getEditor().getText().trim();
        if (!Check.isFieldNull(powerPoint)) {
            if (Check.isNewPowerPoint(powerPoint, powerSupId))
                createNewPowerPoint(powerPoint, powerSupId);
            return true;
        } else {
            AlertMessage.showEmptyFieldMessage(POWERPOINTLABEL);
            return false;
        }
    }

    private void createNewPowerPoint(String powerPoint, int powerSupId) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO power_point (pointName, powerSupply_id) VALUES (?,?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, powerPoint);
            preState.setInt(2, powerSupId);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении новой точки питания", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private boolean isStageRight() {
        if (!Check.isComboBoxNull(stageBox))
            return isYearRight();
        else {
            AlertMessage.showEmptyFieldMessage(BGSTAGELABEL);
            return false;
        }
    }

    private boolean isYearRight() {
        if (!Check.isComboBoxNull(yearBox))
            return true;
        else {
            AlertMessage.showEmptyFieldMessage(YEARLABEL);
            return false;
        }
    }

    private boolean isAreaStateRight() {
        if (!Check.isComboBoxNull(areaStateBox))
            return true;
        else {
            AlertMessage.showEmptyFieldMessage(AREASTATELABEL);
            return false;
        }
    }

    private boolean isSubnetRight() {
        String subnet = subNetBox.getEditor().getText().trim();
        if (!Check.isFieldNull(subnet)) {
            return isSubnetValid(subnet);
        } else {
            AlertMessage.showEmptyFieldMessage(SUBNETLABLE);
            return false;
        }
    }

    private boolean isSubnetValid(String subnet) {
        if (isSubnetFormatValid(subnet))
            return true;
        else {
            AlertMessage.showSubnetWrongFormat(SUBNETLABLE);
            return false;
        }
    }

    private void addNewSubnet(String subnet, int areaId) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO network (area_id, sub_network, mask, gateway, vlan_id) " +
                "VALUES (?,INET_ATON(?),24,INET_ATON(0),1)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, areaId);
            preState.setString(2, subnet);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Проблема при добавлении новой подсети", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private boolean isSubnetFormatValid(String ipAddress) {
        Pattern pattern;
        Matcher matcher;
        String IPADDRESS_PATTERN
                = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
                + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
                + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
                + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
        pattern = Pattern.compile(IPADDRESS_PATTERN);
        matcher = pattern.matcher(ipAddress);
        return matcher.matches();
    }

    private void setIpAddressFilter() {
        String regex = makePartialIPRegex();
        final UnaryOperator<Change> ipAddressFilter = c -> {
            String text = c.getControlNewText();
            if (text.matches(regex)) {
                return c;
            } else {
                return null;
            }
        };
        subNetBox.getEditor().setTextFormatter(new TextFormatter<>(ipAddressFilter));
    }

    private String makePartialIPRegex() {
        String partialBlock = "(([01]?[0-9]{0,2})|(2[0-4][0-9])|(25[0-5]))";
        String subsequentPartialBlock = "(\\." + partialBlock + ")";
        String ipAddress = partialBlock + "?" + subsequentPartialBlock + "{0,3}";
        return "^" + ipAddress;
    }

    @FXML
    private void closeWindow() {
        stage.close();
        DeviceTreeController deviceTreeController = Main.deviceTreeController;
        deviceTreeController.initialize();
    }
}
