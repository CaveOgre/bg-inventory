package view.area;

import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mainpackage.Main;
import model.Area;
import view.DeviceTreeController;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AreaInfoController {

    public static int areaId;

    @FXML
    private TextField areaNameField;
    @FXML
    private TextField areaStateField;
    @FXML
    private TextArea addressField;
    @FXML
    private TextField subnetField;
    @FXML
    private TextField gatewayField;
    @FXML
    private TextField vlanField;
    @FXML
    private TextField maskField;
    @FXML
    private TextField contractField;
    @FXML
    private TextField providerField;
    @FXML
    private TextField bgStageField;
    @FXML
    private TextField yearField;
    @FXML
    private TextField powerPointField;
    @FXML
    private TextField powerSupplyField;
    @FXML
    private TextField phoneField;
    @FXML
    private Button addBtn;
    @FXML
    private Button editBtn;
    @FXML
    private Button delBtn;

    private DbConnection dbConnection = new DbConnection();

    private static Stage stage;

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    @FXML
    private void initialize() {
        getAreaInfo();
    }

    private void getAreaInfo() {
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT areaName, areaState, (INET_NTOA(sub_network)) as subnet, (INET_NTOA(gateway)) as " +
                "gateway, vlanId, mask, vlanContNumber, providerName, bgStage, `year`, pointName, powerSupName, " +
                "phone FROM area\n" +
                "LEFT JOIN network ON area.id = network.area_id\n" +
                "LEFT JOIN area_state ON area.areaState_id = area_state.id\n" +
                "LEFT JOIN vlan ON network.vlan_id = vlan.vlanId\n" +
                "LEFT JOIN vlan_contract ON vlan.vlanContract_id = vlan_contract.id\n" +
                "LEFT JOIN provider ON vlan_contract.provider_id = provider.id\n" +
                "LEFT JOIN power_point ON area.powerPoint_id = power_point.id\n" +
                "LEFT JOIN power_supply ON power_point.powerSupply_id = power_supply.id\n" +
                "WHERE area.id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, areaId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                areaNameField.setText(resSet.getString("areaName"));
                areaStateField.setText(resSet.getString("areaState"));
                String address = getAddress(getArea());
                addressField.setText(address);
                subnetField.setText(resSet.getString("subnet"));
                gatewayField.setText(resSet.getString("gateway"));
                vlanField.setText(resSet.getString("vlanId"));
                maskField.setText(resSet.getString("mask"));
                contractField.setText(resSet.getString("vlanContNumber"));
                providerField.setText(resSet.getString("providerName"));
                bgStageField.setText(resSet.getString("bgStage"));
                yearField.setText(String.valueOf(resSet.getInt("year")));
                powerPointField.setText(resSet.getString("pointName"));
                powerSupplyField.setText(resSet.getString("powerSupName"));
                phoneField.setText(resSet.getString("phone"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
    }

    private String getAddress(Area area) {
        assert area != null;
        boolean isCrossroad = area.getCrossroad();
        String address = "";
        String cityName = getCityName(area.getCityId());
        address = address.concat("г. " + cityName + ", ");
        if (isCrossroad)
            address = address.concat("перекресток ");
        String fStreet = getStreetName(area.getFStreetId());
        address = address.concat(fStreet);
        String fStreetBuilding = "";
        if (area.getFStreetBuilding() != null)
            fStreetBuilding = " " + area.getFStreetBuilding();
        address = address.concat(fStreetBuilding);
        if (isCrossroad) {
            String sStreet = " - " + getStreetName(area.getSStreetId());
            address = address.concat(sStreet);
            String sStreetBuilding = "";
            if (area.getSStreetBuilding() != null)
                sStreetBuilding = " " + area.getSStreetBuilding();
            address = address.concat(sStreetBuilding);
        }
        return address;
    }

    private String getCityName(int cityId) {
        String cityName = "";
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT cityName FROM city WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, cityId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                cityName = resSet.getString("cityName");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске названия города", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return cityName;
    }

    private String getStreetName(int streetId) {
        String street = "";
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT streetName FROM street WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, streetId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                street = resSet.getString("streetName");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске названия улицы", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return street;
    }


    @FXML
    public void deleteArea() {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "DELETE FROM area WHERE id = ?";
        if (areaId != 0) {
            try {
                setNetworkUnknown();
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setInt(1, areaId);
                preState.executeUpdate();
                redrawForm();
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при удалении площадки", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        }
    }

    private void setNetworkUnknown() {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "UPDATE network SET area_id = 0 WHERE area_id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, areaId);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при назначении area_id = 0", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void redrawForm() {
        DeviceTreeController deviceTreeController = Main.deviceTreeController;
        deviceTreeController.initialize();
        areaId = 0;
        initialize();
    }

    @FXML
    public void goToAddView() {
        String title = "Добавление";

        Stage stage = createNewWindow(title);
        AddEditAreaController.setStage(stage);

        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    public void goToEditView() {
        String title = "Редактирование";

        AddEditAreaController.setGlobalArea(getArea());
        AddEditAreaController.setIsUpdate(true);

        Stage stage = createNewWindow(title);
        AddEditAreaController.setStage(stage);

        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
        AddEditAreaController.setIsUpdate(false);
    }

    private Stage createNewWindow(String title) {
        String view = "/AddEditArea.fxml";
        String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        AddEditAreaController.setStage(stage);

        return stage;
    }

    private Area getArea() {
        Area area = null;
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM area WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, areaId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                int id = resSet.getInt("id");
                String areaName = resSet.getString("areaName");
                int cityId = resSet.getInt("city_id");
                int fStreetId = resSet.getInt("fStreet_Id");
                String fStreetBuilding = resSet.getString("fStreetBuilding");
                boolean crossroad = resSet.getBoolean("crossroad");
                int sStreetId = resSet.getInt("sStreet_id");
                String sStreetBuilding = resSet.getString("sStreetBuilding");
                int powerPointId = resSet.getInt("powerPoint_id");
                int bgStage = resSet.getInt("bgStage");
                short year = resSet.getShort("year");
                int areaStateId = resSet.getInt("areaState_id");
                area = new Area(id, areaName, cityId, fStreetId, fStreetBuilding, crossroad, sStreetId,
                        sStreetBuilding, powerPointId, bgStage, year, areaStateId);
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске объекта Area", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return area;
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }

}

