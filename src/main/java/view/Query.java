package view;

import dbpackage.DbConnection;
import view.connection.ConnectionController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Query {

    private static DbConnection dbConnection = new DbConnection();

    public static int getIdQuery(String valueName, String query, String log) {
        int id = 0;
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, valueName);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                id = resSet.getInt("id");
            }
        } catch (SQLException e) {
            dbConnection.setLog(log, e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return id;
    }

    public static int getIdQuery(int valueId, String valueName, String query, String log) {
        int id = 0;
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, valueId);
            preState.setString(2, valueName);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                id = resSet.getInt("id");
            }
        } catch (SQLException e) {
            dbConnection.setLog(log, e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return id;
    }

    public static void updateDeleteItem(String query, int itemId) {
        Connection conn = null;
        PreparedStatement preState = null;
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, itemId);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при обновлении объекта во время удаления", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }
}
