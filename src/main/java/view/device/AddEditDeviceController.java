package view.device;


import mainpackage.Main;
import view.Check;
import view.DeviceTreeController;
import view.Query;
import view.AlertMessage;
import dbpackage.DbConnection;
import view.model.AddEditModelController;
import view.power.AddEditPowerController;
import view.server.AddEditServerController;
import view.vendor.AddEditVendorController;
import view.cameratype.AddEditCameraTypeController;
import view.connection.AddEditConnectionController;
import view.devicetype.AddEditDeviceTypeController;
import view.devicestate.AddEditDeviceStateController;
import view.devicegroup.AddEditDeviceGroupController;
import view.devicesupply.AddEditDeviceSupplyController;
import view.devicecontract.AddEditDeviceContController;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.layout.HBox;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.beans.value.ChangeListener;
import javafx.scene.control.TextFormatter;

import java.sql.ResultSet;
import java.sql.Connection;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.sql.PreparedStatement;
import java.util.function.UnaryOperator;

public class AddEditDeviceController {

    @FXML
    private ComboBox<String> deviceTypeBox;
    @FXML
    private ComboBox<String> deviceGroupBox;
    @FXML
    private ComboBox<String> cameraTypeBox;
    @FXML
    private TextField cameraNumField;
    @FXML
    private ComboBox<String> vendorBox;
    @FXML
    private ComboBox<String> modelBox;
    @FXML
    private TextField inventarField;
    @FXML
    private TextField serialField;
    @FXML
    private ComboBox<String> connTypeBox;
    @FXML
    private ComboBox<String> devicePowerBox;
    @FXML
    private ComboBox<String> deviceSupBox;
    @FXML
    private ComboBox<String> deviceContBox;
    @FXML
    private TextField garantField;
    @FXML
    private TextField ipAddressField;
    @FXML
    private TextField loginField;
    @FXML
    private TextField passwordField;
    @FXML
    private CheckBox isIssBox;
    @FXML
    private ComboBox<String> serverBox;
    @FXML
    private ComboBox<String> deviceStateBox;
    @FXML
    private HBox cameraSettingRow;
    @FXML
    private Button okBtn;
    @FXML
    private Button cancelBtn;

    private DbConnection dbConnection = new DbConnection();

    private static final String DEVICETYPEFIELD = "тип устройства";
    private static final String DEVICEFIELD = "устройство";
    private static final String CAMERATYPEFIELD = "назначение камеры";
    private static final String CAMERANUMFIELD = "номер камеры";
    private static final String VENDORFIELD = "производитель";
    private static final String DEVICEMODEL = "модель утсройства";
    private static final String INVENTARFIELD = "инвентарный номер";
    private static final String SERIALFIELD = "серийный номер";
    private static final String CONNECTIONTYPE = "тип подключения";
    private static final String POWERFIELD = "тип питания";
    private static final String DEVICESUPPLYFIELD = "поставщик устройства";
    private static final String DEVICECONTRACTFIELD = "контракт на поставку";
    private static final String GARANTFIELD = "гарантийный срок";
    private static final String IPFIELD = "IP адрес";
    private static final String LOGINFIELD = "логин";
    private static final String PASSWORDFIELD = "пароль";
    private static final String SERVERFIELD = "заведен на сервер";
    private static final String STATEFIELD = "состояние устройства";

    private ObservableList<String> deviceTypeList;
    private ObservableList<String> deviceGroupList;
    private ObservableList<String> cameraTypeList;
    private ObservableList<String> vendorList;
    private ObservableList<String> modelList;
    private ObservableList<String> connTypeList;
    private ObservableList<String> powerTypeList;
    private ObservableList<String> deviceSupList;
    private ObservableList<String> contractList;
    private ObservableList<String> serverList;
    private ObservableList<String> stateList;

    private String oldDeviceType;
    private String oldGroupName;
    private String oldCameraType;
    private String oldCameraNum;
    private String oldVendorName;
    private String oldModelName;
    private String oldInventar;
    private String oldSerial;
    private String oldConnType;
    private String oldPowerName;
    private String oldDeviceSup;
    private String oldContractNumber;
    private String oldGarant;
    private String oldIpAddress;
    private String oldLogin;
    private String oldPassword;
    private String oldIsIss;
    private String oldServerName;
    private String oldState;

    private static ArrayList<String> deviceInfo;
    private static boolean isUpdate;
    private static Stage stage;
    private static int areaId;
    private static int deviceId;

    private ChangeListener<String> deviceGroupListener = (observable, oldGroup, newGroup) -> {
        if (newGroup != null) {
            switch (newGroup.toLowerCase()) {
                case "камера": {
                    cameraSettingRow.setDisable(false);
                    break;
                }
                default: {
                    cameraSettingRow.setDisable(true);
                    cameraTypeBox.getEditor().setText("");
                    cameraNumField.setText("");
                    break;
                }
            }
        }
    };
    private ChangeListener<String> vendorListener = (observable, oldVendor, newVendor) -> {
        if (newVendor != null) {
            int vendorId = getVendorId(newVendor);
            loadModelList(vendorId);
            modelBox.setItems(modelList);
            if (modelList.size() != 0)
                modelBox.getSelectionModel().selectFirst();
            else modelBox.getEditor().setText("");
        }
    };
    private ChangeListener<String> supplyListener = (observable, oldSupply, newSupply) -> {
        if (newSupply != null) {
            int supplyId = getSupplyId(newSupply);
            loadContractList(supplyId);
            deviceContBox.setItems(contractList);
            if (contractList.size() != 0)
                deviceContBox.getSelectionModel().selectFirst();
            else deviceContBox.getEditor().setText("");
        }
    };
    private ChangeListener<Boolean> isIssListener = (observable, oldValue, newValue) -> {
        if (isIssBox.isSelected()) {
            serverBox.setDisable(false);
            serverBox.getSelectionModel().selectFirst();
        } else {
            serverBox.setDisable(true);
            serverBox.getEditor().setText("");
        }
    };

    private int getVendorId(String vendorName) {
        String query = "SELECT id FROM vendor WHERE vendorName = ?";
        String log = "Ошибка при поиске ID производителя устройства";
        return Query.getIdQuery(vendorName, query, log);
    }

    private int getSupplyId(String supplyName) {
        String query = "SELECT id FROM device_supply WHERE deviceSupName = ?";
        String log = "Ошибка при поиске ID поставщика оборудования";
        return Query.getIdQuery(supplyName, query, log);
    }

    private void loadContractList(int supplyId) {
        contractList = FXCollections.observableArrayList();
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT contractNumber FROM device_contract WHERE deviceSupply_id = ? ORDER BY contractNumber";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, supplyId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                String contractNumber = resSet.getString("contractNumber");
                contractList.add(contractNumber);
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка контрактов поставщика", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
    }

    static void setDeviceInfo(ArrayList<String> outDeviceInfo) {
        deviceInfo = outDeviceInfo;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setAreaId(int outAreaId) {
        areaId = outAreaId;
    }

    static void setDeviceId(int outDeviceId) {
        deviceId = outDeviceId;
    }

    @FXML
    private void initialize() {
        removeListeners();

        loadLists();
        setListsToBoxes();
        setIpAddressFilter();

        if (isUpdate) {
            setInfo();
        }
        setListners();
    }

    private void setInfo() {
        oldDeviceType = deviceInfo.get(0);
        oldGroupName = deviceInfo.get(1);
        oldCameraType = deviceInfo.get(2);
        oldCameraNum = deviceInfo.get(3);
        oldVendorName = deviceInfo.get(4);
        oldModelName = deviceInfo.get(5);
        oldInventar = deviceInfo.get(6);
        oldSerial = deviceInfo.get(7);
        oldConnType = deviceInfo.get(8);
        oldPowerName = deviceInfo.get(9);
        oldDeviceSup = deviceInfo.get(10);
        oldContractNumber = deviceInfo.get(11);
        oldGarant = deviceInfo.get(12);
        oldIpAddress = deviceInfo.get(13);
        oldLogin = deviceInfo.get(14);
        oldPassword = deviceInfo.get(15);
        oldIsIss = deviceInfo.get(16);
        boolean value = Boolean.valueOf(oldIsIss);
        oldServerName = deviceInfo.get(17);
        oldState = deviceInfo.get(18);

        deviceTypeBox.getEditor().setText(oldDeviceType);
        deviceGroupBox.getEditor().setText(oldGroupName);
        cameraTypeBox.getEditor().setText(oldCameraType);
        cameraNumField.setText(oldCameraNum);
        vendorBox.getEditor().setText(oldVendorName);
        modelBox.getEditor().setText(oldModelName);
        inventarField.setText(oldInventar);
        serialField.setText(oldSerial);
        connTypeBox.getEditor().setText(oldConnType);
        devicePowerBox.getEditor().setText(oldPowerName);
        deviceSupBox.getEditor().setText(oldDeviceSup);
        deviceContBox.getEditor().setText(oldContractNumber);
        garantField.setText(oldGarant);
        ipAddressField.setText(oldIpAddress);
        loginField.setText(oldLogin);
        passwordField.setText(oldPassword);
        isIssBox.setSelected(value);
        serverBox.getEditor().setText(oldServerName);
        deviceStateBox.getEditor().setText(oldState);
    }

    private void loadLists() {
        loadDeviceTypeList();
        loadDeviceGroupList();
        loadCameraTypeList();
        loadVendorList();
        loadConnectionList();
        loadPowerList();
        loadDeviceSupList();
        loadServerList();
        loadDeviceStateList();

    }

    private void loadDeviceTypeList() {
        deviceTypeList = FXCollections.observableArrayList();
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM device_type ORDER BY deviceType";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int id = resSet.getInt("id");
                if (id != 0)
                    deviceTypeList.add(resSet.getString("deviceType"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка типов устройства", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private void loadDeviceGroupList() {
        deviceGroupList = FXCollections.observableArrayList();
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM device_group ORDER BY groupName";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int id = resSet.getInt("id");
                if (id != 0)
                    deviceGroupList.add(resSet.getString("groupName"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка устройств", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private void loadCameraTypeList() {
        cameraTypeList = FXCollections.observableArrayList();
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM camera_type ORDER BY cameraType";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int id = resSet.getInt("id");
                if (id != 0)
                    cameraTypeList.add(resSet.getString("cameraType"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка назначений камеры", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private void loadVendorList() {
        vendorList = FXCollections.observableArrayList();
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM vendor ORDER BY vendorName";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int id = resSet.getInt("id");
                if (id != 0)
                    vendorList.add(resSet.getString("vendorName"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка производителей оборудования", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private void loadModelList(int vendorId) {
        modelList = FXCollections.observableArrayList();
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM model WHERE vendor_id = ? ORDER BY modelName";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, vendorId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                int id = resSet.getInt("id");
                if (id != 0)
                    modelList.add(resSet.getString("modelName"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка моделей оборудования", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
    }

    private void loadConnectionList() {
        connTypeList = FXCollections.observableArrayList();
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM connection ORDER BY connType";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int id = resSet.getInt("id");
                if (id != 0)
                    connTypeList.add(resSet.getString("connType"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка типов соединений", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private void loadPowerList() {
        powerTypeList = FXCollections.observableArrayList();
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM power ORDER BY powerName";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int id = resSet.getInt("id");
                if (id != 0)
                    powerTypeList.add(resSet.getString("powerName"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка типов питания", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private void loadDeviceSupList() {
        deviceSupList = FXCollections.observableArrayList();
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM device_supply ORDER BY deviceSupName";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int id = resSet.getInt("id");
                if (id != 0)
                    deviceSupList.add(resSet.getString("deviceSupName"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка поставщиков", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private void loadServerList() {
        serverList = FXCollections.observableArrayList();
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM server ORDER BY serverName";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int id = resSet.getInt("id");
                String serverName = resSet.getString("serverName");
                if (id != 0 && !serverName.isEmpty())
                    serverList.add(serverName);
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка серверов", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private void loadDeviceStateList() {
        stateList = FXCollections.observableArrayList();
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM device_state ORDER BY state";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int id = resSet.getInt("id");
                if (id != 0)
                    stateList.add(resSet.getString("state"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка состояний", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private void setListsToBoxes() {
        deviceTypeBox.setItems(deviceTypeList);
        deviceGroupBox.setItems(deviceGroupList);
        cameraTypeBox.setItems(cameraTypeList);
        vendorBox.setItems(vendorList);
        modelBox.setItems(modelList);
        connTypeBox.setItems(connTypeList);
        devicePowerBox.setItems(powerTypeList);
        deviceSupBox.setItems(deviceSupList);
        serverBox.setItems(serverList);
        deviceStateBox.setItems(stateList);
    }

    private void removeListeners() {
        deviceGroupBox.getEditor().textProperty().removeListener(deviceGroupListener);
        vendorBox.getEditor().textProperty().removeListener(vendorListener);
        deviceSupBox.getEditor().textProperty().removeListener(supplyListener);
        isIssBox.selectedProperty().removeListener(isIssListener);
    }

    private void setListners() {
        deviceGroupBox.getEditor().textProperty().addListener(deviceGroupListener);
        vendorBox.getEditor().textProperty().addListener(vendorListener);
        deviceSupBox.getEditor().textProperty().addListener(supplyListener);
        isIssBox.selectedProperty().addListener(isIssListener);
    }

    private void setIpAddressFilter() {
        String regex = makePartialIPRegex();
        final UnaryOperator<TextFormatter.Change> ipAddressFilter = c -> {
            String text = c.getControlNewText();
            if (text.matches(regex)) {
                return c;
            } else {
                return null;
            }
        };
        ipAddressField.setTextFormatter(new TextFormatter<>(ipAddressFilter));
    }

    private String makePartialIPRegex() {
        String partialBlock = "(([01]?[0-9]{0,2})|(2[0-4][0-9])|(25[0-5]))";
        String subsequentPartialBlock = "(\\." + partialBlock + ")";
        String ipAddress = partialBlock + "?" + subsequentPartialBlock + "{0,3}";
        return "^" + ipAddress;
    }

    @FXML
    private void okAction() {
        ArrayList<String> fieldsData = getFieldsData();
        if (!isOldData(fieldsData)) {
            if (isFieldsRight(fieldsData)) {
                checkBoxForNewItems(fieldsData);
                String addQuery = "INSERT INTO device (area_id, groupDevice_id, deviceType_id, cameraType_id, model_id, " +
                        "cameraNum, server_id, ipAddress, connection_id, power_id, deviceContract_id, deviceState_id, " +
                        "iss_conn, login, password, inventar, serial) VALUES (?,?,?,?,?,?,?,INET_ATON(?),?,?,?,?,?,?,?,?,?)";
                String editQuery = "UPDATE device SET area_id = ?, groupDevice_id = ?, deviceType_id = ?, " +
                        "cameraType_id = ?, model_id = ?, cameraNum = ?, server_id = ?, ipAddress = INET_ATON(?), " +
                        "connection_id = ?, power_id = ?, deviceContract_id = ?, deviceState_id = ?, iss_conn = ?, " +
                        "login = ?, password = ?, inventar = ?, serial = ?" +
                        "WHERE id = ?";
                if (isUpdate) {
                    saveDevice(editQuery, fieldsData);
                    closeWindow();
                } else {
                    saveDevice(addQuery, fieldsData);
                    closeWindow();
                }
            }
        } else {
            closeWindow();
        }
    }

    private ArrayList<String> getFieldsData() {
        ArrayList<String> fieldsData = new ArrayList<>();

        String deviceType = deviceTypeBox.getEditor().getText().trim();
        String deviceGroup = deviceGroupBox.getEditor().getText().trim();
        String cameraType = cameraTypeBox.getEditor().getText().trim();
        String cameraNum = cameraNumField.getText().trim();
        String vendor = vendorBox.getEditor().getText().trim();
        String model = modelBox.getEditor().getText().trim();
        String inventar = inventarField.getText().trim();
        String serial = serialField.getText().trim();
        String connType = connTypeBox.getEditor().getText().trim();
        String power = devicePowerBox.getEditor().getText().trim();
        String deviceSupply = deviceSupBox.getEditor().getText().trim();
        String contract = deviceContBox.getEditor().getText().trim();
        String garant = garantField.getText().trim();
        String ipadress = ipAddressField.getText().trim();
        String login = loginField.getText().trim();
        String password = passwordField.getText().trim();
        String isIss = String.valueOf(isIssBox.isSelected());
        String server = serverBox.getEditor().getText().trim();
        String state = deviceStateBox.getEditor().getText().trim();

        fieldsData.add(deviceType);
        fieldsData.add(deviceGroup);
        fieldsData.add(cameraType);
        fieldsData.add(cameraNum);
        fieldsData.add(vendor);
        fieldsData.add(model);
        fieldsData.add(inventar);
        fieldsData.add(serial);
        fieldsData.add(connType);
        fieldsData.add(power);
        fieldsData.add(deviceSupply);
        fieldsData.add(contract);
        fieldsData.add(garant);
        fieldsData.add(ipadress);
        fieldsData.add(login);
        fieldsData.add(password);
        fieldsData.add(isIss);
        fieldsData.add(server);
        fieldsData.add(state);

        return fieldsData;
    }

    private boolean isOldData(ArrayList<String> fieldsData) {
        ArrayList<String> oldFieldsData = new ArrayList<>();
        oldFieldsData.add(oldDeviceType);
        oldFieldsData.add(oldGroupName);
        oldFieldsData.add(oldCameraType);
        oldFieldsData.add(oldCameraNum);
        oldFieldsData.add(oldVendorName);
        oldFieldsData.add(oldModelName);
        oldFieldsData.add(oldInventar);
        oldFieldsData.add(oldSerial);
        oldFieldsData.add(oldConnType);
        oldFieldsData.add(oldPowerName);
        oldFieldsData.add(oldDeviceSup);
        oldFieldsData.add(oldContractNumber);
        oldFieldsData.add(oldGarant);
        oldFieldsData.add(oldIpAddress);
        oldFieldsData.add(oldLogin);
        oldFieldsData.add(oldPassword);
        oldFieldsData.add(oldIsIss);
        oldFieldsData.add(oldServerName);
        oldFieldsData.add(oldState);

        return fieldsData.equals(oldFieldsData);
    }

    private boolean isFieldsRight(ArrayList<String> fieldsData) {
        return !isFieldsEmpty(fieldsData) && isCameraNumRight(fieldsData.get(3)) && isInventarRight(fieldsData.get(6))
                && isSerialRight(fieldsData.get(7)) && isIpAddressRight(fieldsData.get(13));
    }

    private boolean isFieldsEmpty(ArrayList<String> fieldsData) {
        for (int i = 0; i < fieldsData.size(); i++) {
            if (i == 2) i = 4; // if fieldsData.get(1) != камера, then don't check i = 2,3
            if (i == 16 && fieldsData.get(16).equals("false")) i = 18; // if isISS false skip server check
            switch (fieldsData.get(i)) {
                case "":
                    getEmptyFieldMessage(i);
                    return true;
                case "камера":
                    while (i != 3) {
                        i++;
                        if (Check.isFieldNull(fieldsData.get(i))) {
                            getCameraEmptyField(i);
                            return true;
                        }
                    }
            }
        }
        return false;
    }

    private void getEmptyFieldMessage(int index) {
        switch (index) {
            case 0:
                AlertMessage.showEmptyFieldMessage(DEVICETYPEFIELD);
                break;
            case 1:
                AlertMessage.showEmptyFieldMessage(DEVICEFIELD);
                break;
            case 4:
                AlertMessage.showEmptyFieldMessage(VENDORFIELD);
                break;
            case 5:
                AlertMessage.showEmptyFieldMessage(DEVICEMODEL);
                break;
            case 6:
                AlertMessage.showEmptyFieldMessage(INVENTARFIELD);
                break;
            case 7:
                AlertMessage.showEmptyFieldMessage(SERIALFIELD);
                break;
            case 8:
                AlertMessage.showEmptyFieldMessage(CONNECTIONTYPE);
                break;
            case 9:
                AlertMessage.showEmptyFieldMessage(POWERFIELD);
                break;
            case 10:
                AlertMessage.showEmptyFieldMessage(DEVICESUPPLYFIELD);
                break;
            case 11:
                AlertMessage.showEmptyFieldMessage(DEVICECONTRACTFIELD);
                break;
            case 12:
                AlertMessage.showEmptyFieldMessage(GARANTFIELD);
                break;
            case 13:
                AlertMessage.showEmptyFieldMessage(IPFIELD);
                break;
            case 14:
                AlertMessage.showEmptyFieldMessage(LOGINFIELD);
                break;
            case 15:
                AlertMessage.showEmptyFieldMessage(PASSWORDFIELD);
                break;
            case 17:
                AlertMessage.showEmptyFieldMessage(SERVERFIELD);
                break;
            case 18:
                AlertMessage.showEmptyFieldMessage(STATEFIELD);
                break;
        }
    }

    private void getCameraEmptyField(int index) {
        switch (index) {
            case 2:
                AlertMessage.showEmptyFieldMessage(CAMERATYPEFIELD);
                break;
            case 3:
                AlertMessage.showEmptyFieldMessage(CAMERANUMFIELD);
                break;
        }
    }

    private boolean isCameraNumRight(String cameraNum) {
        if (!Check.isNewCameraNum(cameraNum) && !cameraNum.equals(oldCameraNum) && !cameraNum.isEmpty()) {
            AlertMessage.showSameName(CAMERANUMFIELD);
            return false;
        } else return true;
    }

    private boolean isInventarRight(String inventar) {
        if (!Check.isNewInventar(inventar) && !inventar.equals(oldInventar) && !inventar.equals("не задано")) {
            AlertMessage.showSameName(INVENTARFIELD);
            return false;
        } else return true;
    }

    private boolean isSerialRight(String serial) {
        if (!Check.isNewSerial(serial) && !serial.equals(oldSerial) && !serial.equals("не задано")) {
            AlertMessage.showSameName(SERIALFIELD);
            return false;
        } else return true;
    }

    private boolean isIpAddressRight(String ipaddress) {
        return isSubnetFormatValid(ipaddress) && (Check.isNewIp(ipaddress) || ipaddress.equals(oldIpAddress));
    }

    private boolean isSubnetFormatValid(String ipAddress) {
        Pattern pattern;
        Matcher matcher;
        String IPADDRESS_PATTERN
                = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
                + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
                + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
                + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
        pattern = Pattern.compile(IPADDRESS_PATTERN);
        matcher = pattern.matcher(ipAddress);
        if (!matcher.matches()){
            AlertMessage.showSubnetWrongFormat(IPFIELD);
            return false;
        } else return true;
    }

    private void checkBoxForNewItems(ArrayList<String> fieldsData) {
        if (Check.isNewDeviceType(fieldsData.get(0))) {
            AddEditDeviceTypeController deviceTypeController = new AddEditDeviceTypeController();
            deviceTypeController.addDeviceType(fieldsData.get(0));
        }
        if (Check.isNewGroup(fieldsData.get(1))) {
            AddEditDeviceGroupController deviceGroupController = new AddEditDeviceGroupController();
            deviceGroupController.addGroup(fieldsData.get(1));
        }
        if (Check.isNewCameraType(fieldsData.get(2))) {
            AddEditCameraTypeController cameraTypeController = new AddEditCameraTypeController();
            cameraTypeController.addCameraType(fieldsData.get(2));
        }
        if (Check.isNewVendorName(fieldsData.get(4))) {
            AddEditVendorController vendorController = new AddEditVendorController();
            vendorController.addDeviceVendor(fieldsData.get(4));
        }
        if (Check.isNewModelName(fieldsData.get(4), fieldsData.get(5))) {
            AddEditModelController modelController = new AddEditModelController();
            modelController.addModel(fieldsData.get(4), fieldsData.get(5));
        }
        if (Check.isNewConnType(fieldsData.get(8))) {
            AddEditConnectionController connectionController = new AddEditConnectionController();
            connectionController.addConnection(fieldsData.get(8));
        }
        if (Check.isNewPowerName(fieldsData.get(9))) {
            AddEditPowerController powerController = new AddEditPowerController();
            powerController.addPower(fieldsData.get(9));
        }
        if (Check.isNewDeviceSupply(fieldsData.get(10))) {
            AddEditDeviceSupplyController deviceSupplyController = new AddEditDeviceSupplyController();
            deviceSupplyController.addDeviceSupply(fieldsData.get(10));
        }
        AddEditDeviceContController deviceContController = new AddEditDeviceContController();
        int deviceSupId = deviceContController.findDeviceSupplyId(fieldsData.get(10));
        if (Check.isNewDeviceContract(deviceSupId, fieldsData.get(11))) {
            deviceContController.addGroup(fieldsData.get(10), fieldsData.get(11), fieldsData.get(12));
        }
        if (Check.isNewServerName(fieldsData.get(17))) {
            AddEditServerController serverController = new AddEditServerController();
            serverController.addServerName(fieldsData.get(17));
        }
        if (Check.isNewState(fieldsData.get(18))) {
            AddEditDeviceStateController stateController = new AddEditDeviceStateController();
            stateController.addState(fieldsData.get(18));
        }
    }

    //TODO: 2 fields not added: attachment and describe. At view also. Dont't forget about query!!!
    private void saveDevice(String query, ArrayList<String> fieldsData) {
        Connection conn = null;
        PreparedStatement preState = null;
        int deviceGroupId = getDeviceGroupId(fieldsData.get(1));
        int deviceTypeId = getDeviceTypeId(fieldsData.get(0));
        int cameraTypeId = getCameraTypeId(fieldsData.get(2));
        int vendorId = getVendorId(fieldsData.get(4));
        int modelId = getModelId(vendorId, fieldsData.get(5));
        String cameraNum = fieldsData.get(3);
        int serverId = getServerId(fieldsData.get(17));
        String ipaddress = fieldsData.get(13);
        int connectionId = getConnectionId(fieldsData.get(8));
        int powerId = getPowerId(fieldsData.get(9));
        int supplyId = getSupplyId(fieldsData.get(10));
        int deviceContractId = getDeviceContractId(supplyId, fieldsData.get(11));
        int deviceState = getDeviceStateId(fieldsData.get(18));
        boolean isIss = Boolean.valueOf(fieldsData.get(16));
        String login = fieldsData.get(14);
        String password = fieldsData.get(15);
        String inventar = fieldsData.get(6);
        String serial = fieldsData.get(7);
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, areaId);
            preState.setInt(2, deviceGroupId);
            preState.setInt(3, deviceTypeId);
            preState.setInt(4, cameraTypeId);
            preState.setInt(5, modelId);
            preState.setString(6, cameraNum);
            preState.setInt(7, serverId);
            preState.setString(8, ipaddress);
            preState.setInt(9, connectionId);
            preState.setInt(10, powerId);
            preState.setInt(11, deviceContractId);
            preState.setInt(12, deviceState);
            preState.setBoolean(13, isIss);
            preState.setString(14, login);
            preState.setString(15, password);
            preState.setString(16, inventar);
            preState.setString(17, serial);
            if (isUpdate) {
                preState.setInt(18, deviceId);
            }
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при сохранении устройства", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private int getDeviceGroupId(String groupName) {
        String query = "SELECT id FROM device_group WHERE groupName = ?";
        String log = "Ошибка при поиске ID устройства";
        return Query.getIdQuery(groupName, query, log);
    }

    private int getDeviceTypeId(String deviceType) {
        String query = "SELECT id FROM device_type WHERE deviceType = ?";
        String log = "Ошибка при поиске ID типа устройства";
        return Query.getIdQuery(deviceType, query, log);
    }

    private int getCameraTypeId(String cameraType) {
        String query = "SELECT id FROM camera_type WHERE cameraType = ?";
        String log = "Ошибка при поиске ID типа камеры";
        return Query.getIdQuery(cameraType, query, log);
    }

    private int getModelId(int vendorId, String model) {
        String query = "SELECT id FROM model WHERE vendor_id = ? AND modelName = ?";
        String log = "Ошибка при поиске ID типа камеры";
        return Query.getIdQuery(vendorId, model, query, log);
    }

    private int getServerId(String serverName) {
        String query = "SELECT id FROM server WHERE serverName = ?";
        String log = "Ошибка при поиске ID сервера";
        return Query.getIdQuery(serverName, query, log);
    }

    private int getConnectionId(String connection) {
        String query = "SELECT id FROM connection WHERE connType = ?";
        String log = "Ошибка при поиске ID подключения";
        return Query.getIdQuery(connection, query, log);
    }

    private int getPowerId(String power) {
        String query = "SELECT id FROM power WHERE powerName = ?";
        String log = "Ошибка при поиске ID питания";
        return Query.getIdQuery(power, query, log);
    }

    private int getDeviceContractId(int supplyId, String deviceContract) {
        String query = "SELECT id FROM device_contract WHERE deviceSupply_id = ? AND contractNumber = ?";
        String log = "Ошибка при поиске ID контракта на поставку оборудования";
        return Query.getIdQuery(supplyId, deviceContract, query, log);
    }

    private int getDeviceStateId(String deviceState) {
        String query = "SELECT id FROM device_state WHERE state = ?";
        String log = "Ошибка при поиске ID состояния устройства";
        return Query.getIdQuery(deviceState, query, log);
    }

    @FXML
    private void closeWindow() {
        stage.close();
        DeviceTreeController deviceTreeController = Main.deviceTreeController;
        deviceTreeController.initialize();
    }
}
