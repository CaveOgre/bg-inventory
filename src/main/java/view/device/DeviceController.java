package view.device;

import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import mainpackage.Main;
import view.DeviceTreeController;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DeviceController {

    public static int deviceId;
    public static int areaId;

    @FXML
    private TextField deviceTypeField;
    @FXML
    private TextField deviceGroupField;
    @FXML
    private TextField cameraTypeField;
    @FXML
    private TextField cameraNumField;
    @FXML
    private TextField vendorField;
    @FXML
    private TextField modelField;
    @FXML
    private TextField inventarField;
    @FXML
    private TextField serialField;
    @FXML
    private TextField connTypeField;
    @FXML
    private TextField devicePowerField;
    @FXML
    private TextField deviceSupField;
    @FXML
    private TextField deviceContField;
    @FXML
    private TextField garantField;
    @FXML
    private TextField ipAddressField;
    @FXML
    private TextField loginField;
    @FXML
    private TextField passwordField;
    @FXML
    private CheckBox isIssBox;
    @FXML
    private TextField serverField;
    @FXML
    private TextField deviceStateField;
    @FXML
    private Button addBtn;
    @FXML
    private Button editBtn;
    @FXML
    private Button delBtn;
    @FXML
    private AnchorPane test;

    private DbConnection dbConnection = new DbConnection();

    private ArrayList<String> deviceInfoList;

    @FXML
    private void initialize() {
        getDeviceInfo();
        setDeviceInfo();
    }

    private void getDeviceInfo() {
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT deviceType, groupName, cameraType, cameraNum, vendorName, modelName, inventar,\n" +
                "`serial`, connType,  powerName, deviceSupName, contractNumber, garant,\n" +
                "(INET_NTOA(ipAddress)) as ipAddress, login, `password`, iss_conn, serverName, state\n" +
                "FROM device\n" +
                "LEFT JOIN device_type ON device.deviceType_id = device_type.id\n" +
                "LEFT JOIN device_group ON device.groupDevice_id = device_group.id\n" +
                "LEFT JOIN camera_type ON device.cameraType_id = camera_type.id\n" +
                "LEFT JOIN model ON device.model_id = model.id\n" +
                "LEFT JOIN vendor ON model.vendor_id = vendor.id\n" +
                "LEFT JOIN connection ON device.connection_id = connection.id\n" +
                "LEFT JOIN power ON device.power_id = power.id\n" +
                "LEFT JOIN device_contract ON device.deviceContract_id = device_contract.id\n" +
                "LEFT JOIN device_supply ON device_contract.deviceSupply_id = device_supply.id\n" +
                "LEFT JOIN `server` ON device.server_id = `server`.id\n" +
                "LEFT JOIN device_state ON device.deviceState_id = device_state.id\n" +
                "WHERE device.id = ?";
        deviceInfoList = new ArrayList<>();
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, deviceId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                String deviceType = resSet.getString("deviceType");
                String groupName = resSet.getString("groupName");
                String cameraType = resSet.getString("cameraType");
                String cameraNum = resSet.getString("cameraNum");
                String vendorName = resSet.getString("vendorName");
                String modelName = resSet.getString("modelName");
                String inventar = resSet.getString("inventar");
                String serial = resSet.getString("serial");
                String connType = resSet.getString("connType");
                String powerName = resSet.getString("powerName");
                String deviceSup = resSet.getString("deviceSupName");
                String contractNumber = resSet.getString("contractNumber");
                String garant = resSet.getString("garant");
                String ipAddress = resSet.getString("ipAddress");
                String login = resSet.getString("login");
                String password = resSet.getString("password");
                boolean issConn = resSet.getBoolean("iss_conn");
                String serverName = resSet.getString("serverName");
                String state = resSet.getString("state");
                deviceInfoList.add(deviceType);
                deviceInfoList.add(groupName);
                deviceInfoList.add(cameraType);
                deviceInfoList.add(cameraNum);
                deviceInfoList.add(vendorName);
                deviceInfoList.add(modelName);
                deviceInfoList.add(inventar);
                deviceInfoList.add(serial);
                deviceInfoList.add(connType);
                deviceInfoList.add(powerName);
                deviceInfoList.add(deviceSup);
                deviceInfoList.add(contractNumber);
                deviceInfoList.add(garant);
                deviceInfoList.add(ipAddress);
                deviceInfoList.add(login);
                deviceInfoList.add(password);
                deviceInfoList.add(String.valueOf(issConn));
                deviceInfoList.add(serverName);
                deviceInfoList.add(state);
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка во время загрузки информации по устройству.", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
    }

    private void setDeviceInfo() {
        deviceTypeField.setText(deviceInfoList.get(0));
        deviceGroupField.setText(deviceInfoList.get(1));
        cameraTypeField.setText(deviceInfoList.get(2));
        cameraNumField.setText(deviceInfoList.get(3));
        vendorField.setText(deviceInfoList.get(4));
        modelField.setText(deviceInfoList.get(5));
        inventarField.setText(deviceInfoList.get(6));
        serialField.setText(deviceInfoList.get(7));
        connTypeField.setText(deviceInfoList.get(8));
        devicePowerField.setText(deviceInfoList.get(9));
        deviceSupField.setText(deviceInfoList.get(10));
        deviceContField.setText(deviceInfoList.get(11));
        garantField.setText(deviceInfoList.get(12));
        ipAddressField.setText(deviceInfoList.get(13));
        loginField.setText(deviceInfoList.get(14));
        passwordField.setText(deviceInfoList.get(15));
        boolean issConn = Boolean.valueOf(deviceInfoList.get(16));
        isIssBox.setSelected(issConn);
        serverField.setText(deviceInfoList.get(17));
        deviceStateField.setText(deviceInfoList.get(18));
    }

    @FXML
    public void goToAddView() {
        String title = "Добавление";

        Stage stage = createNewWindow(title);
        AddEditDeviceController.setStage(stage);
        AddEditDeviceController.setAreaId(areaId);

        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    public void goToEditView() {
        String title = "Редактирование";

        AddEditDeviceController.setDeviceInfo(deviceInfoList);
        AddEditDeviceController.setIsUpdate(true);
        AddEditDeviceController.setDeviceId(deviceId);
        AddEditDeviceController.setAreaId(areaId);

        Stage stage = createNewWindow(title);
        AddEditDeviceController.setStage(stage);

        assert stage != null;
        stage.showAndWait();
        AddEditDeviceController.setIsUpdate(false);

        initialize();
    }

    private Stage createNewWindow(String title) {
        String view = "/AddEditDevice.fxml";
        String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        return Main.loadStage(toView, title, logText);
    }

    @FXML
    public void deleteDevice() {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "DELETE FROM device WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, deviceId);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при удалении устройства", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
        DeviceTreeController deviceTreeController = Main.deviceTreeController;
        deviceTreeController.initialize();
    }
}
