package view.vendor;

import dbpackage.DbConnection;
import model.Vendor;
import view.AlertMessage;
import view.Check;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class AddEditVendorController {

    @FXML
    private Label vendorLabel;

    @FXML
    private TextField vendorField;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;

    private DbConnection dbConnection = new DbConnection();

    private static Stage stage;
    private static boolean isUpdate;
    private static Vendor vendor;

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    static void setVendor(Vendor outVendor) {
        vendor = outVendor;
    }

    private String oldVendor = "";

    @FXML
    private void initialize() {
        if (isUpdate) {
            oldVendor = vendor.getVendorName();
            vendorField.setText(oldVendor);
        }
    }

    @FXML
    private void okAction() {
        String vendorName = vendorField.getText().trim();
        if (isFieldRight(vendorName)) {
            if (isUpdate) {
                updateDeviceVendor(vendorName);
                closeWindow();
            } else {
                addDeviceVendor(vendorName);
                closeWindow();
            }
        } else vendorField.setText(vendorName);
    }

    public void addDeviceVendor(String vendorNameToAdd) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO vendor (vendorName) VALUES (?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, vendorNameToAdd);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении имени производителя", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void updateDeviceVendor(String vendorNameToUpdate) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "UPDATE vendor SET vendorName = ? WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, vendorNameToUpdate);
            preState.setInt(2, vendor.getId());
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при редактировании имени производителя", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private boolean isFieldRight(String vendorName) {
        if (!Check.isFieldNull(vendorName)) {
            if (!oldVendor.equals(vendorName))
                if (!Check.isNewVendorName(vendorName)) {
                    AlertMessage.showSameName(vendorLabel.getText());
                    return false;
                }
            return true;
        } else {
            AlertMessage.showEmptyFieldMessage(vendorLabel.getText());
            return false;
        }
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}
