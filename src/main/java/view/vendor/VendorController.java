package view.vendor;

import view.Check;
import model.Vendor;
import mainpackage.Main;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import view.Query;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class VendorController {

    @FXML
    private ListView<Vendor> vendorListView;

    @FXML
    private Button addBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button delBtn;

    @FXML
    private Button exitBtn;

    private DbConnection dbConnection = new DbConnection();

    private ObservableList<Vendor> vendorList;

    private static Stage stage;

    public static void setStage(Stage outStage) {
        stage = outStage;
    }

    @FXML
    private void initialize() {
        vendorList = FXCollections.observableArrayList();
        loadVendorList();
        vendorListView.setItems(vendorList);
        vendorListView.getSelectionModel().selectFirst();
    }

    @FXML
    private void goToAddView() {
        int selectedIndex = vendorListView.getSelectionModel().getSelectedIndex();
        String title = "Редактирование";

        Stage stage = createNewWindow(title);
        AddEditVendorController.setStage(stage);

        stage.showAndWait();

        initialize();
        vendorListView.getSelectionModel().select(selectedIndex);
    }

    @FXML
    private void goToEditView() {
        if (Check.isSelectedListView(vendorListView)) {
            int selectedIndex = vendorListView.getSelectionModel().getSelectedIndex();
            Vendor vendorToUpdate = vendorListView.getSelectionModel().getSelectedItem();
            AddEditVendorController.setVendor(vendorToUpdate);
            String title = "Редактирование";

            AddEditVendorController.setIsUpdate(true);
            Stage stage = createNewWindow(title);
            AddEditVendorController.setStage(stage);

            stage.showAndWait();
            AddEditVendorController.setIsUpdate(false);

            initialize();
            vendorListView.getSelectionModel().select(selectedIndex);
        }
    }

    @FXML
    private void deleteDeviceVendor() {
        if (Check.isSelectedListView(vendorListView)) {
            int selectedIndex = vendorListView.getSelectionModel().getSelectedIndex();
            Connection conn = null;
            PreparedStatement preState = null;
            Vendor vendorToDel = vendorListView.getSelectionModel().getSelectedItem();
            String query = "DELETE FROM vendor WHERE id = ?";
            String updateQuery = "UPDATE model SET vendor_id = 0 WHERE vendor_id = ?";
            Query.updateDeleteItem(updateQuery, vendorToDel.getId());
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setInt(1, vendorToDel.getId());
                preState.executeUpdate();
                initialize();
                vendorListView.getSelectionModel().select(Check.selectIndex(selectedIndex));
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при удалении имени производителя", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        } else AlertMessage.showNotSelectedElement();
    }

    private void loadVendorList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM vendor";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int vendorId = resSet.getInt("id");
                if (vendorId != 0)
                    vendorList.add(new Vendor(vendorId,
                            resSet.getString("vendorName")));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка производителей", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private Stage createNewWindow(String title) {
        String view = "/AddEditVendor.fxml";
        String errorText = "Ошибка при создани окна \"" + title + "\"";
        URL toView = AddEditVendorController.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, errorText);
        AddEditVendorController.setStage(stage);

        return stage;
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}
