package view;

import javafx.scene.input.KeyCombination;
import model.Area;
import model.Device;
import dbpackage.DbConnection;
import view.area.AreaInfoController;
import view.device.DeviceController;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TreeView;
import javafx.scene.control.TreeItem;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;
import javafx.collections.FXCollections;
import javafx.scene.control.ContextMenu;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class DeviceTreeController {

    private DbConnection dbConnection = new DbConnection();

    private ObservableList<Device> deviceList;
    private ObservableList<Area> areaList;

    @FXML
    private TreeView<Object> deviceTree;

    @FXML
    private SplitPane splitPane;

    private ChangeListener<TreeItem<Object>> deviceTreeListener = (observable, oldItem, newItem) -> {
        if (newItem != null) {
            if (newItem.getValue() instanceof Area) {
                int areaId = ((Area) newItem.getValue()).getId();
                AreaInfoController.areaId = areaId;
                DeviceController.areaId = areaId;
                splitPane.getItems().set(1, showAreaView());
            } else {
                DeviceController.areaId = ((Area) newItem.getParent().getValue()).getId();
                DeviceController.deviceId = ((Device) newItem.getValue()).getId();
                splitPane.getItems().set(1, showDeviceView());
            }
        }
    };

    //TODO: conditions for shortcuts
    private void setContextMenu() {
        DeviceController deviceController = new DeviceController();
        AreaInfoController areaInfoController = new AreaInfoController();
        ContextMenu contextMenu = new ContextMenu();
        MenuItem addAreaItem = new MenuItem("Добавить площадку");
        MenuItem addDeviceItem = new MenuItem("Добавить устройство");
        addAreaItem.setOnAction(event -> {
            areaInfoController.goToAddView();
        });
        addAreaItem.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));
        addDeviceItem.setOnAction(event -> {
            deviceController.goToAddView();
        });
        addDeviceItem.setAccelerator(KeyCombination.keyCombination("Ctrl+Shift+N"));
        MenuItem editAreaItem = new MenuItem("Редактировать площадку");
        MenuItem editDeviceItem = new MenuItem("Редактировать устройство");
        editAreaItem.setOnAction(event -> {
            areaInfoController.goToEditView();
        });
        editAreaItem.setAccelerator(KeyCombination.keyCombination("Ctrl+E"));
        editDeviceItem.setOnAction(event -> {
            deviceController.goToEditView();
        });
        editDeviceItem.setAccelerator(KeyCombination.keyCombination("Ctrl+Shift+E"));
        MenuItem delAreaItem = new MenuItem("Удалить площадку");
        MenuItem delDeviceItem = new MenuItem("Удалить площадку");
        delAreaItem.setOnAction(event -> {
                areaInfoController.deleteArea();
        });
        delDeviceItem.setOnAction(event -> {
            deviceController.deleteDevice();
        });
        contextMenu.getItems().addAll(addAreaItem, addDeviceItem, editAreaItem,
                editDeviceItem, delAreaItem, delDeviceItem);
        deviceTree.setContextMenu(contextMenu);
    }

    @FXML
    public void initialize() {
        deviceTree.getSelectionModel().selectedItemProperty()
                .removeListener(deviceTreeListener);

        getDeviceList();
        getAreaList();

        TreeItem<Object> root = new TreeItem<>();
        root.setExpanded(true);
        for (Area area : areaList) {
            TreeItem<Object> areaBranch = makeAreaBranch(area, root);
            for (Device device : deviceList) {
                if (isInArea(device, area)) {
                    makeDevcieBranch(device, areaBranch);
                }
            }
        }

        deviceTree.setRoot(root);
        deviceTree.setShowRoot(false);
        deviceTree.getSelectionModel().selectedItemProperty()
                .addListener(deviceTreeListener);
        setContextMenu();
    }

    private void getAreaList() {
        areaList = FXCollections.observableArrayList();
        Connection conn = null;
        ResultSet resSet = null;
        final String query = "SELECT*FROM area";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int id = resSet.getInt("id");
                if (id != 0) {
                    areaList.add(new Area(id, resSet.getString("areaName"),
                            resSet.getInt("city_id"), resSet.getInt("fStreet_id"),
                            resSet.getString("fStreetBuilding"), resSet.getBoolean("crossroad"),
                            resSet.getInt("sStreet_id"), resSet.getString("sStreetBuilding"),
                            resSet.getInt("powerPoint_id"), resSet.getInt("bgStage"),
                            resSet.getShort("year"), resSet.getInt("areaState_id")));
                }
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке информации по площадке", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private void getDeviceList() {
        deviceList = FXCollections.observableArrayList();
        Connection conn = null;
        ResultSet resSet = null;
        final String query = "SELECT*FROM device ORDER BY cameraNum";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int id = resSet.getInt("id");
                if (id != 0) {
                    deviceList.add(new Device(id, resSet.getInt("area_id"),
                            resSet.getInt("groupDevice_id"), resSet.getInt("deviceType_id"),
                            resSet.getInt("cameraType_id"), resSet.getInt("model_id"),
                            resSet.getString("cameraNum"), resSet.getInt("server_id"),
                            resSet.getInt("ipAddress"), resSet.getInt("connection_id"),
                            resSet.getInt("power_id"), resSet.getString("attachment"),
                            resSet.getInt("deviceContract_id"), resSet.getInt("deviceState_id"),
                            resSet.getBoolean("iss_conn"), resSet.getString("login"),
                            resSet.getString("password"), resSet.getString("inventar"),
                            resSet.getString("serial"), resSet.getString("describe")));
                }
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке информации по устройству", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private TreeItem<Object> makeAreaBranch(Object area, TreeItem<Object> root) {
        TreeItem<Object> item = new TreeItem<>(area);
        item.setExpanded(true);
        root.getChildren().add(item);
        return item;
    }

    private void makeDevcieBranch(Object device, TreeItem<Object> areaParent) {
        TreeItem<Object> item = new TreeItem<>(device);
        item.setExpanded(true);
        areaParent.getChildren().add(item);
    }

    private Boolean isInArea(Device device, Area area) {
        return device.getAreaId() == area.getId();
    }

    private AnchorPane showAreaView() {
        try {
            FXMLLoader viewAreaLoader = new FXMLLoader(getClass().getResource("/AreaInfo.fxml"));
            return viewAreaLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private AnchorPane showDeviceView() {
        try {
            FXMLLoader viewDeviceLoader = new FXMLLoader(getClass().getResource("/Device.fxml"));
            return viewDeviceLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}