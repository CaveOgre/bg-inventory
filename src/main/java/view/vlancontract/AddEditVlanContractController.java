package view.vlancontract;

import view.Check;
import view.AlertMessage;
import model.VlanContract;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class AddEditVlanContractController {

    @FXML
    private ComboBox<String> providerBox;

    @FXML
    private Label providerLabel;

    @FXML
    private Label vlanContractLabel;

    @FXML
    private TextField vlanContractField;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;

    private ObservableList<String> providerList;

    private DbConnection dbConnection = new DbConnection();

    private static Stage stage;
    private static boolean isUpdate;
    private static VlanContract vlanContract;

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    static void setVlanContract(VlanContract outVlanContract) {
        vlanContract = outVlanContract;
    }

    private String oldProvider = "";
    private String oldContract = "";

    @FXML
    private void initialize() {
        providerList = FXCollections.observableArrayList();
        loadProviderList();

        providerBox.setItems(providerList);

        if (isUpdate) {
            oldContract = vlanContract.getVlanContNumber();
            vlanContractField.setText(oldContract);
            int providerId = vlanContract.getProviderId();
            oldProvider = findProviderName(providerId);
            providerBox.getSelectionModel().select(oldProvider);
        }
    }

    @FXML
    private void okAction() {
        String providerBoxText = providerBox.getEditor().getText().trim();
        String vlanContFieldText = vlanContractField.getText().trim();
        if (isFieldsRight(providerBoxText, vlanContFieldText)) {
            if (isUpdate) {
                updateVlanContract(providerBoxText, vlanContFieldText);
            } else addVlanContract(providerBoxText, vlanContFieldText);
        } else {
            providerBox.getEditor().setText(providerBoxText);
            vlanContractField.setText(vlanContFieldText);
        }
    }

    private void addVlanContract(String providerFieldText, String vlanContractFieldText) {
        Connection conn = null;
        PreparedStatement preState = null;
        int providerId = findProviderId(providerFieldText);
        String query = "INSERT INTO vlan_contract (provider_id, vlanContNumber) VALUES (?, ?)";
        if (!Check.isFieldNull(vlanContractFieldText)) {
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setInt(1, providerId);
                preState.setString(2, vlanContractFieldText.trim());
                preState.executeUpdate();
                closeWindow();
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при добавлении номера контракта", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        } else AlertMessage.showEmptyFieldMessage(vlanContractLabel.getText());
    }

    private void updateVlanContract(String providerBoxText, String vlanContFieldText) {

        int idProviderToUpdate = findProviderId(providerBoxText);
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "UPDATE vlan_contract SET provider_id = ?, vlanContNumber = ? WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, idProviderToUpdate);
            preState.setString(2, vlanContFieldText);
            preState.setInt(3, vlanContract.getId());
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при редактировании номер контракта", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void createNewProvider(String providerName) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO provider (providerName)VALUES (?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, providerName);
            preState.executeUpdate();
            initialize();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении новго провайдера " +
                    "во время изменения контракта Vlan", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void loadProviderList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM provider";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.createStatement().executeQuery(query);
            while (resSet.next()) {
                providerList.add(resSet.getString("providerName"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка провайдеров " +
                    "во время изменения контракта по Vlan", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private String findProviderName(int providerId) {
        String provider = "";
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT providerName FROM provider WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, providerId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                provider = resSet.getString("providerName");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске провайдера во время изменения контракта на Vlan", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return provider;
    }

    private int findProviderId(String providerName) {
        int providerId = 0;
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT id FROM provider WHERE providerName = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, providerName);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                providerId = resSet.getInt("id");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске провайдера во время " +
                    "изменения контракта на Vlan", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return providerId;
    }

    private boolean isFieldsRight(String providerName, String vlanCont) {
        String providerLabelText = providerLabel.getText().trim();
        String vlanContLaberlText = vlanContractLabel.getText().trim();
        int providerId = findProviderId(providerName);
        if (!Check.isFieldNull(providerName)) {
            if (!oldProvider.equals(providerName)) {
                if (Check.isNewProvider(providerName)) {
                    if (AlertMessage.isOkNewValue(providerLabel.getText())) {
                        createNewProvider(providerName);
                        providerBox.getSelectionModel().select(providerName);
                    } else return false;
                }
            }
            if (!Check.isFieldNull(vlanCont)) {
                if (!oldProvider.equals(providerName) || !oldContract.equals(vlanCont))
                    if (Check.isNewVlanContract(providerId, vlanCont)) {
                        AlertMessage.showSameName(vlanContLaberlText);
                        return false;
                    }
                return true;
            } else {
                AlertMessage.showEmptyFieldMessage(vlanContLaberlText);
                return false;
            }
        } else {
            AlertMessage.showEmptyFieldMessage(providerLabelText);
            return false;
        }
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}
