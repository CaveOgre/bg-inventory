package view.vlancontract;

import view.Check;
import mainpackage.Main;
import view.AlertMessage;
import model.VlanContract;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class VlanContractController {

    @FXML
    private ListView<VlanContract> vlanContractView;

    @FXML
    private Button addBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button delBtn;

    @FXML
    private Button exitBtn;

    private DbConnection dbConnection = new DbConnection();

    private ObservableList<VlanContract> vlanContractList;

    private static Stage stage;

    public static void setStage(Stage outStage) {
        stage = outStage;
    }

    @FXML
    private void initialize() {
        vlanContractList = FXCollections.observableArrayList();
        loadVlanContracList();

        vlanContractView.setItems(vlanContractList);
        vlanContractView.getSelectionModel().selectFirst();
    }

    @FXML
    private void goToAddView() {
        int selectedIndex = vlanContractView.getSelectionModel().getSelectedIndex();
        String title = "Добавление";

        Stage stage = createNewWindow(title);

        assert stage != null;
        stage.showAndWait();

        initialize();
        vlanContractView.getSelectionModel().select(selectedIndex);
    }

    @FXML
    private void goToEditView() {
        if (Check.isSelectedListView(vlanContractView)) {
            int selectedIndex = vlanContractView.getSelectionModel().getSelectedIndex();
            String title = "Редактирование";

            VlanContract vlanContract = vlanContractView.getSelectionModel().getSelectedItem();
            AddEditVlanContractController.setVlanContract(vlanContract);
            AddEditVlanContractController.setIsUpdate(true);

            Stage stage = createNewWindow(title);

            assert stage != null;
            stage.showAndWait();
            AddEditVlanContractController.setIsUpdate(false);

            initialize();
            vlanContractView.getSelectionModel().select(selectedIndex);
        } else AlertMessage.showNotSelectedElement();
    }

    @FXML
    private void deleteVlanContract() {
        if (Check.isSelectedListView(vlanContractView)) {
            Connection conn = null;
            PreparedStatement preState = null;
            String query = "DELETE FROM vlan_contract WHERE id = ?";
            int selectedIndex = vlanContractView.getSelectionModel().getSelectedIndex();
            VlanContract vlanContract = vlanContractView.getSelectionModel().getSelectedItem();
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setInt(1, vlanContract.getId());
                preState.executeUpdate();
                initialize();
                vlanContractView.getSelectionModel().select(Check.selectIndex(selectedIndex));
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при удалении контракта на Vlan", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        } else AlertMessage.showNotSelectedElement();
    }

    private Stage createNewWindow(String title) {
        String view = "/AddEditVlanContract.fxml";
        String errorText = "Ошибка при создани окна \"" + title + "\"";
        URL toView = AddEditVlanContractController.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, errorText);
        AddEditVlanContractController.setStage(stage);

        return stage;
    }

    private void loadVlanContracList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM vlan_contract";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int id = resSet.getInt("id");
                if (id != 0) {
                    vlanContractList.add(new VlanContract(resSet.getInt("id"),
                            resSet.getInt("provider_id"),
                            resSet.getString("vlanContNumber")));
                }
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка контрактов на Vlan", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }

}
