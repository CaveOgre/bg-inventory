package view;

import dbpackage.DbConnection;

import javafx.scene.control.TreeView;
import javafx.scene.control.ListView;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableView;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;

public class Check {

    private static DbConnection dbConnection = new DbConnection();

    public static boolean isFieldNull(String value) {
        if (value != null) {
            int simbolCount = value.trim().length();
            return simbolCount == 0;
        }
        return true;
    }

    public static boolean isComboBoxNull(ComboBox<String> comboBox) {
        return comboBox.getSelectionModel().isEmpty();
    }

    public static boolean isSelectedTableView(TableView tableView) {
        int selectedIndex = tableView.getSelectionModel().getSelectedIndex();
        return (selectedIndex >= 0);
    }

    public static boolean isSelectedTreeView(TreeView treeView) {
        int selectedIndex = treeView.getSelectionModel().getSelectedIndex();
        return (selectedIndex >= 0);
    }

    public static boolean isSelectedChoiceBox(ChoiceBox choiceBox) {
        int selectedIndex = choiceBox.getSelectionModel().getSelectedIndex();
        return (selectedIndex >= 0);
    }

    public static boolean isSelectedListView(ListView listView) {
        int selectedIndex = listView.getSelectionModel().getSelectedIndex();
        return (selectedIndex >= 0);
    }

    public static int selectIndex(int selectedIndex) {
        if (selectedIndex - 1 < 0) return selectedIndex;
        return selectedIndex - 1;
    }

    public static boolean isDigit(String textField) {
        for (int i = 0; i < textField.length(); i++) {
            if (!Character.isDigit(textField.charAt(i))) return false;
        }
        return true;
    }

    public static boolean isNewProvider(String providerToCompare) {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT providerName FROM provider";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                String providerName = resSet.getString("providerName");
                if (providerToCompare.equals(providerName))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликатов имени провайдера", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return true;
    }

    public static boolean isNewVlanContract(int providerId, String vlanContToCompare) {
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT vlanContNumber FROM vlan_contract WHERE provider_id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, providerId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                String vlanCont = resSet.getString("vlanContNumber");
                if (vlanContToCompare.equals(vlanCont))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка во время поиска дубликатов номера контрактов", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return true;
    }

    public static boolean isNewVlan(short vlanIdToCompare) {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT vlanId FROM vlan";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                Short vlanId = resSet.getShort("vlanId");
                if (vlanIdToCompare == vlanId)
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликата VLAN", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return true;
    }

    public static boolean isNewVendorName(String deviceVendorName) {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT vendorName FROM vendor";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                String vendorName = resSet.getString("vendorName");
                if (deviceVendorName.equals(vendorName))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске совпадения имени производителя", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return true;
    }

    public static boolean isNewServerName(String serverNameToCompare) {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT serverName FROM server";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                String serverName = resSet.getString("serverName");
                if (serverNameToCompare.equals(serverName))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликатов имени сервера", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return true;
    }

    public static boolean isNewPowerSupp(String supplyToCompare) {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT powerSupName FROM power_supply";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                String powerSupply = resSet.getString("powerSupName");
                if (supplyToCompare.equals(powerSupply))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дублекато поставщиков электричества", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return true;
    }

    public static boolean isNewPhone(String phoneToCompare) {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT phone FROM power_supply";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                String phone = resSet.getString("phone");
                if (phoneToCompare.equals(phone))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дублекато поставщиков электричества", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return true;
    }

    public static boolean isNewPowerPoint(String pointNameToCompare, int powerSupId) {
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT pointName FROM power_point WHERE powerSupply_id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, powerSupId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                String pointName = resSet.getString("pointName");
                if (pointNameToCompare.equals(pointName))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликата точки питания", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return true;
    }

    public static boolean isNewPowerName(String powerNameToCompare) {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT powerName FROM power";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                String powerName = resSet.getString("powerName");
                if (powerNameToCompare.equals(powerName))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликатов типа питания", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return true;
    }

    public static boolean isNewModelName(String vendorName, String modelNameToCompare) {
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        int vendorId = getVendorId(vendorName);
        String query = "SELECT modelName FROM model WHERE vendor_id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, vendorId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                String model = resSet.getString("modelName");
                if (modelNameToCompare.equals(model))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликата модели устройства", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return true;
    }

    private static int getVendorId(String vendorName) {
        int vendorId = 0;
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT id FROM vendor WHERE vendorName = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, vendorName);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                vendorId = resSet.getInt("id");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске ID производителя оборудования", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return vendorId;
    }

    public static boolean isNewDeviceType(String typeTitleToCompare) {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT deviceType FROM device_type";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                String typeName = resSet.getString("deviceType");
                if (typeTitleToCompare.equals(typeName))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликатов типа оборудования", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return true;
    }

    public static boolean isNewDeviceSupply(String deviceSupNameToCompare) {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT deviceSupName FROM device_supply";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                String deviceSupName = resSet.getString("deviceSupName");
                if (deviceSupNameToCompare.equals(deviceSupName))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликатов поставщиков оборудования", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return true;
    }

    public static boolean isNewState(String stateToCompare) {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT state FROM device_state";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                String state = resSet.getString("state");
                if (stateToCompare.equals(state))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликатов состояния оборудования", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return true;
    }

    public static boolean isNewGroup(String groupToCompare) {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT groupName FROM device_group";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                String deviceGroup = resSet.getString("groupName");
                if (groupToCompare.equals(deviceGroup))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликатов группы устройства", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return true;
    }

    public static boolean isNewDeviceContract(int deviceSupId, String contractToCompare) {
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT contractNumber FROM device_contract WHERE deviceSupply_id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, deviceSupId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                String deviceContract = resSet.getString("contractNumber");
                if (contractToCompare.equals(deviceContract))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликатов группы устройства", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return true;
    }

    public static boolean isNewConnType(String connTypeToCompare) {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT connType FROM connection";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                String connType = resSet.getString("connType");
                if (connTypeToCompare.equals(connType))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликатов типа подключения", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return true;
    }

    public static boolean isNewCameraType(String cameraTypeToCompare) {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT cameraType FROM camera_type";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                String cameraType = resSet.getString("cameraType");
                if (cameraTypeToCompare.equals(cameraType))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликатов типа камеры", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return true;
    }

    public static boolean isNewAreaName(String areaNameToCompare) {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT areaName FROM area";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                String areaName = resSet.getString("areaName");
                if (areaNameToCompare.equals(areaName))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликата названия площадки", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return true;
    }

    public static boolean isNewCity(String cityToCompare) {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT cityName FROM city";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                String connType = resSet.getString("cityName");
                if (cityToCompare.equals(connType))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликатов названия города", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return true;
    }

    public static boolean isNewStreet(String streetNameToCompare, int cityId) {
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT streetName FROM street WHERE city_id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, cityId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                String streetName = resSet.getString("streetName");
                if (streetNameToCompare.equals(streetName))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликата улицы", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return true;
    }

    public static boolean isNewSubnet(String subnetToCompare) {
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT INET_NTOA(sub_network) as subnet FROM network";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                String subnet = resSet.getString("subnet");
                if (subnetToCompare.equals(subnet))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликата подсети", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return true;
    }

    public static boolean isNewCameraNum(String cameraNumToCompare) {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT cameraNum FROM device";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                String cameraNum = resSet.getString("cameraNum");
                if (cameraNumToCompare.equals(cameraNum))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликата номера камеры", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return true;
    }

    public static boolean isNewInventar(String inventarToCompare) {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT inventar FROM device";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                String inventar = resSet.getString("inventar");
                if (inventarToCompare.equals(inventar))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликата инвентраного номера устройства", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return true;
    }

    public static boolean isNewSerial(String serialToCompare) {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT serial FROM device";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                String serial = resSet.getString("serial");
                if (serialToCompare.equals(serial))
                    return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дубликата серийного номера устройства", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return true;
    }

    public static boolean isNewIp(String ipToCompare) {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT INET_NTOA(ipAddress) as ipAddress FROM device";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                String ipaddress = resSet.getString("ipAddress");
                if (ipToCompare.equals(ipaddress)) return false;
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске дублика та IP адреса", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
        return true;
    }
}