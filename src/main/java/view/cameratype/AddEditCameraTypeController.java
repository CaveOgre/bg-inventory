package view.cameratype;

import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.CameraType;
import view.AlertMessage;
import view.Check;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AddEditCameraTypeController {

    @FXML
    private Label cameraTypeLabel;

    @FXML
    private TextField cameraTypeField;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;

    private DbConnection dbConnection = new DbConnection();

    private static Stage stage;
    private static CameraType cameraType;
    private static boolean isUpdate;

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setCameraType(CameraType outCameraType) {
        cameraType = outCameraType;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    private String oldCameraType = "";

    @FXML
    private void initialize() {
        if (isUpdate)
            oldCameraType = cameraType.getCameraType();
        cameraTypeField.setText(oldCameraType);
    }

    @FXML
    private void okAction() {
        String cameraType = cameraTypeField.getText().trim();
        if (isFieldRight(cameraType)) {
            if (isUpdate) {
                editCameraType(cameraType);
                closeWindow();
            } else {
                addCameraType(cameraType);
                closeWindow();
            }
        } else cameraTypeField.setText(cameraType);
    }

    public void addCameraType(String cameraTypeToAdd) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO camera_type (cameraType) VALUES (?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, cameraTypeToAdd);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении типа камеры", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void editCameraType(String cameraTypeToEdit) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "UPDATE camera_type SET cameraType = ? WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, cameraTypeToEdit);
            preState.setInt(2, cameraType.getId());
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при редактировании типа камер", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private boolean isFieldRight(String cameraType) {
        if (!Check.isFieldNull(cameraType)) {
            if (!oldCameraType.equals(cameraType))
                if (!Check.isNewCameraType(cameraType)) {
                    AlertMessage.showSameName(cameraTypeLabel.getText());
                    return false;
                }
            return true;
        } else {
            AlertMessage.showEmptyFieldMessage(cameraTypeLabel.getText());
            return false;
        }
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}
