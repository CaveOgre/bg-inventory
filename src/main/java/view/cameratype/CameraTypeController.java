package view.cameratype;

import view.Check;
import mainpackage.Main;
import model.CameraType;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class CameraTypeController {

    @FXML
    private ListView<CameraType> cameraTypeView;

    @FXML
    private Button addBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button delBtn;

    @FXML
    private Button exitBtn;

    private DbConnection dbConnection = new DbConnection();

    private ObservableList<CameraType> cameraTypeList;

    private static Stage stage;

    public static void setStage(Stage outStage) {
        stage = outStage;
    }

    @FXML
    private void initialize() {
        cameraTypeList = FXCollections.observableArrayList();
        loadCameraType();

        cameraTypeView.setItems(cameraTypeList);
    }

    @FXML
    private void goToAddView() {
        int selectedIndex = cameraTypeView.getSelectionModel().getSelectedIndex();
        String title = "Добавление";

        Stage stage = createNewWindow(title);
        AddEditCameraTypeController.setStage(stage);

        assert stage != null;
        stage.showAndWait();

        initialize();
        cameraTypeView.getSelectionModel().select(selectedIndex);
    }

    @FXML
    private void goToEditView() {
        if (Check.isSelectedListView(cameraTypeView)) {
            int selectedIndex = cameraTypeView.getSelectionModel().getSelectedIndex();
            String title = "Редактирование";

            CameraType cameraType = cameraTypeView.getSelectionModel().getSelectedItem();
            AddEditCameraTypeController.setCameraType(cameraType);
            AddEditCameraTypeController.setIsUpdate(true);

            Stage stage = createNewWindow(title);
            AddEditCameraTypeController.setStage(stage);

            assert stage != null;
            stage.showAndWait();
            AddEditCameraTypeController.setIsUpdate(false);

            initialize();
            cameraTypeView.getSelectionModel().select(selectedIndex);
        } else AlertMessage.showNotSelectedElement();
    }

    @FXML
    private void deleteCameraType() {
        if (Check.isSelectedListView(cameraTypeView)) {
            Connection conn = null;
            PreparedStatement preState = null;
            String query = "DELETE FROM camera_type WHERE id = ?";
            int selectedIndex = cameraTypeView.getSelectionModel().getSelectedIndex();
            CameraType cameraType = cameraTypeView.getSelectionModel().getSelectedItem();
            updateDeviceCameraTypeId(cameraType.getId());
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setInt(1, cameraType.getId());
                preState.executeUpdate();

                initialize();
                cameraTypeView.getSelectionModel().select(Check.selectIndex(selectedIndex));
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при удалении типа камеры", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        } else AlertMessage.showNotSelectedElement();
    }

    private void updateDeviceCameraTypeId(int id) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "UPDATE device SET cameraType_id = 0 WHERE cameraType_id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, id);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при назначении cameraType_id = 0", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void loadCameraType() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM camera_type";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int cameraTypeId = resSet.getInt("id");
                if (cameraTypeId != 0)
                    cameraTypeList.add(new CameraType(cameraTypeId,
                            resSet.getString("cameraType")));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка во время загрузки списка типов камер", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private Stage createNewWindow(String title) {
        String view = "/AddEditCameraType.fxml";
        String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        AddEditCameraTypeController.setStage(stage);

        return stage;
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}
