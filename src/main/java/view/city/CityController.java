package view.city;

import view.Check;
import model.City;
import mainpackage.Main;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import view.Query;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class CityController {

    @FXML
    private ListView<City> cityListView;

    @FXML
    private Button addBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button delBtn;

    @FXML
    private Button exitBtn;

    private DbConnection dbConnection = new DbConnection();
    private ObservableList<City> cityList;

    private static Stage stage;

    public static void setStage(Stage outStage) {
        stage = outStage;
    }

    @FXML
    private void initialize() {
        cityList = FXCollections.observableArrayList();
        loadCityList();

        cityListView.setItems(cityList);
    }

    @FXML
    private void goToAddView() {
        int selectedIndex = cityListView.getSelectionModel().getSelectedIndex();
        String title = "Добавление";

        Stage stage = createNewWindow(title);
        AddEditCityController.setStage(stage);

        assert stage != null;
        stage.showAndWait();

        initialize();
        cityListView.getSelectionModel().select(selectedIndex);
    }

    @FXML
    private void goToEditView() {
        if (Check.isSelectedListView(cityListView)) {
            int selectedIndex = cityListView.getSelectionModel().getSelectedIndex();
            String title = "Редактирование";

            City city = cityListView.getSelectionModel().getSelectedItem();
            AddEditCityController.setCity(city);
            AddEditCityController.setIsUpdate(true);

            Stage stage = createNewWindow(title);
            AddEditCityController.setStage(stage);

            assert stage != null;
            stage.showAndWait();
            AddEditCityController.setIsUpdate(false);

            initialize();
            cityListView.getSelectionModel().select(selectedIndex);
        } else AlertMessage.showNotSelectedElement();
    }

    @FXML
    private void deleteCity() {
        if (Check.isSelectedListView(cityListView)) {
            Connection conn = null;
            PreparedStatement preState = null;
            String query = "DELETE FROM city WHERE id = ?";
            String areaUpdateQuery = "UPDATE area SET city_id = 0 WHERE city_id = ?";
            String streetUpdateQuery = "UPDATE street SET city_id = 0 WHERE city_id = ?";
            int selectedIndex = cityListView.getSelectionModel().getSelectedIndex();
            City city = cityListView.getSelectionModel().getSelectedItem();
            Query.updateDeleteItem(areaUpdateQuery, city.getId());
            Query.updateDeleteItem(streetUpdateQuery, city.getId());
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setInt(1, city.getId());
                preState.executeUpdate();
                initialize();
                cityListView.getSelectionModel().select(Check.selectIndex(selectedIndex));
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при удалении города", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        } else AlertMessage.showNotSelectedElement();
    }


    private void loadCityList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM city ORDER BY cityName";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int cityId = resSet.getInt("id");
                if (cityId != 0)
                    cityList.add(new City(cityId,
                            resSet.getString("cityName")));

            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка городов", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private Stage createNewWindow(String title) {
        String view = "/AddEditCity.fxml";
        String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        AddEditCityController.setStage(stage);

        return stage;
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }

}