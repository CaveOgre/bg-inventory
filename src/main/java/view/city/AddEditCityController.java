package view.city;

import model.City;
import view.Check;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class AddEditCityController {

    @FXML
    private Label cityLabel;

    @FXML
    private TextField cityField;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;

    private DbConnection dbConnection = new DbConnection();

    private static Stage stage;
    private static City city;
    private static boolean isUpdate;

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setCity(City outCity) {
        city = outCity;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    private String oldCity = "";

    @FXML
    private void initialize() {
        if (isUpdate)
            oldCity = city.getCityName();
        cityField.setText(oldCity);
    }

    @FXML
    private void okAction() {
        String cityName = cityField.getText().trim();
        if (isFieldRight(cityName)) {
            if (isUpdate)
                editCityName(cityName);
            else addCityName(cityName);
        } else cityField.setText(cityName);
    }

    private void addCityName(String cityNameToAdd) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO city (cityName) VALUES (?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, cityNameToAdd);
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении города", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void editCityName(String cityNameToEdit) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "UPDATE city SET cityName = ? WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, cityNameToEdit);
            preState.setInt(2, city.getId());
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при редактировании названия города", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private boolean isFieldRight(String cityName) {
        if (!Check.isFieldNull(cityName)) {
            if (!oldCity.equals(cityName))
                if (!Check.isNewCity(cityName)) {
                    AlertMessage.showSameName(cityLabel.getText());
                    return false;
                }
            return true;
        } else {
            AlertMessage.showEmptyFieldMessage(cityLabel.getText());
            return false;
        }
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}
