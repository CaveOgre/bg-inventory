package view.server;

import view.Check;
import model.Server;
import mainpackage.Main;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import view.Query;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class ServerController {

    @FXML
    private ListView<Server> serverListView;

    @FXML
    private Button addBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button delBtn;

    @FXML
    private Button exitBtn;

    private DbConnection dbConnection = new DbConnection();
    private ObservableList<Server> serverList;

    private static Stage stage;

    public static void setStage(Stage outStage) {
        stage = outStage;
    }

    @FXML
    private void initialize() {
        serverList = FXCollections.observableArrayList();
        loadServerList();

        serverListView.setItems(serverList);
        serverListView.getSelectionModel().selectFirst();
    }

    @FXML
    private void goToAddView() {
        int selectedIndex = serverListView.getSelectionModel().getSelectedIndex();
        String title = "Добавление";

        Stage stage = createNewWindow(title);
        AddEditServerController.setStage(stage);

        assert stage != null;
        stage.showAndWait();

        initialize();
        serverListView.getSelectionModel().select(selectedIndex);
    }

    @FXML
    private void goToEditView() {
        if (Check.isSelectedListView(serverListView)) {
            int selectedIndex = serverListView.getSelectionModel().getSelectedIndex();
            String title = "Редактирование";
            Server server = serverListView.getSelectionModel().getSelectedItem();
            AddEditServerController.setServer(server);
            AddEditServerController.setIsUpdate(true);

            Stage stage = createNewWindow(title);
            AddEditServerController.setStage(stage);

            assert stage != null;
            stage.showAndWait();
            AddEditServerController.setIsUpdate(false);

            initialize();
            serverListView.getSelectionModel().select(selectedIndex);
        } else AlertMessage.showNotSelectedElement();
    }

    private Stage createNewWindow(String title) {
        String view = "/AddEditServer.fxml";
        String errorText = "Ошибка при создани окна \"" + title + "\"";
        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, errorText);
        AddEditServerController.setStage(stage);

        return stage;
    }

    @FXML
    private void deleteServer() {
        if (Check.isSelectedListView(serverListView)) {
            Connection conn = null;
            PreparedStatement preState = null;
            String query = "DELETE FROM server WHERE id = ?";
            String updateQuery = "UPDATE device SET server_id = 0 WHERE server_id = ?";
            int selectedIndex = serverListView.getSelectionModel().getSelectedIndex();
            Server server = serverListView.getSelectionModel().getSelectedItem();
            Query.updateDeleteItem(updateQuery, server.getId());
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setInt(1, server.getId());
                preState.executeUpdate();
                initialize();
                serverListView.getSelectionModel().select(Check.selectIndex(selectedIndex));
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при удалении имени сервера", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        } else AlertMessage.showNotSelectedElement();
    }

    private void loadServerList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM server";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int serverId = resSet.getInt("id");
                if (serverId != 0)
                    serverList.add(new Server(serverId,
                            resSet.getString("serverName")));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка имен серверов", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }

}
