package view.server;

import view.Check;
import model.Server;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class AddEditServerController {

    @FXML
    private Label serverLabel;

    @FXML
    private TextField serverField;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;

    private DbConnection dbConnection = new DbConnection();

    private static Stage stage;
    private static Server server;
    private static boolean isUpdate;

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setServer(Server outServer) {
        server = outServer;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    private String oldServer = "";

    @FXML
    private void initialize() {
        if (isUpdate) {
            oldServer = server.getServerName();
            serverField.setText(oldServer);
        }
    }

    @FXML
    private void okAction() {
        String serverName = serverField.getText().trim();
        if (isFieldRight(serverName)) {
            if (isUpdate) {
                editServerName(serverName);
                closeWindow();
            } else {
                addServerName(serverName);
                closeWindow();
            }
        } else serverField.setText(serverName);
    }

    public void addServerName(String serverFieldText) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO server (serverName) VALUES (?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, serverFieldText.trim());
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении имени сервера", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void editServerName(String serverFieldText) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "UPDATE server SET serverName = ? WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, serverFieldText.trim());
            preState.setInt(2, server.getId());
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при редактировании имени сервера", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private boolean isFieldRight(String serverName) {
        if (!Check.isFieldNull(serverName)) {
            if (!oldServer.equals(serverName))
                if (!Check.isNewServerName(serverName)) {
                    AlertMessage.showSameName(serverLabel.getText());
                    return false;
                }
            return true;
        } else {
            AlertMessage.showEmptyFieldMessage(serverLabel.getText());
            return false;
        }
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }

}
