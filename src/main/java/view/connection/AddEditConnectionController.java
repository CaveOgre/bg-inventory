package view.connection;

import dbpackage.DbConnection;
import model.DeviceConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import view.AlertMessage;
import view.Check;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AddEditConnectionController {

    @FXML
    private Label connectionLabel;

    @FXML
    private TextField connectionField;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;

    private DbConnection dbConnection = new DbConnection();

    private static Stage stage;
    private static DeviceConnection deviceConnection;
    private static boolean isUpdate;

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setDeviceConnection(DeviceConnection outDeviceConnection) {
        deviceConnection = outDeviceConnection;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    private String oldConnection = "";

    @FXML
    private void initialize() {
        if (isUpdate)
            oldConnection = deviceConnection.getConnType();
        connectionField.setText(oldConnection);
    }

    @FXML
    private void okAction() {
        String connectionType = connectionField.getText().trim();
        if (isFieldRight(connectionType)) {
            if (isUpdate) {
                editConnection(connectionType);
                closeWindow();
            } else {
                addConnection(connectionType);
                closeWindow();
            }
        } else connectionField.setText(connectionType);
    }

    public void addConnection(String connectionType) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO connection (connType) VALUES (?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, connectionType);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении типа соединения", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void editConnection(String connectionType) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "UPDATE connection SET connType = ? WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, connectionType);
            preState.setInt(2, deviceConnection.getId());
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при редактировании типа соединения", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private boolean isFieldRight(String connectionType) {
        if (!Check.isFieldNull(connectionType)) {
            if (!oldConnection.equals(connectionType))
                if (!Check.isNewConnType(connectionType)) {
                    AlertMessage.showSameName(connectionLabel.getText());
                    return false;
                }
            return true;
        } else {
            AlertMessage.showEmptyFieldMessage(connectionLabel.getText());
            return false;
        }
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}
