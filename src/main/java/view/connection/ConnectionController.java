package view.connection;

import view.Check;
import mainpackage.Main;
import view.AlertMessage;
import model.DeviceConnection;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class ConnectionController {

    @FXML
    private ListView<DeviceConnection> connectionView;

    @FXML
    private Button addBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button delBtn;

    @FXML
    private Button exitBtn;

    private DbConnection dbConnection = new DbConnection();
    private ObservableList<DeviceConnection> deviceConnectionList;

    private static Stage stage;

    public static void setStage(Stage outStage) {
        stage = outStage;
    }

    @FXML
    private void initialize() {
        deviceConnectionList = FXCollections.observableArrayList();
        loadDeviceConnectionList();

        connectionView.setItems(deviceConnectionList);
    }

    @FXML
    private void goToAddView() {
        int selectedIndex = connectionView.getSelectionModel().getSelectedIndex();
        String title = "Добавление";

        Stage stage = createNewWindow(title);
        AddEditConnectionController.setStage(stage);

        assert stage != null;
        stage.showAndWait();

        initialize();
        connectionView.getSelectionModel().select(selectedIndex);
    }

    @FXML
    private void goToEditView() {
        if (Check.isSelectedListView(connectionView)) {
            int selectedIndex = connectionView.getSelectionModel().getSelectedIndex();
            String title = "Редактирование";

            DeviceConnection deviceConnection = connectionView.getSelectionModel().getSelectedItem();
            AddEditConnectionController.setDeviceConnection(deviceConnection);
            AddEditConnectionController.setIsUpdate(true);

            Stage stage = createNewWindow(title);
            AddEditConnectionController.setStage(stage);

            assert stage != null;
            stage.showAndWait();
            AddEditConnectionController.setIsUpdate(false);

            initialize();
            connectionView.getSelectionModel().select(selectedIndex);
        } else AlertMessage.showNotSelectedElement();
    }

    @FXML
    private void deleteDeviceConnection() {
        if (Check.isSelectedListView(connectionView)) {
            Connection conn = null;
            PreparedStatement preState = null;
            String query = "DELETE FROM connection WHERE id = ?";
            int selectedIndex = connectionView.getSelectionModel().getSelectedIndex();
            DeviceConnection deviceConnection = connectionView.getSelectionModel().getSelectedItem();
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setInt(1, deviceConnection.getId());
                preState.executeUpdate();
                initialize();
                connectionView.getSelectionModel().select(Check.selectIndex(selectedIndex));
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при удалении типа соединения", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        } else AlertMessage.showNotSelectedElement();
    }

    private void loadDeviceConnectionList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM connection";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int connTypeId = resSet.getInt("id");
                if (connTypeId != 0)
                    deviceConnectionList.add(new DeviceConnection(connTypeId,
                        resSet.getString("connType")));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка типа подключений", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private Stage createNewWindow(String title) {
        String view = "/AddEditConnection.fxml";
        String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        AddEditConnectionController.setStage(stage);

        return stage;
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }

}
