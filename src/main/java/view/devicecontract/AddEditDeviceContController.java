package view.devicecontract;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import model.DeviceContract;
import view.Check;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class AddEditDeviceContController {
    @FXML
    private Label deviceSupLable;

    @FXML
    private Label contractLabel;

    @FXML
    private Label garantLabel;

    @FXML
    private ComboBox<String> deviceSupBox;

    @FXML
    private TextField contractField;

    @FXML
    private TextField garantField;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;

    private DbConnection dbConnection = new DbConnection();
    private ObservableList<String> deviceSupList;

    private static Stage stage;
    private static DeviceContract deviceContract;
    private static boolean isUpdate;

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setDeviceContract(DeviceContract outDeviceContract) {
        deviceContract = outDeviceContract;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    private String oldDeviceContract = "";
    private int oldDeviceSupId = 0;

    @FXML
    private void initialize() {
        deviceSupList = FXCollections.observableArrayList();
        loadDeviceSupList();

        deviceSupBox.setItems(deviceSupList);

        if (isUpdate) {
            oldDeviceSupId = deviceContract.getDeviceSupplyId();
            String deviceSupName = findDeviceSupplyName(oldDeviceSupId);
            oldDeviceContract = deviceContract.getContractNumber();
            String garant = String.valueOf(deviceContract.getGarant());
            deviceSupBox.getEditor().setText(deviceSupName);
            contractField.setText(oldDeviceContract);
            garantField.setText(garant);
        }
    }

    @FXML
    private void okAction() {
        String deviceSupply = deviceSupBox.getEditor().getText().trim();
        String contrNumber = contractField.getText().trim();
        String garant = garantField.getText().trim();
        if (isFieldsRight(deviceSupply, contrNumber, garant)) {
            if (isUpdate) {
                editGroup(deviceSupply, contrNumber, garant);
                closeWindow();
            } else {
                addGroup(deviceSupply, contrNumber, garant);
                closeWindow();
            }
        } else {
            deviceSupBox.getEditor().setText(deviceSupply);
            contractField.setText(contrNumber);
            garantField.setText(garant);
        }
    }

    private void loadDeviceSupList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT deviceSupName FROM device_supply";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                deviceSupList.add(resSet.getString("deviceSupName"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка поставщиков оборудования", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private void editGroup(String deviceSupply, String contrNumber, String garant) {
        int deviceSuppId = findDeviceSupplyId(deviceSupply);
        byte garantToEdit = Byte.valueOf(garant);
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "UPDATE device_contract SET contractNumber = ?, deviceSupply_id = ?, garant = ? WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, contrNumber);
            preState.setInt(2, deviceSuppId);
            preState.setByte(3, garantToEdit);
            preState.setInt(4, deviceContract.getId());
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при редактировании типа оборудования", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    public void addGroup(String deviceSupply, String contrNumber, String garant) {
        int deviceSupId = findDeviceSupplyId(deviceSupply);
        byte garantToAdd = Byte.valueOf(garant);
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO device_contract (contractNumber, deviceSupply_id, garant) VALUES (?, ?, ?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, contrNumber);
            preState.setInt(2, deviceSupId);
            preState.setByte(3, garantToAdd);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении типа оборудования", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private boolean isFieldsRight(String deviceSupply, String contrNumber, String garant) {
        if (!Check.isFieldNull(deviceSupply)) {
            if (Check.isNewDeviceSupply(deviceSupply))
                createNewDeviceSup(deviceSupply);
            if (!Check.isFieldNull(contrNumber)) {
                int deviceSupId = findDeviceSupplyId(deviceSupply);
                if (oldDeviceSupId != deviceSupId || !oldDeviceContract.equals(contrNumber))
                    if (!Check.isNewDeviceContract(deviceSupId, contrNumber)) {
                        AlertMessage.showSameName(contractLabel.getText());
                        return false;
                    }
                if (!Check.isFieldNull(garant))
                    return true;
                else {
                    AlertMessage.showEmptyFieldMessage(garantLabel.getText());
                    return false;
                }
            } else {
                AlertMessage.showEmptyFieldMessage(contractField.getText());
                return false;
            }
        } else {
            AlertMessage.showEmptyFieldMessage(deviceSupLable.getText());
            return false;
        }
    }

    private String findDeviceSupplyName(int deviceSupplyid) {
        String deviceSupply = "";
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT deviceSupName FROM device_supply WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, deviceSupplyid);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                deviceSupply = resSet.getString("deviceSupName");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске имени поставщика оборудования по id", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return deviceSupply;
    }

    public int findDeviceSupplyId(String deviceSupName) {
        int deviceSupplyId = 0;
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT id FROM device_supply WHERE deviceSupName = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, deviceSupName);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                deviceSupplyId = resSet.getInt("id");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске имени поставщика оборудования по id", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return deviceSupplyId;
    }

    private void createNewDeviceSup(String newDeviceSup) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO device_supply (deviceSupName) VALUES (?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, newDeviceSup);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлени нового поставщика оборудования", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}