package view.devicecontract;

import view.Check;
import mainpackage.Main;
import view.AlertMessage;
import model.DeviceContract;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import view.Query;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class DeviceContractController {


    @FXML
    private ListView<DeviceContract> deviceContractView;

    @FXML
    private Button addBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button delBtn;

    @FXML
    private Button exitBtn;

    private DbConnection dbConnection = new DbConnection();
    private ObservableList<DeviceContract> deviceContractList;

    private static Stage stage;

    public static void setStage(Stage outStage) {
        stage = outStage;
    }

    @FXML
    private void initialize() {
        deviceContractList = FXCollections.observableArrayList();
        loadDeviceContractList();

        deviceContractView.setItems(deviceContractList);
    }

    @FXML
    private void goToAddView() {
        int selectedIndex = deviceContractView.getSelectionModel().getSelectedIndex();
        String title = "Добавление";

        Stage stage = createNewWindow(title);
        AddEditDeviceContController.setStage(stage);

        assert stage != null;
        stage.showAndWait();

        initialize();
        deviceContractView.getSelectionModel().select(selectedIndex);
    }

    @FXML
    private void goToEditView() {
        if (Check.isSelectedListView(deviceContractView)) {
            int selectedIndex = deviceContractView.getSelectionModel().getSelectedIndex();
            String title = "Редактирование";

            DeviceContract deviceContract = deviceContractView.getSelectionModel().getSelectedItem();
            AddEditDeviceContController.setDeviceContract(deviceContract);
            AddEditDeviceContController.setIsUpdate(true);

            Stage stage = createNewWindow(title);
            AddEditDeviceContController.setStage(stage);

            assert stage != null;
            stage.showAndWait();
            AddEditDeviceContController.setIsUpdate(false);

            initialize();
            deviceContractView.getSelectionModel().select(selectedIndex);
        } else AlertMessage.showNotSelectedElement();
    }

    @FXML
    private void deleteDeviceContract() {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "DELETE FROM device_contract WHERE id = ?";
        String updateQuery = "UPDATE device SET deviceContract_id = 0 WHERE deviceContract_id = ?";
        if (Check.isSelectedListView(deviceContractView)) {
            int selectedIndex = deviceContractView.getSelectionModel().getSelectedIndex();
            DeviceContract deviceContract = deviceContractView.getSelectionModel().getSelectedItem();
            Query.updateDeleteItem(updateQuery, deviceContract.getId());
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setInt(1, deviceContract.getId());
                preState.executeUpdate();
                initialize();
                deviceContractView.getSelectionModel().select(Check.selectIndex(selectedIndex));
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при удалении типа оборудования", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        } else AlertMessage.showNotSelectedElement();
    }

    private void loadDeviceContractList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM device_contract";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int deviceContractId = resSet.getInt("id");
                if (deviceContractId != 0)
                    deviceContractList.add(new DeviceContract(deviceContractId,
                        resSet.getString("contractNumber"), resSet.getInt("deviceSupply_id"),
                        resSet.getByte("garant")));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка типов устройств", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private Stage createNewWindow(String title) {
        String view = "/AddEditDeviceContract.fxml";
        String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        AddEditDeviceContController.setStage(stage);

        return stage;
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }

}