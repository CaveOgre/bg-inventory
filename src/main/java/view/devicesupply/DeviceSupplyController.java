package view.devicesupply;

import view.Check;
import mainpackage.Main;
import view.AlertMessage;
import model.DeviceSupply;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class DeviceSupplyController {

    @FXML
    private ListView<DeviceSupply> deviceSupplyView;

    @FXML
    private Button addBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button delBtn;

    @FXML
    private Button exitBtn;

    private DbConnection dbConnection = new DbConnection();
    private ObservableList<DeviceSupply> deviceSupplyList;

    private static Stage stage;

    public static void setStage(Stage outStage) {
        stage = outStage;
    }

    @FXML
    private void initialize() {
        deviceSupplyList = FXCollections.observableArrayList();
        loadDeviceSupplyList();

        deviceSupplyView.setItems(deviceSupplyList);
    }

    @FXML
    private void goToAddView() {
        int selectedIndex = deviceSupplyView.getSelectionModel().getSelectedIndex();
        String title = "Добавление";

        Stage stage = createNewWindow(title);
        AddEditDeviceSupplyController.setStage(stage);

        assert stage != null;
        stage.showAndWait();

        initialize();
        deviceSupplyView.getSelectionModel().select(selectedIndex);
    }

    @FXML
    private void goToEditView() {
        if (Check.isSelectedListView(deviceSupplyView)) {
            int selectedIndex = deviceSupplyView.getSelectionModel().getSelectedIndex();
            String title = "Редактирование";

            DeviceSupply deviceSupply = deviceSupplyView.getSelectionModel().getSelectedItem();
            AddEditDeviceSupplyController.setDeviceSupply(deviceSupply);
            AddEditDeviceSupplyController.setIsUpdate(true);

            Stage stage = createNewWindow(title);
            AddEditDeviceSupplyController.setStage(stage);

            assert stage != null;
            stage.showAndWait();
            AddEditDeviceSupplyController.setIsUpdate(false);

            initialize();
            deviceSupplyView.getSelectionModel().select(selectedIndex);
        } else AlertMessage.showNotSelectedElement();
    }

    @FXML
    private void deleteDeviceSupply() {
        if (Check.isSelectedListView(deviceSupplyView)) {
            Connection conn = null;
            PreparedStatement preState = null;
            String query = "DELETE FROM device_supply WHERE id = ?";
            int selectedIndex = deviceSupplyView.getSelectionModel().getSelectedIndex();
            DeviceSupply deviceSupply = deviceSupplyView.getSelectionModel().getSelectedItem();
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setInt(1, deviceSupply.getId());
                preState.executeUpdate();
                initialize();
                deviceSupplyView.getSelectionModel().select(Check.selectIndex(selectedIndex));
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при удалении поставщика устройтва", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        } else AlertMessage.showNotSelectedElement();
    }

    private void loadDeviceSupplyList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM device_supply";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int deviceSupplyId = resSet.getInt("id");
                if (deviceSupplyId != 0)
                    deviceSupplyList.add(new DeviceSupply(deviceSupplyId,
                        resSet.getString("deviceSupName")));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка поставщиков устройтв", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private Stage createNewWindow(String title) {
        String view = "/AddEditDeviceSupply.fxml";
        String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        AddEditDeviceSupplyController.setStage(stage);

        return stage;
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }

}
