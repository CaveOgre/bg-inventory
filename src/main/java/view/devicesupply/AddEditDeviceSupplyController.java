package view.devicesupply;

import dbpackage.DbConnection;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.DeviceSupply;
import view.AlertMessage;
import view.Check;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AddEditDeviceSupplyController {

    @FXML
    private Label deviceSupplyLabel;

    @FXML
    private TextField deviceSupplyField;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;

    private DbConnection dbConnection = new DbConnection();

    private static Stage stage;
    private static DeviceSupply deviceSupply;
    private static boolean isUpdate;

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setDeviceSupply(DeviceSupply outDeviceSupply) {
        deviceSupply = outDeviceSupply;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    private String oldDeviceSup = "";

    @FXML
    private void initialize() {
        if (isUpdate)
            oldDeviceSup = deviceSupply.getVendorName();
        deviceSupplyField.setText(oldDeviceSup);
    }

    @FXML
    private void okAction() {
        String deviceSupplyName = deviceSupplyField.getText().trim();
        if (isFieldRight(deviceSupplyName)) {
            if (isUpdate) {
                editDeviceSupply(deviceSupplyName);
                closeWindow();
            } else {
                addDeviceSupply(deviceSupplyName);
                closeWindow();
            }
        } else deviceSupplyField.setText(deviceSupplyName);
    }

    public void addDeviceSupply(String deviceSupplyName) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO device_supply (deviceSupName) VALUES (?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, deviceSupplyName);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении поставщика устройства", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void editDeviceSupply(String deviceSupplyName) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "UPDATE device_supply SET deviceSupName = ? WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, deviceSupplyName);
            preState.setInt(2, deviceSupply.getId());
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при редактировании поставщика устройства", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private boolean isFieldRight(String deviceSupplyName) {
        if (!Check.isFieldNull(deviceSupplyName)) {
            if (!oldDeviceSup.equals(deviceSupplyName))
                if (!Check.isNewDeviceSupply(deviceSupplyName)) {
                    AlertMessage.showSameName(deviceSupplyLabel.getText());
                    return false;
                }
            return true;
        } else {
            AlertMessage.showEmptyFieldMessage(deviceSupplyLabel.getText());
            return false;
        }
    }


    @FXML
    private void closeWindow() {
        stage.close();
    }
}
