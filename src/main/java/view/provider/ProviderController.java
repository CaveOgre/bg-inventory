package view.provider;

import view.Check;
import model.Provider;
import mainpackage.Main;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import view.Query;
import view.vlancontract.AddEditVlanContractController;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class ProviderController {

    @FXML
    private ListView<Provider> providerView;

    @FXML
    private Button addBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button delBtn;

    @FXML
    private Button exitBtn;

    private DbConnection dbConnection = new DbConnection();
    private ObservableList<Provider> providerList;

    private static Stage stage;

    public static void setStage(Stage outStage) {
        stage = outStage;
    }

    @FXML
    private void initialize() {
        providerList = FXCollections.observableArrayList();
        loadProviderList();

        providerView.setItems(providerList);
        providerView.getSelectionModel().selectFirst();
    }

    @FXML
    private void goToAddView() {
        int selectedIndex = providerView.getSelectionModel().getSelectedIndex();
        String title = "Добавление";

        Stage stage = createNewWindow(title);
        AddEditProviderController.setStage(stage);

        assert stage != null;
        stage.showAndWait();

        initialize();
        providerView.getSelectionModel().select(selectedIndex);
    }

    @FXML
    private void goToEditView() {
        if (Check.isSelectedListView(providerView)) {
            int selectedIndex = providerView.getSelectionModel().getSelectedIndex();
            String title = "Редактирование";
            Provider provider = providerView.getSelectionModel().getSelectedItem();
            AddEditProviderController.setProvider(provider);
            AddEditProviderController.setIsUpdate(true);

            Stage stage = createNewWindow(title);
            AddEditProviderController.setStage(stage);

            assert stage != null;
            stage.showAndWait();
            AddEditProviderController.setIsUpdate(false);

            initialize();
            providerView.getSelectionModel().select(selectedIndex);
        } else AlertMessage.showNotSelectedElement();
    }

    private Stage createNewWindow(String title) {
        String view = "/AddEditProvider.fxml";
        String errorText = "Ошибка при создани окна \"" + title + "\"";
        URL toView = AddEditVlanContractController.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, errorText);
        AddEditProviderController.setStage(stage);

        return stage;
    }

    @FXML
    private void deleteProvider() {
        if (Check.isSelectedListView(providerView)) {
            Connection conn = null;
            PreparedStatement preState = null;
            String query = "DELETE FROM provider WHERE id = ?";
            String updateQuery = "UPDATE vlan_contract SET provider_id = 0 WHERE provider_id = ?";
            int selectedIndex = providerView.getSelectionModel().getSelectedIndex();
            Provider provider = providerView.getSelectionModel().getSelectedItem();
            Query.updateDeleteItem(updateQuery, provider.getId());
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setInt(1, provider.getId());
                preState.executeUpdate();
                initialize();
                providerView.getSelectionModel().select(Check.selectIndex(selectedIndex));
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при удалении провайдера", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        } else AlertMessage.showNotSelectedElement();
    }

    private void loadProviderList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM provider";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int providerId = resSet.getInt("id");
                if (providerId != 0) {
                    providerList.add(new Provider(providerId,
                            resSet.getString("providerName")));
                }
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка провайдера", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }

}
