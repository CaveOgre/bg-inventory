package view.provider;

import view.Check;
import model.Provider;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class AddEditProviderController {

    @FXML
    private Label providerLabel;

    @FXML
    private TextField providerField;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;

    private DbConnection dbConnection = new DbConnection();

    private static Stage stage;
    private static Provider provider;
    private static boolean isUpdate;

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setProvider(Provider outProvider) {
        provider = outProvider;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    private String oldProvider = "";

    @FXML
    private void initialize() {
        if (isUpdate) {
            oldProvider = provider.getProviderName();
            providerField.setText(oldProvider);
        }
    }

    @FXML
    private void okAction() {
        String providerFieldText = providerField.getText().trim();
        if (isFieldRight(providerFieldText)) {
            if (isUpdate) {
                editProvider(providerFieldText);
            } else addProvider(providerFieldText);
        } else providerField.setText(providerFieldText);
    }

    private void editProvider(String providerFieldText) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "UPDATE provider SET providerName = ? WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, providerFieldText.trim());
            preState.setInt(2, provider.getId());
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при редактировании имени провайдера", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void addProvider(String providerFieldText) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO provider (providerName) VALUES (?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, providerFieldText.trim());
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении имени провайдера", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private boolean isFieldRight(String providerName) {
        if (!Check.isFieldNull(providerName)) {
            if (!oldProvider.equals(providerName))
                if (!Check.isNewProvider(providerName)) {
                    AlertMessage.showSameName(providerLabel.getText());
                    return false;
                }
            return true;
        } else {
            AlertMessage.showEmptyFieldMessage(providerLabel.getText());
            return false;
        }
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}
