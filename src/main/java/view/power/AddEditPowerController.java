package view.power;

import view.Check;
import model.Power;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class AddEditPowerController {

    @FXML
    private Label powerLabel;

    @FXML
    private TextField powerField;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;

    private DbConnection dbConnection = new DbConnection();

    private static Stage stage;
    private static Power devicePower;
    private static boolean isUpdate;

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setDevicePower(Power outDevicePower) {
        devicePower = outDevicePower;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    private String oldPower = "";

    @FXML
    private void initialize() {
        if (isUpdate)
            oldPower = devicePower.getPowerName();
        powerField.setText(oldPower);
    }

    @FXML
    private void okAction() {
        String powerName = powerField.getText().trim();
        if (isFieldRight(powerName)) {
            if (isUpdate) {
                editPower(powerName);
                closeWindow();
            }
            else {
                addPower(powerName);
                closeWindow();
            }
        } else powerField.setText(powerName);
    }

    private void editPower(String powerName) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "UPDATE power SET powerName = ? WHERE id = ?";
        if (!Check.isFieldNull(powerName)) {
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setString(1, powerName);
                preState.setInt(2, devicePower.getId());
                preState.executeUpdate();
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при редактировании типа питания оборудования", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        } else AlertMessage.showEmptyFieldMessage(powerLabel.getText());
    }

    public void addPower(String powerName) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO power (powerName) VALUES (?)";
        if (!Check.isFieldNull(powerName)) {
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setString(1, powerName);
                preState.executeUpdate();
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при добавлении типа питания оборудования", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        } else AlertMessage.showEmptyFieldMessage(powerLabel.getText());
    }

    private boolean isFieldRight(String powerName) {
        if (!Check.isFieldNull(powerName)) {
            if (!oldPower.equals(powerName))
                if (!Check.isNewPowerName(powerName)) {
                    AlertMessage.showSameName(powerLabel.getText());
                    return false;
                }
            return true;
        } else {
            AlertMessage.showEmptyFieldMessage(powerLabel.getText());
            return false;
        }
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}
