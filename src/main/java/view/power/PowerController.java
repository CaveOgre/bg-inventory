package view.power;

import view.Check;
import model.Power;
import mainpackage.Main;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import view.Query;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class PowerController {

    @FXML
    private ListView<Power> devicePowerView;

    @FXML
    private Button addBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button delBtn;

    @FXML
    private Button exitBtn;

    private DbConnection dbConnection = new DbConnection();
    private ObservableList<Power> devicePowerList;

    private static Stage stage;

    public static void setStage(Stage outStage) {
        stage = outStage;
    }

    @FXML
    private void initialize() {
        devicePowerList = FXCollections.observableArrayList();
        loadDevicePowerList();

        devicePowerView.setItems(devicePowerList);
        devicePowerView.getSelectionModel().selectFirst();
    }

    @FXML
    private void goToAddView() {
        int selectedIndex = devicePowerView.getSelectionModel().getSelectedIndex();
        String title = "Добавление";

        Stage stage = createNewWindow(title);
        AddEditPowerController.setStage(stage);

        assert stage != null;
        stage.showAndWait();

        initialize();
        devicePowerView.getSelectionModel().select(selectedIndex);
    }

    @FXML
    private void goToEditView() {
        if (Check.isSelectedListView(devicePowerView)) {
            int selectedIndex = devicePowerView.getSelectionModel().getSelectedIndex();
            String title = "Редактировать";

            Power devicePower = devicePowerView.getSelectionModel().getSelectedItem();
            AddEditPowerController.setDevicePower(devicePower);
            AddEditPowerController.setIsUpdate(true);

            Stage stage = createNewWindow(title);
            AddEditPowerController.setStage(stage);

            assert stage != null;
            stage.showAndWait();
            AddEditPowerController.setIsUpdate(false);

            initialize();
            devicePowerView.getSelectionModel().select(selectedIndex);
        } else AlertMessage.showNotSelectedElement();
    }

    @FXML
    private void deletePower() {
        if (Check.isSelectedListView(devicePowerView)) {
            int selectedIndex = devicePowerView.getSelectionModel().getSelectedIndex();
            Power devicePower = devicePowerView.getSelectionModel().getSelectedItem();
            Connection conn = null;
            PreparedStatement preState = null;
            String query = "DELETE FROM power WHERE id = ?";
            String updateQuery = "UPDATE device SET power_id = 0 WHERE power_id = ?";
            Query.updateDeleteItem(updateQuery, devicePower.getId());
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setInt(1, devicePower.getId());
                preState.executeUpdate();
                initialize();
                devicePowerView.getSelectionModel().select(Check.selectIndex(selectedIndex));
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при удалении типа питания устройства", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        } else AlertMessage.showNotSelectedElement();
    }

    private void loadDevicePowerList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM power";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int powerId = resSet.getInt("id");
                if (powerId != 0)
                devicePowerList.add(new Power(powerId,
                        resSet.getString("powerName")));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка типа питания устройства", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private Stage createNewWindow(String title) {
        String view = "/AddEditPower.fxml";
        String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        AddEditPowerController.setStage(stage);

        return stage;
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }

}
