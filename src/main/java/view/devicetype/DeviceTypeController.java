package view.devicetype;

import javafx.scene.control.ListView;
import view.Check;
import model.DeviceType;
import mainpackage.Main;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import view.Query;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class DeviceTypeController {

    @FXML
    private ListView<DeviceType> deviceTypeView;

    @FXML
    private Button addBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button delBtn;

    @FXML
    private Button exitBtn;

    private DbConnection dbConnection = new DbConnection();
    private ObservableList<DeviceType> deviceTypeList;

    private static Stage stage;

    public static void setStage(Stage outStage) {
        stage = outStage;
    }

    @FXML
    private void initialize() {
        deviceTypeList = FXCollections.observableArrayList();
        loadDeviceTypeList();

        deviceTypeView.setItems(deviceTypeList);
        deviceTypeView.getSelectionModel().selectFirst();
    }

    @FXML
    private void goToAddView() {
        int selectedIndex = deviceTypeView.getSelectionModel().getSelectedIndex();
        String title = "Добавление";

        Stage stage = createNewWindow(title);
        AddEditDeviceTypeController.setStage(stage);

        assert stage != null;
        stage.showAndWait();

        initialize();
        deviceTypeView.getSelectionModel().select(selectedIndex);
    }

    @FXML
    private void goToEditView() {
        if (Check.isSelectedListView(deviceTypeView)) {
            int selectedIndex = deviceTypeView.getSelectionModel().getSelectedIndex();
            String title = "Редактирование";

            DeviceType deviceState = deviceTypeView.getSelectionModel().getSelectedItem();
            AddEditDeviceTypeController.setDeviceType(deviceState);
            AddEditDeviceTypeController.setIsUpdate(true);

            Stage stage = createNewWindow(title);
            AddEditDeviceTypeController.setStage(stage);

            assert stage != null;
            stage.showAndWait();
            AddEditDeviceTypeController.setIsUpdate(false);

            initialize();
            deviceTypeView.getSelectionModel().select(selectedIndex);
        } else AlertMessage.showNotSelectedElement();
    }

    @FXML
    private void deleteType() {
        if (Check.isSelectedListView(deviceTypeView)) {
            Connection conn = null;
            PreparedStatement preState = null;
            String query = "DELETE FROM device_type WHERE id = ?";
            String updateQuery = "UPDATE device SET deviceType_id = 0 WHERE deviceType_id = ?";
            int selectedIndex = deviceTypeView.getSelectionModel().getSelectedIndex();
            DeviceType deviceType = deviceTypeView.getSelectionModel().getSelectedItem();
            Query.updateDeleteItem(updateQuery, deviceType.getId());
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setInt(1, deviceType.getId());
                preState.executeUpdate();
                initialize();
                deviceTypeView.getSelectionModel().select(Check.selectIndex(selectedIndex));
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при удалении типа устройства", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        } else AlertMessage.showNotSelectedElement();
    }

    private void loadDeviceTypeList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM device_type";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int id = resSet.getInt("id");
                if (id != 0) {
                    deviceTypeList.add(new DeviceType(resSet.getInt("id"),
                            resSet.getString("deviceType")));
                }
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка типа устройства", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private Stage createNewWindow(String title) {
        String view = "/AddEditDeviceType.fxml";
        String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        AddEditDeviceTypeController.setStage(stage);

        return stage;
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }

}
