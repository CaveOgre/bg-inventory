package view.devicetype;

import view.Check;
import model.DeviceType;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class AddEditDeviceTypeController {

    @FXML
    private Label typeLabel;

    @FXML
    private TextField typeField;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;

    private DbConnection dbConnection = new DbConnection();

    private static Stage stage;
    private static boolean isUpdate;
    private static DeviceType deviceType;

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    static void setDeviceType(DeviceType outDeviceType) {
        deviceType = outDeviceType;
    }

    private String oldDeviceType = "";

    @FXML
    private void initialize() {
        if (isUpdate)
            oldDeviceType = deviceType.getDeviceType();
        typeField.setText(oldDeviceType);
    }

    @FXML
    private void okAction() {
        String typeTitle = typeField.getText().trim();
        if (isFieldRight(typeTitle)) {
            if (isUpdate) {
                editDeviceType(typeTitle);
                closeWindow();
            }
            else {
                addDeviceType(typeTitle);
                closeWindow();
            }
        } else typeField.setText(typeTitle);
    }

    public void addDeviceType(String typeTitle) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO device_type (deviceType) VALUES (?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, typeTitle);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении типа оборудования", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void editDeviceType(String typeTitle) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "UPDATE device_type SET deviceType = ? WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, typeTitle);
            preState.setInt(2, deviceType.getId());
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при редактировании типа оборудования", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private boolean isFieldRight(String typeTitle) {
        if (!Check.isFieldNull(typeTitle)) {
            if (!oldDeviceType.equals(typeTitle))
                if (!Check.isNewDeviceType(typeTitle)) {
                    AlertMessage.showSameName(typeLabel.getText());
                    return false;
                }
            return true;
        } else {
            AlertMessage.showEmptyFieldMessage(typeLabel.getText());
            return false;
        }
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}
