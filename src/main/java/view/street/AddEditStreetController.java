package view.street;

import view.Check;
import model.City;
import model.Street;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class AddEditStreetController {

    @FXML
    private Label streetLabel;

    @FXML
    private ComboBox<City> cityBox;

    @FXML
    private TextField streetField;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;

    private DbConnection dbConnection = new DbConnection();

    private static Stage stage;
    private static Street street;
    private static boolean isUpdate;

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setStreet(Street outStreet) {
        street = outStreet;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    private static final String FIELDNAME = "улица";
    private ObservableList<City> cityList;
    private String oldStreet = "";
    private int oldCityId = 0;

    @FXML
    private void initialize() {
        cityList = FXCollections.observableArrayList();
        loadCityList();
        cityBox.setItems(cityList);
        cityBox.getSelectionModel().selectFirst();

        if (isUpdate) {
            oldStreet = street.getStreetName();
            oldCityId = street.getCityId();
            City cityToSelect = getCityObj(oldCityId);
            cityBox.getSelectionModel().select(cityToSelect);
            streetField.setText(oldStreet);
        }
    }

    private void loadCityList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM city";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int cityId = resSet.getInt("id");
                if (cityId != 0)
                    cityList.add(new City(cityId, resSet.getString("cityName")));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка городов", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private City getCityObj(int cityId) {
        City city = null;
        Connection conn = null;
        PreparedStatement preSet = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM city WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preSet = conn.prepareStatement(query);
            preSet.setInt(1, cityId);
            resSet = preSet.executeQuery();
            while (resSet.next()){
                city = new City(resSet.getInt("id"), resSet.getString("cityName"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке объекта город", e);
        } finally {
            dbConnection.closeConnection(conn, preSet, resSet);
        }
        return city;
    }

    @FXML
    private void okAction() {
        String street = streetField.getText().trim();
        City city = cityBox.getSelectionModel().getSelectedItem();
        int cityId = city.getId();
        if (isFieldsRight(street, cityId)) {
            if (isUpdate) {
                editStreet(street, cityId);
            } else addStreet(street, cityId);
        } else {
            streetField.setText(street);
        }
    }

    private void addStreet(String streetName, int cityId) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO street (city_id, streetName) VALUES (?,?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, cityId);
            preState.setString(2, streetName);
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении улицы", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void editStreet(String streetName, int cityId) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "UPDATE street SET city_id = ?, streetName = ? WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, cityId);
            preState.setString(2, streetName);
            preState.setInt(3, street.getId());
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при редактировании улицы", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private boolean isFieldsRight(String streetName, int cityId) {
        if (!Check.isFieldNull(streetName)) {
            return isNewData(streetName, cityId);
        } else {
            AlertMessage.showEmptyFieldMessage(FIELDNAME);
            return false;
        }
    }

    private boolean isNewData(String streetName, int cityId) {
        if (!oldStreet.equals(streetName) || oldCityId != cityId) {
            if (!Check.isNewStreet(streetName, cityId)) {
                AlertMessage.showSameName(FIELDNAME);
                return false;
            }
        }
        return true;
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }

}
