package view.street;

import model.Street;
import view.Check;
import mainpackage.Main;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import view.Query;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class StreetController {
    @FXML
    private ListView<Street> streetListView;

    @FXML
    private Button addBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button delBtn;

    @FXML
    private Button exitBtn;

    private DbConnection dbConnection = new DbConnection();
    private ObservableList<Street> streetList;

    private static Stage stage;

    public static void setStage(Stage outStage) {
        stage = outStage;
    }

    @FXML
    private void initialize() {
        streetList = FXCollections.observableArrayList();
        loadModelList();

        streetListView.setItems(streetList);
    }

    @FXML
    private void goToAddView() {
        int selectedIndex = streetListView.getSelectionModel().getSelectedIndex();
        String title = "Добавление";

        Stage stage = createNewWindow(title);
        AddEditStreetController.setStage(stage);

        assert stage != null;
        stage.showAndWait();

        initialize();
        streetListView.getSelectionModel().select(selectedIndex);
    }

    @FXML
    private void goToEditView() {
        if (Check.isSelectedListView(streetListView)) {
            int selectedIndex = streetListView.getSelectionModel().getSelectedIndex();
            String title = "Редактировать";

            Street street = streetListView.getSelectionModel().getSelectedItem();
            AddEditStreetController.setStreet(street);
            AddEditStreetController.setIsUpdate(true);

            Stage stage = createNewWindow(title);
            AddEditStreetController.setStage(stage);

            assert stage != null;
            stage.showAndWait();
            AddEditStreetController.setIsUpdate(false);

            initialize();
            streetListView.getSelectionModel().select(selectedIndex);
        } else AlertMessage.showNotSelectedElement();
    }

    @FXML
    private void deleteStreet() {
        if (Check.isSelectedListView(streetListView)) {
            int selectedIndex = streetListView.getSelectionModel().getSelectedIndex();
            Street street = streetListView.getSelectionModel().getSelectedItem();
            Connection conn = null;
            PreparedStatement preState = null;
            String query = "DELETE FROM street WHERE id = ?";
            String updateQuery1 = "UPDATE area SET fStreet_id = 0 WHERE fStreet_id = ?";
            String updateQuery2 = "UPDATE area SET sStreet_id = 0 WHERE sStreet_id = ?";
            Query.updateDeleteItem(updateQuery1, street.getId());
            Query.updateDeleteItem(updateQuery2, street.getId());
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setInt(1, street.getId());
                preState.executeUpdate();
                initialize();
                streetListView.getSelectionModel().select(Check.selectIndex(selectedIndex));
            } catch (SQLException e) {
                dbConnection.setLog("Ошибка при удалении улицы", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        } else AlertMessage.showNotSelectedElement();
    }

    private void loadModelList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM street";

        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int streetId = resSet.getInt("id");
                if (streetId != 0)
                    streetList.add(new Street(streetId, resSet.getInt("city_id"),
                            resSet.getString("streetName")));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка улиц", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    private Stage createNewWindow(String title) {
        String view = "/AddEditStreet.fxml";
        String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        AddEditStreetController.setStage(stage);

        return stage;
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }

}