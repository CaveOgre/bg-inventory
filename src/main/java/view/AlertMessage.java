package view;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;

import java.util.Optional;

public class AlertMessage {

    //Messages
    public static void showSameName(String value){
        String text = " Такой " + value + " уже есть. Введите новое значение!";
        showErrorMessage(text);
    }

    public static void showEmptyFieldMessage(String labelText){
        String text = " Поле \"" + labelText + "\" пустое.";
        showInfoMessage(text);
    }

    public static void showNotSelectedElement(){
        String text = "\nНе выбрано ни одного элемента.";
        showWarningMessage(text);
    }

    public static void showNotDigitMessage(String labelText){
        String text = " В поле \""+ labelText + "\" неверный формат.\n Введите число.";
        showErrorMessage(text);
    }

    public static void showNotValidRange(String label, String fromValue, String toValue){
        String text = " Невено указан " + label + "!\n Введите значение от " + fromValue + " до " + toValue + ".";
        showWarningMessage(text);
    }

    public static boolean isOkEmtyField(String labelText){
        String text = " Поле " + labelText + " пустое.\n Вы уверены, что хотите оставить его пустым?";
        return isOKMessage(text);
    }

    public static boolean isOkNewValue(String labelText){
        String text = " Поле " + labelText + " содержит новое значение.\n Вы хотите добавить?";
        return isOKMessage(text);
    }

    public static void showStrValueTooBig(String labelText){

        String text = " Значение в строке " + labelText + " слишком длинное!";
        showErrorMessage(text);
    }

    public static String showPhoneDialog(){
        String text = "Введите номер телефона поставщика электричества: ";
        String phone = "";
        while (phone.equals("")){
            phone = showMessageWithField(text);
            if (phone == null)
                break;
            phone = phone.trim();
            if (phone.equals(""))
                showEmptyFieldMessage("номер телефона");
        }
        return phone;
    }

    public static void showSubnetWrongFormat(String labelText) {
        String text = "Неверный формат IP адреса в поле " + labelText;
        showWarningMessage(text);
    }

    public static void showAlreadyInUse(String labelText) {
        String text = "Такое значени " + labelText + " уже используется";
        showErrorMessage(text);
    }


    //Windows body
    private static void showErrorMessage(String text){
        Alert alert = new Alert(Alert.AlertType.ERROR);

        alert.setTitle("Ошибка");
        alert.setHeaderText(null);
        alert.setContentText(text);

        alert.showAndWait();
    }

    private static void showWarningMessage(String text){
        Alert alert = new Alert(Alert.AlertType.WARNING);

        alert.setTitle("Предупреждение");
        alert.setHeaderText(null);
        alert.setContentText(text);

        alert.showAndWait();
    }

    private static void showInfoMessage(String text){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);

        alert.setTitle("Информация");
        alert.setHeaderText(null);
        alert.setContentText(text);

        alert.showAndWait();
    }

    private static boolean isOKMessage(String text){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);

        alert.setTitle("Информация");
        alert.setHeaderText(null);
        alert.setContentText(text);

        Optional<ButtonType> result = alert.showAndWait();
        return result.orElse(null) == ButtonType.OK;
    }

    private static String showMessageWithField(String text){
        TextInputDialog dialog = new TextInputDialog();

        dialog.setTitle("Информация");
        dialog.setHeaderText(null);
        dialog.setContentText(text);

        Optional<String> result = dialog.showAndWait();

        return result.orElse(null);
    }
}
