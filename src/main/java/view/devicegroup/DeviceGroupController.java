package view.devicegroup;

import view.Check;
import mainpackage.Main;
import model.DeviceGroup;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import view.Query;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class DeviceGroupController {

    @FXML
    private ListView<DeviceGroup> groupListView;

    @FXML
    private Button addBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button delBtn;

    @FXML
    private Button exitBtn;

    private DbConnection dbConnection = new DbConnection();
    private ObservableList<DeviceGroup> groupDeviceList;

    private static Stage stage;

    public static void setStage(Stage outStage){
        stage = outStage;
    }

    @FXML
    private void initialize(){
        groupDeviceList = FXCollections.observableArrayList();
        loadDeviceGroupList();

        groupListView.setItems(groupDeviceList);
    }

    private void loadDeviceGroupList(){
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM device_group";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()){
                int deviceGroupId = resSet.getInt("id");
                if ( deviceGroupId != 0)
                    groupDeviceList.add(new DeviceGroup(deviceGroupId,
                            resSet.getString("groupName")));
            }
        } catch (SQLException e){
            dbConnection.setLog("Ошибка при загрузке списка типов устройств", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    @FXML
    private void goToAddView(){
        int selectedIndex = groupListView.getSelectionModel().getSelectedIndex();
        String title = "Добавление";

        Stage stage = createNewWindow(title);
        AddEditDeviceGroupController.setStage(stage);

        assert stage != null;
        stage.showAndWait();

        initialize();
        groupListView.getSelectionModel().select(selectedIndex);
    }

    @FXML
    private void goToEditView(){
        if (Check.isSelectedListView(groupListView)) {
            int selectedIndex = groupListView.getSelectionModel().getSelectedIndex();
            String title = "Редактирование";

            DeviceGroup deviceGroup = groupListView.getSelectionModel().getSelectedItem();
            AddEditDeviceGroupController.setDeviceGroup(deviceGroup);
            AddEditDeviceGroupController.setIsUpdate(true);

            Stage stage = createNewWindow(title);
            AddEditDeviceGroupController.setStage(stage);

            assert stage != null;
            stage.showAndWait();
            AddEditDeviceGroupController.setIsUpdate(false);

            initialize();
            groupListView.getSelectionModel().select(selectedIndex);
        } else AlertMessage.showNotSelectedElement();
    }

    private Stage createNewWindow(String title) {
        String view = "/AddEditDeviceGroup.fxml";
        String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        AddEditDeviceGroupController.setStage(stage);

        return stage;
    }

    @FXML
    private void deleteDeviceGroup(){
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "DELETE FROM device_group WHERE id = ?";
        String updateQuery = "UPDATE device SET groupDevice_id = 0 WHERE groupDevice_id = ?";
        if (Check.isSelectedListView(groupListView)) {
            int selectedIndex = groupListView.getSelectionModel().getSelectedIndex();
            DeviceGroup deviceGroup = groupListView.getSelectionModel().getSelectedItem();
            Query.updateDeleteItem(updateQuery, deviceGroup.getId());
            try {
                conn = dbConnection.getDbConnection();
                preState = conn.prepareStatement(query);
                preState.setInt(1, deviceGroup.getId());
                preState.executeUpdate();
                initialize();
                groupListView.getSelectionModel().select(Check.selectIndex(selectedIndex));
            } catch (SQLException e){
                dbConnection.setLog("Ошибка при удалении типа оборудования", e);
            } finally {
                dbConnection.closeConnection(conn, preState);
            }
        }else AlertMessage.showNotSelectedElement();
    }

    @FXML
    private void closeWindow(){
        stage.close();
    }

}
