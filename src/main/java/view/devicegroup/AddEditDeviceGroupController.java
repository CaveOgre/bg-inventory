package view.devicegroup;

import dbpackage.DbConnection;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.DeviceGroup;
import view.AlertMessage;
import view.Check;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AddEditDeviceGroupController {

    @FXML
    private Label groupLabel;

    @FXML
    private TextField groupField;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;

    private DbConnection dbConnection = new DbConnection();

    private static Stage stage;
    private static DeviceGroup deviceGroup;
    private static boolean isUpdate;

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setDeviceGroup(DeviceGroup outDeviceGroup) {
        deviceGroup = outDeviceGroup;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    private String oldGroup = "";

    @FXML
    private void initialize() {
        if (isUpdate)
            oldGroup = deviceGroup.getGroupName();
        groupField.setText(oldGroup);
    }

    @FXML
    private void okAction() {
        String groupName = groupField.getText().trim();
        if (isFieldRight(groupName)) {
            if (isUpdate) {
                editGroup(groupName);
                closeWindow();
            } else {
                addGroup(groupName);
                closeWindow();
            }
        } else groupField.setText(groupName);
    }

    private void editGroup(String groupName) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "UPDATE device_group SET groupName = ? WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, groupName);
            preState.setInt(2, deviceGroup.getId());
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при редактировании типа оборудования", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    public void addGroup(String groupName) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO device_group (groupName) VALUES (?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, groupName);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении типа оборудования", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private boolean isFieldRight(String deviceGroup) {
        if (!Check.isFieldNull(deviceGroup)) {
            if (!oldGroup.equals(deviceGroup))
                if (!Check.isNewGroup(deviceGroup)) {
                    AlertMessage.showSameName(groupLabel.getText());
                    return false;
                }
            return true;
        } else {
            AlertMessage.showEmptyFieldMessage(groupLabel.getText());
            return false;
        }
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}
