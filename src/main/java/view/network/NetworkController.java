package view.network;

import view.AlertMessage;
import view.Check;
import model.Network;
import mainpackage.Main;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.beans.value.ChangeListener;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class NetworkController {

    @FXML
    private ListView<Network> subnetView;

    @FXML
    private TextField areaField;

    @FXML
    private TextField vlanField;

    @FXML
    private TextField subnetField;

    @FXML
    private TextField gatewayField;

    @FXML
    private TextField maskField;

    @FXML
    private Button addBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button delBtn;

    private DbConnection dbConnection = new DbConnection();

    private static Stage stage;
    public static void setNetworkStage(Stage outStage) {
        stage = outStage;
    }

    private ObservableList<Network> subnetList;

    private ChangeListener<Network> networkListener = (observable, oldNet, newNet) -> {
        if (newNet != null) {
            loadSubNetInfo(newNet);
        } else {
            setEmptyData();
        }
    };

    private void loadSubNetInfo(Network network) {
        String areaName = getAreaName(network.getAreaId());
        String subnet = network.getSubNetwork();
        String mask = String.valueOf(network.getMask());
        String gateway = network.getGateway();
        String vlanId = String.valueOf(network.getVlanId());

        areaField.setText(areaName);
        subnetField.setText(subnet);
        maskField.setText(mask);
        gatewayField.setText(gateway);
        vlanField.setText(vlanId);
    }

    private String getAreaName(int areaId) {
        String areaName = "";
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT areaName FROM area WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, areaId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                areaName = resSet.getString("areaName");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске имени площадки", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return areaName;
    }

    private void setEmptyData() {
        areaField.setText("");
        subnetField.setText("");
        maskField.setText("");
        gatewayField.setText("");
        vlanField.setText("");
    }

    @FXML
    private void initialize() {
        subnetView.getSelectionModel().selectedItemProperty().removeListener(networkListener);

        loadSubnetList();
        subnetView.setItems(subnetList);

        subnetView.getSelectionModel().selectedItemProperty().addListener(networkListener);
    }

    private void loadSubnetList() {
        subnetList = FXCollections.observableArrayList();
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT id, area_id, INET_NTOA(sub_network) as subnet, mask, " +
                "INET_NTOA(gateway) as gateway, vlan_id FROM network";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                subnetList.add(new Network(resSet.getInt("id"), resSet.getInt("area_id"),
                        resSet.getString("subnet"), resSet.getByte("mask"),
                        resSet.getString("gateway"), resSet.getShort("vlan_id")));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка подсетей", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    @FXML
    private void deleteNetwork() {
        if (Check.isSelectedListView(subnetView)) {
            int index = subnetView.getSelectionModel().getSelectedIndex();
            Network subnetToDel = subnetView.getSelectionModel().getSelectedItem();
                Connection conn = null;
                PreparedStatement preState = null;
                String query = "DELETE FROM network WHERE id = ?";
                try {
                    conn = dbConnection.getDbConnection();
                    preState = conn.prepareStatement(query);
                    preState.setInt(1, subnetToDel.getId());
                    preState.executeUpdate();
                    initialize();
                    subnetView.getSelectionModel().select(Check.selectIndex(index));
                } catch (SQLException e) {
                    dbConnection.setLog("Ошибка при удалении подсети", e);
                } finally {
                    dbConnection.closeConnection(conn, preState);
                }
        } else AlertMessage.showNotSelectedElement();
    }

    @FXML
    private void goToAddView() {
        int selectedIndex = subnetView.getSelectionModel().getSelectedIndex();
        String title = "Добавление";

        Stage stage = createNewWindow(title);
        AddEditNetworkController.setStage(stage);

        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();

        initialize();
        subnetView.getSelectionModel().select(selectedIndex);
    }

    @FXML
    private void goToEditView() {
        if (Check.isSelectedListView(subnetView)) {
            int selectedIndex = subnetView.getSelectionModel().getSelectedIndex();
            String title = "Редактирование";

            Network network = subnetView.getSelectionModel().getSelectedItem();
            AddEditNetworkController.setNetwork(network);
            AddEditNetworkController.setIsUpdate(true);

            Stage stage = createNewWindow(title);
            AddEditNetworkController.setStage(stage);

            assert stage != null;
            stage.setResizable(false);
            stage.showAndWait();
            AddEditNetworkController.setIsUpdate(false);

            initialize();
            subnetView.getSelectionModel().select(selectedIndex);
        } else AlertMessage.showNotSelectedElement();
    }

    private Stage createNewWindow(String title) {
        String view = "/AddEditNetwork.fxml";
        String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        AddEditNetworkController.setStage(stage);

        return stage;
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}
