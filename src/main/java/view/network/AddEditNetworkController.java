package view.network;

import javafx.beans.value.ChangeListener;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import model.Network;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import view.AlertMessage;
import view.Check;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddEditNetworkController {

    @FXML
    private ComboBox<String> areaNameBox;

    @FXML
    private TextField subnetField;

    @FXML
    private TextField maskField;

    @FXML
    private TextField gatewayField;

    @FXML
    private TextField vlanField;

    @FXML
    private Button okBtn;

    @FXML
    private Button cancelBtn;

    private final static String AREALABEL = "площадка";
    private final static String SUBNETLABEL = "подсеть";
    private final static String MASKLABEL = "маска";
    private final static String GATEWAYLABEL = "шлюз";
    private final static String VLANLABEL = "vlan";

    private static Stage stage;
    private static Network network;
    private static boolean isUpdate;

    static void setStage(Stage outStage) {
        stage = outStage;
    }

    static void setNetwork(Network outNetwork) {
        network = outNetwork;
    }

    static void setIsUpdate(boolean value) {
        isUpdate = value;
    }

    private ChangeListener<String> maskListener = (observable, oldMask, newMask) -> {
        if (!Check.isFieldNull(newMask)) {
            if (Check.isDigit(newMask)) {
                int mask = Integer.valueOf(newMask);
                if (mask < 0 || mask > 32) {
                    maskField.setText(String.valueOf(oldMask));
                    AlertMessage.showNotValidRange(MASKLABEL, "0", "32");
                }
            } else {
                maskField.setText(oldMask);
                AlertMessage.showNotDigitMessage(MASKLABEL);
            }
        }
    };
    private ChangeListener<String> vlanListener = (observable, oldVlan, newVlan) -> {
        if (!Check.isFieldNull(newVlan)) {
            if (Check.isDigit(newVlan)) {
                int vlan = Integer.valueOf(newVlan);
                if (vlan < 0 || vlan > 4094) {
                    vlanField.setText(String.valueOf(oldVlan));
                    AlertMessage.showNotValidRange(VLANLABEL, "2", "4094");
                }
            } else {
                vlanField.setText(oldVlan);
                AlertMessage.showNotDigitMessage(VLANLABEL);
            }
        }
    };

    private DbConnection dbConnection = new DbConnection();

    private String oldSubnet = "";
    private String oldMask = "";
    private String oldVlan = "";

    @FXML
    private void initialize() {
        removeListeners();

        setIpAddressFilter();
        if (isUpdate) {
            oldSubnet = network.getSubNetwork();
            oldMask = String.valueOf(network.getMask());
            String oldGateway = network.getGateway();
            oldVlan = String.valueOf(network.getVlanId());

            subnetField.setText(oldSubnet);
            maskField.setText(oldMask);
            gatewayField.setText(oldGateway);
            vlanField.setText(oldVlan);
        }

        setListeners();
    }


    private void removeListeners() {
        maskField.textProperty().removeListener(maskListener);
        vlanField.textProperty().removeListener(vlanListener);
    }

    private void setListeners() {
        maskField.textProperty().addListener(maskListener);
        vlanField.textProperty().addListener(vlanListener);
    }

    @FXML
    private void okAction() {
        String addQuery = "INSERT INTO network (area_id, sub_network, mask, gateway, vlan_id) " +
                "VALUES (0,INET_ATON(?), ?, INET_ATON(?), ?)";
        String saveQuery = "UPDATE network SET sub_network = INET_ATON(?), " +
                "mask = ?, gateway = INET_ATON(?), vlan_id = ? " +
                "WHERE id = ?";
        ArrayList<String> networkList = new ArrayList<>();
        networkList = fillNetworkList(networkList);
        if (isFieldsValid(networkList)) {
            if (isUpdate)
                saveNetwork(saveQuery, networkList);
            else saveNetwork(addQuery, networkList);
        }
    }

    private ArrayList<String> fillNetworkList(ArrayList<String> networkList) {
        String subnet = subnetField.getText().trim();
        String mask = maskField.getText().trim();
        String gateway = gatewayField.getText().trim();
        String vlan = vlanField.getText().trim();

        networkList.add(subnet);
        networkList.add(mask);
        networkList.add(gateway);
        networkList.add(vlan);

        return networkList;
    }

    private void saveNetwork(String query, ArrayList<String> networkList) {
        String subnet = networkList.get(1);
        Byte mask = Byte.valueOf(networkList.get(2));
        String gateway = networkList.get(3);
        Short vlanId = Short.valueOf(networkList.get(4));
        Connection conn = null;
        PreparedStatement preState = null;
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, subnet);
            preState.setByte(2, mask);
            preState.setString(3, gateway);
            preState.setInt(4, vlanId);
            if (isUpdate)
                preState.setInt(5, network.getId());
            preState.executeUpdate();
            closeWindow();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при сохранении подсети", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private boolean isFieldsValid(ArrayList<String> networkList) {
        String subnet = networkList.get(0);
        String mask = networkList.get(1);
        String gateway = networkList.get(2);
        String vlanId = networkList.get(3);
        return isNetValid(subnet) && isMaskValid(mask)
                && isGatewayValid(gateway) && isVlanValid(vlanId);
    }

    private boolean isNetValid(String subnet) {
        if (!Check.isFieldNull(subnet)) {
            return subnet.equals(oldSubnet) || isSubnetNew(subnet);
        } else {
            AlertMessage.showEmptyFieldMessage(SUBNETLABEL);
            return false;
        }
    }

    private boolean isSubnetNew(String subnet) {
        if (isSubnetFormatValid(subnet)) {
            if (Check.isNewSubnet(subnet)) {
                return true;
            } else {
                AlertMessage.showSameName(SUBNETLABEL);
                return false;
            }
        } else {
            AlertMessage.showSubnetWrongFormat(SUBNETLABEL);
            return false;
        }
    }

    private boolean isMaskValid(String mask) {
        if (!Check.isFieldNull(mask)) {
            return mask.equals(oldMask) || isMaskDigit(mask);
        } else {
            AlertMessage.showEmptyFieldMessage(MASKLABEL);
            return false;
        }
    }

    private boolean isMaskDigit(String mask) {
        assert mask != null;
        if (Check.isDigit(mask))
            return true;
        else {
            AlertMessage.showNotDigitMessage(MASKLABEL);
            return false;
        }
    }

    private boolean isGatewayValid(String gateway) {
        if (!Check.isFieldNull(gateway)) {
            if (isSubnetFormatValid(gateway)) {
                return true;
            } else {
                AlertMessage.showSubnetWrongFormat(GATEWAYLABEL);
                return false;
            }
        } else {
            AlertMessage.showEmptyFieldMessage(GATEWAYLABEL);
            return false;
        }
    }

    private boolean isVlanValid(String vlanId) {
        if (!Check.isFieldNull(vlanId)) {
            if (Check.isDigit(vlanId))
                return oldVlan.equals(vlanId) || isVlanExist(vlanId);
            else {
                AlertMessage.showNotDigitMessage(VLANLABEL);
                return false;
            }
        } else {
            AlertMessage.showEmptyFieldMessage(VLANLABEL);
            return false;
        }
    }

    private boolean isVlanExist(String shortVlanId) {
        short vlanId = Short.valueOf(shortVlanId);
        if (Check.isNewVlan(vlanId)) {
            return isOkNewVlan(vlanId);
        }
        return isVlanNotUse(vlanId);
    }

    private boolean isOkNewVlan(short vlandId) {
        if (AlertMessage.isOkNewValue(AREALABEL)) {
            addNewVlan(vlandId);
            return true;
        } else return false;
    }

    private void addNewVlan(short vlanId) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO vlan (vlanId, vlanName, vlanContract_id) VALUES (?,?,0)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, vlanId);
            String vlanName = "VLAN" + vlanId;
            preState.setString(2, vlanName);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при добавлении нового vlan", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private boolean isVlanNotUse(short vlanToCompare) {
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT vlan_id FROM network";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                short vlanId = resSet.getShort("vlan_id");
                if (vlanId == vlanToCompare) {
                    AlertMessage.showAlreadyInUse(VLANLABEL);
                    return false;
                }
            }
        } catch (SQLException e) {
            dbConnection.setLog("Такой vlan уже используется в другой подсети", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return true;
    }

    private boolean isSubnetFormatValid(String ipAddress) {
        Pattern pattern;
        Matcher matcher;
        String IPADDRESS_PATTERN
                = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
                + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
                + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
                + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
        pattern = Pattern.compile(IPADDRESS_PATTERN);
        matcher = pattern.matcher(ipAddress);
        return matcher.matches();
    }

    private void setIpAddressFilter() {
        String regex = makePartialIPRegex();
        final UnaryOperator<TextFormatter.Change> ipAddressFilter = c -> {
            String text = c.getControlNewText();
            if (text.matches(regex)) {
                return c;
            } else {
                return null;
            }
        };
        subnetField.setTextFormatter(new TextFormatter<>(ipAddressFilter));
        gatewayField.setTextFormatter(new TextFormatter<>(ipAddressFilter));
    }

    private String makePartialIPRegex() {
        String partialBlock = "(([01]?[0-9]{0,2})|(2[0-4][0-9])|(25[0-5]))";
        String subsequentPartialBlock = "(\\." + partialBlock + ")";
        String ipAddress = partialBlock + "?" + subsequentPartialBlock + "{0,3}";
        return "^" + ipAddress;
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}
