package view.vlan;

import view.Check;
import view.AlertMessage;
import dbpackage.DbConnection;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TreeItem;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TreeView;
import javafx.scene.control.TextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.beans.value.ChangeListener;

import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class VlanController {

    @FXML
    private Label vlanNumLabel;

    @FXML
    private Label vlanNameLabel;

    @FXML
    private Label providerLabel;

    @FXML
    private Label vlanContLabel;

    @FXML
    private TreeView<String> vlanTree = new TreeView<>();

    @FXML
    private TextField vlanNumField;

    @FXML
    private TextField vlanNameField;

    @FXML
    private ComboBox<String> providerBox;

    @FXML
    private ComboBox<String> vlanContractBox;

    @FXML
    private Button addBtn;

    @FXML
    private Button deleteBtn;

    @FXML
    private Button saveBtn;

    @FXML
    private Button cancelBtn;

    @FXML
    private Button exitBtn;

    private final static String VLANLABEL = "vlan";

    private DbConnection dbConnection = new DbConnection();

    private ObservableList<String> vlanList;
    private ObservableList<String> providerList;
    private ObservableList<String> vlanContractList;

    private String oldVlanId = "";
    private String oldVlanName = "";
    private String oldProvider = "";
    private String oldVlanContract = "";

    private static Stage stage;

    private ChangeListener<String> vlanNumListener = (observable, oldValue, newValue) -> {
        if (!Check.isFieldNull(newValue)) {
            if (Check.isDigit(newValue)) {
                int newVlan = Integer.valueOf(newValue);
                if (newVlan > 0 && newVlan < 4095) {
                    if (!isPreviousData()) {
                        setSaveCancelBtnDisable(false);
                        if (!vlanList.contains(newValue)) {
                            addBtn.setDisable(false);
                        } else addBtn.setDisable(true);
                    } else {
                        setSaveCancelBtnDisable(true);
                        addBtn.setDisable(true);
                    }
                } else {
                    AlertMessage.showNotValidRange(VLANLABEL, "2", "4094");
                    vlanNumField.setText(oldValue);
                }
            } else {
                AlertMessage.showNotDigitMessage(vlanNumLabel.getText());
                vlanNumField.setText(oldValue);
            }
        } else if (isPreviousData()) {
            setSaveCancelBtnDisable(true);
            addBtn.setDisable(true);
            deleteBtn.setDisable(true);
        }
    };

    private ChangeListener<String> vlanNameListener = (observable, oldValue, newValue) -> {
        if (!isPreviousData()) {
            setSaveCancelBtnDisable(false);
        } else {
            setSaveCancelBtnDisable(true);
            addBtn.setDisable(true);
        }
    };

    private ChangeListener<String> providerListener = (observable, oldValue, newValue) -> {
        ObservableList<String> oldVlanContList = vlanContractList;
        if (!isPreviousData()) {
            setSaveCancelBtnDisable(false);
            initVlanContBox(newValue);
        } else {
            setSaveCancelBtnDisable(true);
            addBtn.setDisable(true);
            vlanContractBox.setItems(oldVlanContList);
            vlanContractBox.getSelectionModel().select(oldVlanContract);
        }
    };

    private ChangeListener<String> vlanContractListener = (observable, oldValue, newValue) -> {
        if (oldVlanContract == null) oldVlanContract = "";
        if (!isPreviousData()) {
            setSaveCancelBtnDisable(false);
        } else {
            setSaveCancelBtnDisable(true);
            addBtn.setDisable(true);
        }
    };

    private ChangeListener<TreeItem<String>> vlanTreeListener = (observable, oldValue, newValue) -> {
        if (newValue != null)
            if (!(newValue.getValue()).equals("VLAN")) {
                loadVlanInfo(newValue.getValue());
                deleteBtn.setDisable(false);
            } else {
                setDefaultData();
            }
    };

    //---- Get stage from main form to closeWindow method;
    public static void setStage(Stage outStage) {
        stage = outStage;
    }

    //---- Set visibility for Btns
    private void setSaveCancelBtnDisable(boolean visibility) {
        saveBtn.setDisable(visibility);
        cancelBtn.setDisable(visibility);
    }

    //---- Load all lists and set listeners for selected item
    @FXML
    private void initialize() {
        removeListeners();
        setSaveCancelBtnDisable(true);
        addBtn.setDisable(true);
        deleteBtn.setDisable(true);
        vlanList = FXCollections.observableArrayList();
        loadVlanList();
        providerList = FXCollections.observableArrayList();
        loadProviderList();
        vlanContractList = FXCollections.observableArrayList();

        TreeItem<String> root = new TreeItem<>("VLAN");
        root.setExpanded(true);

        vlanTree.setRoot(root);

        for (String vlanNum : vlanList) {
            makeDevcieBranch(vlanNum, root);
        }

        vlanTree.getSelectionModel().select(searchTreeItem(root, oldVlanId));

        FXCollections.sort(providerList);
        providerBox.setItems(providerList);
        addListeners();
    }

    private void setDefaultData() {
        oldVlanId = "";
        oldVlanName = "";
        oldProvider = "";
        oldVlanContract = "";
        vlanNumField.setText(oldVlanId);
        vlanNameField.setText(oldVlanName);
        providerBox.setItems(providerList);
        providerBox.getSelectionModel().select("");
        vlanContractBox.setItems(null);
        addBtn.setDisable(true);
        deleteBtn.setDisable(true);
    }

    //---- Load vlan ObservableList<String>
    private void loadVlanList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM vlan";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                int vlanId = resSet.getInt("vlanId");
                if (vlanId != 0) {
                    vlanList.add(String.valueOf(vlanId));
                }
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка Vlan", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    //---- Load provider name list
    private void loadProviderList() {
        Connection conn = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM provider";
        try {
            conn = dbConnection.getDbConnection();
            resSet = conn.prepareStatement(query).executeQuery();
            while (resSet.next()) {
                providerList.add(resSet.getString("providerName"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка имен провайдеров", e);
        } finally {
            dbConnection.closeConnection(conn, resSet);
        }
    }

    //---- Load vlan contract number string list
    private void loadVlanContractList(int providerId) {
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM vlan_contract WHERE provider_id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, providerId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                vlanContractList.add(resSet.getString("vlanContNumber"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке списка контрактов на Vlan", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
    }

    //---- Load and set all info about vlan into comboBox
    private void loadVlanInfo(String vlan) {
        int vlanId = Integer.valueOf(vlan);
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT vlanId, vlanName, providerName, vlanContNumber FROM vlan\n" +
                "LEFT JOIN vlan_contract on vlan.vlanContract_id = vlan_contract.id\n" +
                "LEFT JOIN provider ON vlan_contract.provider_id = provider.id\n" +
                "WHERE vlan.vlanid = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, vlanId);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                oldVlanId = resSet.getString("vlanId");
                oldVlanName = resSet.getString("vlanName");
                oldProvider = resSet.getString("providerName");
                oldVlanContract = resSet.getString("vlanContNumber");
                vlanNumField.setText(oldVlanId);
                if (Check.isFieldNull(oldVlanName)) {
                    oldVlanName = "не задано";
                    vlanNameField.setText(oldVlanName);
                } else vlanNameField.setText(oldVlanName);
                providerBox.getSelectionModel().select(oldProvider);
                initVlanContBox(oldProvider);
                vlanContractBox.getSelectionModel().select(oldVlanContract);
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка во время загрузки информации по Vlan", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
    }

    //---- Value saveOrUpdate for saveQuery method if construction
    //---- If true -> saveVlan, false -> update
    @FXML
    private void saveVlan() {
        boolean isSave = !oldVlanId.equals("");
        String vlanNum = vlanNumField.getText().trim();
        String newVlanName = vlanNameField.getText().trim();
        String newProvider = providerBox.getEditor().getText().trim();
        String newVlanContract = vlanContractBox.getEditor().getText().trim();
        if (isFieldsRight(vlanNum, newVlanName, newProvider, newVlanContract)) {
            int newVlanId = Integer.valueOf(vlanNum);
            String queryInsertVlan = "INSERT INTO vlan (vlanId, vlanName, vlanContract_id) VALUES (?,?,?)";
            String queryUpdateVlan = "UPDATE vlan SET vlanId = ?, vlanName = ?, " +
                    "vlanContract_id = ? WHERE vlanId = ?";
            int vlanContractId = findVlanContId(newVlanContract);
            if (isSave) {
                saveQuery(queryUpdateVlan, newVlanId, newVlanName, vlanContractId);
                oldVlanId = String.valueOf(newVlanId);
                initialize();
            } else {
                saveQuery(queryInsertVlan, newVlanId, newVlanName, vlanContractId);
                oldVlanId = String.valueOf(newVlanId);
                initialize();
            }
        }
    }

    @FXML
    private void cancel() {
        vlanNumField.setText(oldVlanId);
        vlanNameField.setText(oldVlanName);
        providerBox.getSelectionModel().select(oldProvider);
        vlanContractBox.getSelectionModel().select(oldVlanContract);
        setSaveCancelBtnDisable(true);
        addBtn.setDisable(true);
        deleteBtn.setDisable(true);
    }

    @FXML
    private void addVlan() {
        String tmp = oldVlanId;
        oldVlanId = "";
        saveVlan();
        oldVlanId = tmp;
    }

    @FXML
    private void deleteVlan() {
        if (Check.isSelectedTreeView(vlanTree)) {
            int selectedIndex = vlanTree.getSelectionModel().getSelectedIndex();
            String vlanToDel = vlanTree.getSelectionModel().getSelectedItem().getValue();
            if (!vlanToDel.equals("VLAN")) {
                int vlanIdToDel = Integer.valueOf(vlanToDel);
                changeVlanIdAtNet(vlanIdToDel);
                Connection conn = null;
                PreparedStatement preState = null;
                String query = "DELETE FROM vlan WHERE vlanId = ?";
                try {
                    conn = dbConnection.getDbConnection();
                    preState = conn.prepareStatement(query);
                    preState.setInt(1, vlanIdToDel);
                    preState.executeUpdate();
                    initialize();
                    vlanTree.getSelectionModel().select(Check.selectIndex(selectedIndex));
                } catch (SQLException e) {
                    dbConnection.setLog("Ошибка при удалении Vlan", e);
                } finally {
                    dbConnection.closeConnection(conn, preState);
                }
            }
        } else AlertMessage.showNotSelectedElement();
    }

    private void changeVlanIdAtNet(int vlanId) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "UPDATE network SET vlan_id = 0 WHERE vlan_id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, vlanId);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при замене Vlan во время удаления", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void saveQuery(String query, int vlanId, String vlanName, int vlanContractId) {
        Connection conn = null;
        PreparedStatement preState = null;
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, vlanId);
            preState.setString(2, vlanName);
            preState.setInt(3, vlanContractId);
            if (!oldVlanId.equals("")) {
                preState.setInt(4, Integer.valueOf(oldVlanId));
            }
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при сохранении Vlan", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    //---- Load VlanContract list, that provider has, to the vlanContractBox
    private void initVlanContBox(String vlanCont) {
        if (vlanCont != null) {
            int providerId = findProviderId(vlanCont);
            if (providerId != 0) {
                vlanContractList = FXCollections.observableArrayList();
                loadVlanContractList(providerId);
                FXCollections.sort(vlanContractList);
                vlanContractBox.setItems(vlanContractList);
                vlanContractBox.getSelectionModel().selectFirst();
            } else {
                vlanContractBox.setItems(null);
            }
        }
    }

    private void createNewProvider(String provider) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO provider (providerName) VALUES (?)";

        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, provider);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при сохранении нового провайдера", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void createNewVlanContract(int providerId, String vlanContract) {
        Connection conn = null;
        PreparedStatement preState = null;
        String query = "INSERT INTO vlan_contract (provider_id, vlanContNumber) VALUES (?,?)";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, providerId);
            preState.setString(2, vlanContract);
            preState.executeUpdate();
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при сохранении нового контракта на Vlan", e);
        } finally {
            dbConnection.closeConnection(conn, preState);
        }
    }

    private void addListeners() {
        vlanNumField.textProperty().addListener(vlanNumListener);
        vlanNameField.textProperty().addListener(vlanNameListener);
        providerBox.getEditor().textProperty().addListener(providerListener);
        vlanContractBox.getEditor().textProperty().addListener(vlanContractListener);
        vlanTree.getSelectionModel().selectedItemProperty().addListener(vlanTreeListener);
    }

    private void removeListeners() {
        vlanNumField.textProperty().removeListener(vlanNumListener);
        vlanNameField.textProperty().removeListener(vlanNameListener);
        providerBox.getEditor().textProperty().removeListener(providerListener);
        vlanContractBox.getEditor().textProperty().removeListener(vlanContractListener);
        vlanTree.getSelectionModel().selectedItemProperty().removeListener(vlanTreeListener);
    }

    private int findProviderId(String value) {
        int providerId = 0;
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT id FROM provider WHERE providerName = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, value);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                providerId = resSet.getInt("id");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка во время поиска провайдера", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return providerId;
    }

    private int findVlanContId(String vlanContract) {
        int vlanContractid = 0;
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT id FROM vlan_contract WHERE vlanContNumber LIKE ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setString(1, vlanContract);
            resSet = preState.executeQuery();
            while (resSet.next()) {
                vlanContractid = resSet.getInt("id");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка во время поиска контракта на Vlan", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return vlanContractid;
    }

    //---- isSave resolve problem with sameValue check. (saveVlan do not allowed cause same vlan)
    private boolean isFieldsRight(String vlanNumber, String vlanName, String providerName, String vlanCont) {
        if (!Check.isFieldNull(vlanNumber)) {
            if (Check.isDigit(vlanNumber)) {
                short vlanId = Short.valueOf(vlanNumber);
                if (vlanId > 1 && vlanId < 4095) {
                    if (!oldVlanId.equals(vlanNumber)) {
                        if (!Check.isNewVlan(vlanId)) {
                            AlertMessage.showSameName(vlanNumLabel.getText());
                            return false;
                        }
                    }
                    if (Check.isFieldNull(vlanName))
                        if (!AlertMessage.isOkEmtyField(vlanNameLabel.getText())) return false;
                    if (vlanName.length() < 45) {
                        if (!Check.isFieldNull(providerName)) {
                            if (providerName.length() < 45) {
                                if (Check.isNewProvider(providerName)) {
                                    createNewProvider(providerName);
                                }
                                if (!Check.isFieldNull(vlanCont)) {
                                    if (vlanCont.length() < 100) {
                                        int providerId = findProviderId(providerName);
                                        if (!oldProvider.equals(providerName) || !oldVlanContract.equals(vlanCont)) {
                                            if (Check.isNewVlanContract(providerId, vlanCont)) {
                                                createNewVlanContract(providerId, vlanCont);
                                            }
                                        }
                                        return true;
                                    } else {
                                        AlertMessage.showStrValueTooBig(vlanContLabel.getText());
                                        return false;
                                    }
                                } else {
                                    AlertMessage.showEmptyFieldMessage(vlanContLabel.getText());
                                    return false;
                                }
                            } else {
                                AlertMessage.showStrValueTooBig(providerLabel.getText());
                                return false;
                            }
                        } else {
                            AlertMessage.showEmptyFieldMessage(providerLabel.getText());
                            return false;
                        }
                    } else {
                        AlertMessage.showStrValueTooBig(vlanNameLabel.getText());
                        return false;
                    }

                } else {
                    AlertMessage.showNotValidRange(VLANLABEL, "2", "4094");
                    return false;
                }
            } else {
                AlertMessage.showNotDigitMessage(vlanNumLabel.getText());
                return false;
            }
        } else {
            AlertMessage.showEmptyFieldMessage(vlanNumLabel.getText());
            return false;
        }
    }


    private boolean isPreviousData() {
        String vlanNumText = vlanNumField.getText().trim();
        String vlanNameText = vlanNameField.getText().trim();
        String providerText = providerBox.getEditor().getText().trim();
        String contNumText = vlanContractBox.getEditor().getText().trim();
        if (vlanNumText.equals(oldVlanId)) {
            if (vlanNameText.equals(oldVlanName)) {
                if (providerText.equals(oldProvider)) {
                    if (contNumText.equals(oldVlanContract)) {
                        return true;
                    } else return false;
                } else return false;
            } else return false;
        } else return false;
    }


    private void makeDevcieBranch(String title, TreeItem<String> parent) {
        TreeItem<String> item = new TreeItem<>(title);
        item.setExpanded(true);
        parent.getChildren().add(item);
    }

    private TreeItem<String> searchTreeItem(TreeItem<String> item, String searchItem) {

        if (item.getValue().equals(searchItem)) return item; // hit!

        // continue on the children:
        TreeItem<String> result;
        for (TreeItem<String> child : item.getChildren()) {
            result = searchTreeItem(child, searchItem);
            if (result != null) return result; // hit!
        }
        setDefaultData();
        return null;
    }

    @FXML
    private void closeWindow() {
        stage.close();
    }
}
