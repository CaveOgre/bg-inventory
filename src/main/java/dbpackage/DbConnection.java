package dbpackage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbConnection {

    public Connection getDbConnection() {

        String BDURL = "jdbc:mysql://192.168.1.13:3306/bginventar?useSSL=true";
        String USER = "root";
        String PASSWORD = "root";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection(BDURL, USER, PASSWORD);
        } catch (ClassNotFoundException | SQLException sqlExp) {
            Logger.getLogger(DbConnection.class.getName()).log(Level.SEVERE, null, sqlExp);
        }
        return null;
    }

    public void closeConnection(Connection conn, PreparedStatement preState, ResultSet resSet){
        try{
            assert conn != null;
            conn.close();
        } catch (SQLException e){
            setLog("Ошибка при закрытии соединения", e);
        }
        try{
            assert preState != null;
            preState.close();
        } catch (SQLException e){
            setLog("Ошибка при закрытии preparedStatement", e);
        }
        try{
            assert resSet != null;
            resSet.close();
        } catch (SQLException e){
            setLog("Ошибка при закрытии resultSet", e);
        }
    }

    public void closeConnection(Connection conn, PreparedStatement preState){
        try{
            assert conn != null;
            conn.close();
        } catch (SQLException e){
            setLog("Ошибка при закрытии соединения", e);
        }
        try{
            assert preState != null;
            preState.close();
        } catch (SQLException e){
            setLog("Ошибка при закрытии preparedStatement", e);
        }
    }

    public void closeConnection(Connection conn, ResultSet resSet){
        try{
            assert conn != null;
            conn.close();
        } catch (SQLException e){
            setLog("Ошибка при закрытии соединения", e);
        }
        try{
            assert resSet != null;
            resSet.close();
        } catch (SQLException e){
            setLog("Ошибка при закрытии resultSet", e);
        }
    }

    public void closeConnection(ResultSet resSet) {
        try{
            assert resSet != null;
            resSet.close();
        } catch (SQLException e){
            setLog("Ошибка при закрытии resultSet", e);
        }
    }

    public void setLog(String text, SQLException e) {
        Logger logger = Logger.getLogger(getClass().getName());
        logger.log(Level.SEVERE, text, e);
    }

    public void setLog(String text, IOException e) {
        Logger logger = Logger.getLogger(getClass().getName());
        logger.log(Level.SEVERE, text, e);
    }
}
