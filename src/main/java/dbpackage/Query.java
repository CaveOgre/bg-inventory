package dbpackage;

import model.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Date;
import java.sql.SQLException;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class Query {


/*
    public List<Address> getAddressList(ResultSet resSet) {
        final String query = "SELECT * FROM address";
        List<Address> addressList = new ArrayList<>();
        try {
            openForSelect(query);
            while (resSet.next()) {
                int id = resSet.getInt("id");
                int cityId = resSet.getInt("city_id");
                int streetId = resSet.getInt("street_id");
                int building = resSet.getInt("building");
                Address address = new Address(id,cityId,streetId,building);
                addressList.add(address);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeForSelect();
        }
        return addressList;
    }

    public List<Area> getAreaList(){
        final String query = "SELECT*FROM area";
        List<Area> areasList = new ArrayList<>();
        try {
            openForSelect(query);
            while (resSet.next()) {
                int id = resSet.getInt("id");
                int areaNameId = resSet.getInt("areaName_id");
                boolean crossroad = resSet.getBoolean("crossroad");
                int firstStreetId = resSet.getInt("firstStreet_id");
                int secondStreetId = resSet.getInt("secondStreet_id");
                int powerPointId = resSet.getInt("powerPoint_id");
                Area area = new Area(id, areaNameId, crossroad,
                                        firstStreetId, secondStreetId, powerPointId);
                areasList.add(area);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return areasList;
    }

    public List<AreaName> getAreaNameList(){
        final String query = "SELECT*FROM area_name";
        List<AreaName> areaNamesList = new ArrayList<>();
        try{
            openForSelect(query);
            while (resSet.next()){

                int id = resSet.getInt("id");
                String areaNameWord = resSet.getString("areaName");
                AreaName areaName = new AreaName(id, areaNameWord);
                areaNamesList.add(areaName);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return areaNamesList;
    }

    public List<BgStage> getBgStageList(){
        final String query = "SELECT*FROM bg_stage";
        List<BgStage> bgStageList = new ArrayList<>();
        try{
            openForSelect(query);
            while(resSet.next()){
                int id = resSet.getInt("id");
                byte stageNumber = resSet.getByte("stageNumber");
                Date year = resSet.getDate("year");
                BgStage bgStage = new BgStage(id, stageNumber, year);
                bgStageList.add(bgStage);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return bgStageList;
    }

    public List<CameraType> getCameraTypeList(){
        final String query = "SELECT*FROM camera_type";
        List<CameraType> cameraTypeList = new ArrayList<>();
        try {
            openForSelect(query);
            while(resSet.next()){
                int id = resSet.getInt("id");
                String cameraTypeName = resSet.getString("cameraType");
                CameraType cameraType = new CameraType(id, cameraTypeName);
                cameraTypeList.add(cameraType);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return cameraTypeList;
    }

    public List<Characteristic> getCharacteristicList(){
        final String query = "SELECT*FROM characteristic";
        List<Characteristic> characteristicList = new ArrayList<>();
        try {
            openForSelect(query);
            while (resSet.next()){
                int id = resSet.getInt("id");
                int vendorId = resSet.getInt("vendor_id");
                int modelId = resSet.getInt("model_id");
                int deviceTypeId = resSet.getInt("deviceType_id");
                int cameraTypeId = resSet.getInt("cameraType_id");
                String cameraNum = resSet.getString("cameraNum");
                int serverId = resSet.getInt("server_id");
                boolean issConn = resSet.getBoolean("iss_conn");
                String login = resSet.getString("login");
                String password = resSet.getString("password");
                int connectionId = resSet.getInt("connection_id");
                int powerId = resSet.getInt("power_id");
                String inventar = resSet.getString("inventar");
                String serial = resSet.getString("serial");
                Characteristic characteristic = new Characteristic(id, vendorId, modelId, deviceTypeId, cameraTypeId,
                                                                    cameraNum, serverId, issConn, login, password,
                                                                    connectionId, powerId, inventar, serial);
                characteristicList.add(characteristic);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return characteristicList;
    }


    public List<model.DeviceConnection> getConnectionList(){
        final String query = "SELECT*FROM connection";
        List<model.DeviceConnection> connectionList = new ArrayList<>();
        try{
            openForSelect(query);
            while (resSet.next()){
                int id = resSet.getInt("id");
                String connType = resSet.getString("connType");
                model.DeviceConnection connection = new model.DeviceConnection(id, connType);
                connectionList.add(connection);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return connectionList;
    }

    public List<Device> getDeviceList(){
        final String query = "SELECT*FROM device";
        List<Device> deviceList = new ArrayList<>();
        try {
            openForSelect(query);
            while (resSet.next()){
                int id = resSet.getInt("id");
                int areaId = resSet.getInt("area_id");
                int groupDeviceId = resSet.getInt("groupDevice_id");
                int characteristicsId = resSet.getInt("characteristics_id");
                int networkId = resSet.getInt("network_id");
                int ipAddress = resSet.getInt("ipAddress");
                int deviceContractId = resSet.getInt("deviceContract_id");
                Date turnOnDate = resSet.getDate("turn_on_date");
                Date turnOffDate = resSet.getDate("turn_off_date");
                int bgStageId = resSet.getInt("bgStage_id");
                int deviceStateId = resSet.getInt("deviceState_id");
                Device device = new Device(id, areaId, groupDeviceId, characteristicsId, networkId, ipAddress,
                                            deviceContractId, turnOnDate, turnOffDate, bgStageId, deviceStateId);
                deviceList.add(device);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return deviceList;
    }

    public List<DeviceContract> getDeviceContractIdList(){
        final String query = "SELECT*FROM device_contract";
        List<DeviceContract> deviceContractList = new ArrayList<>();
        try {
            openForSelect(query);
            while (resSet.next()){
                int id = resSet.getInt("id");
                String contractNumber = resSet.getString("contractNumber");
                int deviceVendorId = resSet.getInt("deviceVendor_id");
                byte garant = resSet.getByte("garant");
                DeviceContract deviceContract = new DeviceContract(id, contractNumber, deviceVendorId, garant);
                deviceContractList.add(deviceContract);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return deviceContractList;
    }

    public List<DeviceGroup> deviceGroupList() {
        final String query = "SELECT*FROM device_group";
        List<DeviceGroup> deviceGroupList = new ArrayList<>();
        try {
            openForSelect(query);
            while (resSet.next()) {
                int id = resSet.getInt("id");
                String groupName = resSet.getString("groupName");
                DeviceGroup deviceGroup = new DeviceGroup(id, groupName);
                deviceGroupList.add(deviceGroup);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return deviceGroupList;
    }

    public List<DeviceState> getDeviceStateList(){
        final String query = "SELECT*FROM device_state";
        List<DeviceState> deviceStateList = new ArrayList<>();
        try {
            openForSelect(query);
            while (resSet.next()){
                int id = resSet.getInt("id");
                String state = resSet.getString("state");
                DeviceState deviceState = new DeviceState(id, state);
                deviceStateList.add(deviceState);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return deviceStateList;
    }

    public List<DeviceType> deviceTypeList(){
        final String query = "SELECT*FROM device_type";
        List<DeviceType> deviceTypeList = new ArrayList<>();
        try{
            openForSelect(query);
            while (resSet.next()){
                int id = resSet.getInt("id");
                String deviceTypeName = resSet.getString("deviceType");
                DeviceType deviceType = new DeviceType(id, deviceTypeName);
                deviceTypeList.add(deviceType);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return deviceTypeList;
    }

    public List<DeviceSupply> deviceVendorList(){
        final String query = "SELECT*FROM device_vendor";
        List<DeviceSupply> deviceVendorList = new ArrayList<>();
        try {
            openForSelect(query);
            while (resSet.next()) {
                int id = resSet.getInt("id");
                String vendorName = resSet.getString("vendorName");
                DeviceSupply deviceVendor = new DeviceSupply(id, vendorName);
                deviceVendorList.add(deviceVendor);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return deviceVendorList;
    }

    public List<Model> getModelList(){
        final String query = "SELECT*FROM model";
        List<Model> modelList = new ArrayList<>();
        try{
            openForSelect(query);
            while (resSet.next()){
                int id = resSet.getInt("id");
                int vendorId = resSet.getInt("vendor_id");
                String modelName = resSet.getString("modelName");
                Model model = new Model(id, vendorId, modelName);
                modelList.add(model);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return modelList;
    }

    public List<Network> networkList(){
        final String query = "SELECT*FROM network";
        List<Network> networkList = new ArrayList<>();
        try{
            openForSelect(query);
            while (resSet.next()){
                int id = resSet.getInt("id");
                int subNetwork = resSet.getInt("sub_network");
                byte mask = resSet.getByte("mask");
                short vlanId = resSet.getShort("vlan_id");
                Network network = new Network(id, subNetwork, mask, vlanId);
                networkList.add(network);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return networkList;
    }

    public List<Power> getPowerList(){
        final String query = "SELECT*FROM power";
        List<Power> powerList = new ArrayList<>();
        try {
            openForSelect(query);
            while (resSet.next()){
                int id = resSet.getInt("id");
                String powerName = resSet.getString("powerName");
                Power power = new Power(id, powerName);
                powerList.add(power);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        finally {
            closeForSelect();
        }
        return powerList;
    }

    public List<PowerPoint> getPowerPointList(){
        final String query = "SELECT*FROM power_point";
        List<PowerPoint> powerPointList = new ArrayList<>();
        try {
            openForSelect(query);
            while (resSet.next()){
                int id = resSet.getInt("id");
                String pointName = resSet.getString("pointName");
                int powerSupplyId = resSet.getInt("powerSupply_id");
                PowerPoint powerPoint = new PowerPoint(id, pointName, powerSupplyId);
                powerPointList.add(powerPoint);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return powerPointList;
    }

    public List<PowerSupply> getPowerSupplyList(){
        final String query = "SELECT*FROM power_supply";
        List<PowerSupply> powerSupplyList = new ArrayList<>();
        try{
            openForSelect(query);
            while (resSet.next()){
                int id = resSet.getInt("id");
                String powerSupName = resSet.getString("powerSupName");
                String phone = resSet.getString("phone");
                PowerSupply powerSupply = new PowerSupply(id, powerSupName, phone);
                powerSupplyList.add(powerSupply);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return powerSupplyList;
    }

    public List<Provider> getProviderList(){
        final String query = "SELECT*FROM provider";
        List<Provider> providerList = new ArrayList<>();
        try {
            openForSelect(query);
            while (resSet.next()){
                int id = resSet.getInt("id");
                String providerName = resSet.getString("providerName");
                Provider provider = new Provider(id, providerName);
                providerList.add(provider);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return providerList;
    }

    public List<Server> getServerList(){
        final String query = "SELECT*FROM server";
        List<Server> serverList = new ArrayList<>();
        try{
            openForSelect(query);
            while (resSet.next()){
                int id = resSet.getInt("id");
                String serverName = resSet.getString("serverName");
                Server server = new Server(id, serverName);
                serverList.add(server);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return serverList;
    }

    public List<Street> getStreetList(){
        final String query = "SELECT*FROM street";
        List<Street> streetList = new ArrayList<>();
        try {
            openForSelect(query);
            while (resSet.next()){
                int id = resSet.getInt("id");
                String streetName = resSet.getString("streetName");
                Street street = new Street(id, streetName);
                streetList.add(street);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return streetList;
    }

    public List<Vendor> getVendorList(){
        final String query = "SELECT*FROM vendor";
        List<Vendor> vendorList = new ArrayList<>();
        try {
            openForSelect(query);
            while (resSet.next()){
                int id = resSet.getInt("id");
                String vendorName = resSet.getString("vendorName");
                int deviceGroupId = resSet.getInt("deviceGroup_id");
                Vendor vendor = new Vendor(id, vendorName, deviceGroupId);
                vendorList.add(vendor);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return vendorList;
    }

    public List<Vlan> getVlanList(ResultSet resSet){
        final String query = "SELECT*FROM vlan";
        List<Vlan> vlanList = new ArrayList<>();
        try {
            openForSelect(query);
            while (resSet.next()){
                short vlanId = resSet.getShort("vlanId");
                String vlanName = resSet.getString("vlanName");
                int providerId = resSet.getInt("provider_id");
                int vlanContractId = resSet.getInt("vlanContract_id");
                Vlan vlan = new Vlan(vlanId, vlanName, providerId, vlanContractId);
                vlanList.add(vlan);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect();
        }
        return vlanList;
    }

    public List<VlanContract> getVlanContractList(DbConnection dbConnection, DeviceConnection conn,
                                                  PreparedStatement preState, ResultSet resSet){
        final String query = "SELECT*FROM vlan_contract";
        List<VlanContract> vlanContractList = new ArrayList<>();
        try {
            openForSelect(dbConnection, conn, preState, resSet, query);
            while (resSet.next()){
                int id = resSet.getInt("id");
                String vlanContNumber = resSet.getString("vlanContNumber");
                VlanContract vlanContract = new VlanContract(id, vlanContNumber);
                vlanContractList.add(vlanContract);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForSelect(conn, preState, resSet);
        }
        return vlanContractList;
    }

    public void insertInto (String query){
        try {
            openForUpdate(query);
            preState.setString(1,"Зеленоградск");
            preState.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            closeForUpdate();
        }
    }

    private void openForSelect(DbConnection dbConnection, DeviceConnection conn, PreparedStatement preState,
                               ResultSet resSet, String query){
        try{
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            resSet = preState.executeQuery();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private void openForUpdate(DbConnection dbConnection, PreparedStatement preState, ResultSet resSet, String query){
        try{
            DeviceConnection conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
        }catch (NullPointerException | SQLException e){
            e.printStackTrace();
        }
    }

    private void closeForSelect(DeviceConnection dbConnection, PreparedStatement preState, ResultSet resSet){
        try { dbConnection.close(); } catch (SQLException e) {e.printStackTrace();}
        try { preState.close(); } catch (SQLException e) {e.printStackTrace();}
        try { resSet.close(); } catch (SQLException e) {e.printStackTrace();}
    }

    private void closeForUpdate(DeviceConnection dbConnection, PreparedStatement preState, ResultSet resSet){
        try { dbConnection.close(); } catch (SQLException e) {e.printStackTrace();}
        try { preState.close(); } catch (SQLException e) {e.printStackTrace();}
    }*/
}
