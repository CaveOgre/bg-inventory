package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DeviceConnection {
    private final IntegerProperty id;
    private final StringProperty connType;

    public DeviceConnection(int id, String connType){
        this.id = new SimpleIntegerProperty(id);
        this.connType = new SimpleStringProperty(connType);
    }

    //Getters
    public int getId() {
        return id.get();
    }

    public String getConnType() {
        return connType.get();
    }


    //Setters
    public void setId(int value) {
        id.set(value);
    }

    public void setConnType(String value) {
        connType.set(value);
    }

    //Property value
    public IntegerProperty idProperty() {
        return id;
    }

    public StringProperty connTypeProperty() {
        return connType;
    }

    @Override
    public String toString() {
        return getConnType();
    }
}
