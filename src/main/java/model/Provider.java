package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Provider {
    private final IntegerProperty id;
    private final StringProperty providerName;

    public Provider(){
        this.id = null;
        this.providerName = null;
    }

    public Provider(int id, String providerName){
        this.id = new SimpleIntegerProperty(id);
        this.providerName = new SimpleStringProperty(providerName);
    }

    //Getters
    public int getId() {
        return id.get();
    }

    public String getProviderName() {
        return providerName.get();
    }


    //Setters
    public void setId(int value) {
        id.set(value);
    }

    public void setProviderName(String value) {
        providerName.set(value);
    }

    //Property value
    public IntegerProperty idProperty() {
        return id;
    }

    public StringProperty providerNameProperty() {
        return providerName;
    }

    @Override
    public String toString() {
        return getProviderName();
    }
}
