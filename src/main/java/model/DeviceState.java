package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DeviceState {
    private final IntegerProperty id;
    private final StringProperty state;

    public DeviceState(int id, String state){
        this.id = new SimpleIntegerProperty(id);
        this.state = new SimpleStringProperty(state);
    }

    //Getters
    public int getId() {
        return id.get();
    }

    public String getState() {
        return state.get();
    }

    //Setters
    public void setId(int value) {
        id.set(value);
    }

    public void setState(String value) {
        state.set(value);
    }

    //Property value
    public IntegerProperty idProperty() {
        return id;
    }

    public StringProperty stateProperty() {
        return state;
    }

    @Override
    public String toString() {
        return getState();
    }
}
