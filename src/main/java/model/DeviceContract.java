package model;

import dbpackage.DbConnection;

import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DeviceContract {
    private final IntegerProperty id;
    private final StringProperty contractNumber;
    private final IntegerProperty deviceSupplyId;
    private final IntegerProperty garant;

    public DeviceContract(int id, String contractNumber, int deviceSupplyId, byte garant){
        this.id = new SimpleIntegerProperty(id);
        this.contractNumber = new SimpleStringProperty(contractNumber);
        this.deviceSupplyId = new SimpleIntegerProperty(deviceSupplyId);
        this.garant = new SimpleIntegerProperty(garant);
    }

    //Getters
    public int getId() {
        return id.get();
    }

    public String getContractNumber() {
        return contractNumber.get();
    }

    public int getDeviceSupplyId() {
        return deviceSupplyId.get();
    }

    public byte getGarant() {
        return (byte)garant.get();
    }


    //Setters
    public void setId(int value) {
        id.set(value);
    }

    public void setContractNumber(String value) {
        contractNumber.set(value);
    }

    public void setDeviceSupplyId(int value) {
        deviceSupplyId.set(value);
    }

    public void setGarant(byte value) {
        garant.set(value);
    }

    //Property value

    public IntegerProperty idProperty() {
        return id;
    }

    public StringProperty contractNumberProperty() {
        return contractNumber;
    }

    public IntegerProperty deviceSupplyIdProperty() {
        return deviceSupplyId;
    }

    public IntegerProperty garantProperty() {
        return garant;
    }

    @Override
    public String toString() {
        return getContractInfo() + " ";
    }

    private String getContractInfo(){
        String contractInfo = "";
        DbConnection dbConnection = new DbConnection();
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT device_supply.deviceSupName as deviceSupName, contractNumber " +
                "FROM device_contract\n" +
                "LEFT JOIN device_supply ON device_contract.deviceSupply_id = device_supply.id\n" +
                "WHERE device_contract.id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, getId());
            resSet = preState.executeQuery();
            while (resSet.next()) {
                contractInfo = resSet.getString("deviceSupName") + ", " +
                        resSet.getString("contractNumber");
            }
        } catch (SQLException e){
            dbConnection.setLog("Ошибка при поиске информации о номере контракта на утсройства", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return contractInfo;
    }
}
