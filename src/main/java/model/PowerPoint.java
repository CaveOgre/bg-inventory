package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PowerPoint {
    private final IntegerProperty id;
    private final StringProperty pointName;
    private final IntegerProperty powerSupplyId;

    public PowerPoint(int id, String pointName, int powerSupplyId){
        this.id = new SimpleIntegerProperty(id);
        this.pointName = new SimpleStringProperty(pointName);
        this.powerSupplyId = new SimpleIntegerProperty(powerSupplyId);
    }

    //Getters
    public int getId() {
        return id.get();
    }

    public String getPointName() {
        return pointName.get();
    }

    public int getPowerSupplyId() {
        return powerSupplyId.get();
    }

    //Setters
    public void setId(int value) {
        id.set(value);
    }

    public void setPointName(String value) {
        pointName.set(value);
    }

    public void setPowerSupplyId(int value) {
        powerSupplyId.set(value);
    }

    //Property value
    public IntegerProperty idProperty() {
        return id;
    }

    public StringProperty pointNameProperty() {
        return pointName;
    }

    public IntegerProperty powerSupplyIdProperty() {
        return powerSupplyId;
    }

    @Override
    public String toString() {
        return getPointName();
    }
}
