package model;

import dbpackage.DbConnection;
import javafx.beans.property.*;

import java.sql.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Device {
    private final IntegerProperty id;
    private final IntegerProperty areaId;
    private final IntegerProperty groupDeviceId;
    private final IntegerProperty deviceTypeId;
    private final IntegerProperty cameraTypeId;
    private final IntegerProperty modelId;
    private final StringProperty cameraNum;
    private final IntegerProperty serverId;
    private final IntegerProperty ipAddress;
    private final IntegerProperty connectionId;
    private final IntegerProperty powerId;
    private final StringProperty attachment;
    private final IntegerProperty deviceContractId;
    private final IntegerProperty deviceStateId;
    private final BooleanProperty issConn;
    private final StringProperty login;
    private final StringProperty password;
    private final StringProperty inventar;
    private final StringProperty serial;
    private final StringProperty describe;

    public Device(int id, int areaId, int groupDeviceId, int deviceTypeId, int cameraTypeId, int modelId,
                  String cameraNum, int serverId, int ipAddress, int connectionId, int powerId, String attachment,
                  int deviceContractId, int deviceStateId, boolean issConn, String login, String password,
                  String inventar, String serial, String describe) {
        this.id = new SimpleIntegerProperty(id);
        this.areaId = new SimpleIntegerProperty(areaId);
        this.groupDeviceId = new SimpleIntegerProperty(groupDeviceId);
        this.deviceTypeId = new SimpleIntegerProperty(deviceTypeId);
        this.cameraTypeId = new SimpleIntegerProperty(cameraTypeId);
        this.modelId = new SimpleIntegerProperty(modelId);
        this.cameraNum = new SimpleStringProperty(cameraNum);
        this.serverId = new SimpleIntegerProperty(serverId);
        this.ipAddress = new SimpleIntegerProperty(ipAddress);
        this.connectionId = new SimpleIntegerProperty(connectionId);
        this.powerId = new SimpleIntegerProperty(powerId);
        this.attachment = new SimpleStringProperty(attachment);
        this.deviceContractId = new SimpleIntegerProperty(deviceContractId);
        this.deviceStateId = new SimpleIntegerProperty(deviceStateId);
        this.issConn = new SimpleBooleanProperty(issConn);
        this.login = new SimpleStringProperty(login);
        this.password = new SimpleStringProperty(password);
        this.inventar = new SimpleStringProperty(inventar);
        this.serial = new SimpleStringProperty(serial);
        this.describe = new SimpleStringProperty(describe);
    }


    //Getters
    public int getId() {
        return id.get();
    }

    public int getAreaId() {
        return areaId.get();
    }

    public int getGroupDeviceId() {
        return groupDeviceId.get();
    }

    public int getDeviceTypeId() {
        return deviceTypeId.get();
    }

    public int getCameraTypeId() {
        return cameraTypeId.get();
    }

    public int getModelId() {
        return modelId.get();
    }

    public String getCameraNum() {
        return cameraNum.get();
    }

    public int getServerId() {
        return serverId.get();
    }

    public int getIpAddress() {
        return ipAddress.get();
    }

    public int getConnectionId() {
        return connectionId.get();
    }

    public int getPowerId() {
        return powerId.get();
    }

    public String getAttachment() {
        return attachment.get();
    }

    public int getDeviceContractId() {
        return deviceContractId.get();
    }

    public int getDeviceStateId() {
        return deviceStateId.get();
    }

    public boolean getIssConn() {
        return issConn.get();
    }

    public String getLogin() {
        return login.get();
    }

    public String getPassword() {
        return password.get();
    }

    public String getInventar() {
        return inventar.get();
    }

    public String getSerial() {
        return serial.get();
    }

    public String getDescribe() {
        return describe.get();
    }

    //Setters
    public void setId(int value) {
        id.set(value);
    }

    public void setAreaId(int value) {
        areaId.set(value);
    }

    public void setGroupDeviceId(int value) {
        groupDeviceId.set(value);
    }

    public void setDeviceTypeId(int value) {
        deviceTypeId.set(value);
    }

    public void setCameraTypeId(int value) {
        cameraTypeId.set(value);
    }

    public void setModelId(int value) {
        modelId.set(value);
    }

    public void setCameraNum(String value) {
        cameraNum.set(value);
    }

    public void setServerId(int value) {
        serverId.set(value);
    }

    public void setIpAddress(int value) {
        ipAddress.set(value);
    }

    public void setConnectionId(int value) {
        connectionId.set(value);
    }

    public void setPowerId(int value) {
        powerId.set(value);
    }

    public void setAttachment(String value) {
        attachment.set(value);
    }

    public void setDeviceContractId(int value) {
        deviceContractId.set(value);
    }

    public void setDeviceStateId(int value) {
        deviceStateId.set(value);
    }

    public void setIssConn(boolean value) {
        issConn.set(value);
    }

    public void setLogin(String value) {
        login.set(value);
    }

    public void setPassword(String value) {
        password.set(value);
    }

    public void setInventar(String value) {
        inventar.set(value);
    }

    public void setSerial(String value) { serial.set(value);}

    public void setDescribe(String value) {
        describe.set(value);
    }

    //Property value

    public IntegerProperty idProperty() {
        return id;
    }

    public IntegerProperty areaIdProperty() {
        return areaId;
    }

    public IntegerProperty groupDeviceIdProperty() {
        return groupDeviceId;
    }

    public IntegerProperty deviceTypeIdProperty() {
        return deviceTypeId;
    }

    public IntegerProperty cameraTypeIdProperty() {
        return cameraTypeId;
    }

    public IntegerProperty modelIdProperty() {
        return modelId;
    }

    public StringProperty cameraNumProperty() {
        return cameraNum;
    }

    public IntegerProperty serverIdProperty() {
        return serverId;
    }

    public IntegerProperty ipAddressProperty() {
        return ipAddress;
    }

    public IntegerProperty connectionIdProperty() {
        return connectionId;
    }

    public IntegerProperty powerIdProperty() {
        return powerId;
    }

    public StringProperty attachmentProperty() {
        return attachment;
    }

    public IntegerProperty deviceContractIdProperty() {
        return deviceContractId;
    }

    public IntegerProperty deviceStateIdProperty() {
        return deviceStateId;
    }

    public BooleanProperty issConnProperty() {
        return issConn;
    }

    public StringProperty loginProperty() {
        return login;
    }

    public StringProperty passwordProperty() {
        return password;
    }

    public StringProperty inventarProperty() {
        return inventar;
    }

    public StringProperty serialProperty() {
        return serial;
    }

    public StringProperty describeProperty() {
        return describe;
    }

    @Override
    public String toString() {
        return getDeviceViewName();
    }

    private String getDeviceViewName() {
        DbConnection dbConnection = new DbConnection();
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String deviceViewName = "";
        String query = "SELECT groupName, vendorName, modelName, (inet_ntoa(ipAddress)) as ipAddress, cameraNum FROM device\n" +
                "LEFT JOIN device_group ON device.groupDevice_id = device_group.id\n" +
                "LEFT JOIN model ON device.model_id = model.id\n" +
                "LEFT JOIN vendor ON model.vendor_id = vendor.id\n" +
                "WHERE device.id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, getId());
            resSet = preState.executeQuery();
            while (resSet.next()) {
                String camNum = resSet.getString("cameraNum");
                if (camNum != null && !camNum.isEmpty()) {
                    deviceViewName = resSet.getString("cameraNum").concat(", ");
                }
                deviceViewName += (resSet.getString("groupName") + " " +
                        resSet.getString("vendorName") + " " +
                        resSet.getString("modelName") + ", IP: " +
                        resSet.getString("ipAddress"));
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при загрузке имени устройства", e);
        }
        finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return deviceViewName;
    }
}
