package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Vlan {
    private final IntegerProperty vlanId;
    private final StringProperty vlanName;
    private final IntegerProperty vlanContractId;

    public Vlan(){
        this.vlanId = null;
        this.vlanName = null;
        this.vlanContractId = null;
    }

    public Vlan(short vlanId, String vlanName, int vlanContractId){
        this.vlanId = new SimpleIntegerProperty(vlanId);
        this.vlanName = new SimpleStringProperty(vlanName);
        this.vlanContractId = new SimpleIntegerProperty(vlanContractId);
    }

    //Getters
    public short getVlanId() {
        return (short)vlanId.get();
    }

    public String getVlanName() {
        return vlanName.get();
    }


    public int getVlanContractId() {
        return vlanContractId.get();
    }

    //Setters
    public void setVlanId(short value) {
        vlanId.set(value);
    }
    public void setVlanName(String value) {
        vlanName.set(value);
    }

    public void setVlanContractId(int value) {
        vlanContractId.set(value);
    }

    //Property value
    public StringProperty vlanNameProperty() {
        return vlanName;
    }

    public IntegerProperty vlanContractIdProperty() {
        return vlanContractId;
    }

    @Override
    public String toString() {
        return String.valueOf(getVlanId());
    }
}
