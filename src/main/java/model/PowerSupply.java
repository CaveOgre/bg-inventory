package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PowerSupply {
    private final IntegerProperty id;
    private final StringProperty powerSuppName;
    private final StringProperty phone;

    public PowerSupply(int id, String powerSuppName, String phone){
        this.id = new SimpleIntegerProperty(id);
        this.powerSuppName = new SimpleStringProperty(powerSuppName);
        this.phone = new SimpleStringProperty(phone);
    }

    //Getters
    public int getId() {
        return id.get();
    }

    public String getPowerSuppName() {
        return powerSuppName.get();
    }

    public String getPhone() {
        return phone.get();
    }

    //Setters
    public void setId(int value) {
        id.set(value);
    }
    public void setPowerSuppName(String value) {
        powerSuppName.set(value);
    }
    public void setPhone(String value) {
        phone.set(value);
    }

    //Property value
    public IntegerProperty idProperty() {
        return id;
    }

    public StringProperty powerSuppNameProperty() {
        return powerSuppName;
    }

    public StringProperty phoneProperty() {
        return phone;
    }

    @Override
    public String toString() {
        return getPowerSuppName();
    }
}
