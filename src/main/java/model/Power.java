package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Power {
    private final IntegerProperty id;
    private final StringProperty powerName;

    public Power(int id, String powerName){
        this.id = new SimpleIntegerProperty(id);
        this.powerName = new SimpleStringProperty(powerName);
    }

    //Getters
    public int getId() {
        return id.get();
    }

    public String getPowerName() {
        return powerName.get();
    }

    //Setters
    public void setId(int value) {
        id.set(value);
    }

    public void setPowerName(String value) {
        powerName.set(value);
    }

    //Property value
    public IntegerProperty idProperty() {
        return id;
    }

    public StringProperty powerNameProperty() {
        return powerName;
    }

    @Override
    public String toString() {
        return getPowerName();
    }
}
