package model;

import dbpackage.DbConnection;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class VlanContract {


    private final IntegerProperty id;
    private final IntegerProperty providerId;
    private final StringProperty vlanContNumber;

    public VlanContract(int id, int providerId, String vlanContNumber) {
        this.id = new SimpleIntegerProperty(id);
        this.providerId = new SimpleIntegerProperty(providerId);
        this.vlanContNumber = new SimpleStringProperty(vlanContNumber);
    }

    //Getters
    public int getId() {
        return id.get();
    }

    public int getProviderId() {
        return providerId.get();
    }

    public String getVlanContNumber() {
        return vlanContNumber.get();
    }

    //Setters
    public void setId(int value) {
        id.set(value);
    }

    public void setProviderId(int value) {
        providerId.set(value);
    }

    public void setVlanContNumber(String value) {
        vlanContNumber.set(value);
    }

    //Property value

    public IntegerProperty idProperty() {
        return id;
    }

    public IntegerProperty providerIdProperty() {
        return providerId;
    }

    public StringProperty vlanContNumberProperty() {
        return vlanContNumber;
    }

    @Override
    public String toString() {
        return getVlanContNumber() + ", " + getProviderName();
    }

    private String getProviderName() {
        DbConnection dbConnection = new DbConnection();
        String providerName = "";
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT providerName FROM provider WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, getProviderId());
            resSet = preState.executeQuery();
            while (resSet.next()) {
                providerName = resSet.getString("providerName");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске имени провайдера", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return providerName;
    }
}
