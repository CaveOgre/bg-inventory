package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DeviceType {
    private final IntegerProperty id;
    private final StringProperty deviceType;

    public DeviceType(int id, String deviceType){
        this.id = new SimpleIntegerProperty(id);
        this.deviceType = new SimpleStringProperty(deviceType);
    }

    //Getters
    public int getId() {
        return id.get();
    }

    public String getDeviceType() {
        return deviceType.get();
    }

    //Setters
    public void setId(int value) {
        id.set(value);
    }

    public void setDeviceType(String value) {
        deviceType.set(value);
    }

    //Property value
    public IntegerProperty idProperty() {
        return id;
    }

    public StringProperty deviceTypeProperty() {
        return deviceType;
    }

    @Override
    public String toString() {
        return getDeviceType();
    }
}
