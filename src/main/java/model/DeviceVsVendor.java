package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DeviceVsVendor {

    private final IntegerProperty id;
    private final IntegerProperty deviceGroupId;
    private final IntegerProperty deviceVendorId;

    public DeviceVsVendor(int id, int deviceGroupId, int deviceVendorId) {
        this.id = new SimpleIntegerProperty(id);
        this.deviceGroupId = new SimpleIntegerProperty(deviceGroupId);
        this.deviceVendorId = new SimpleIntegerProperty(deviceVendorId);
    }

    //Getters
    public int getId() {
        return id.get();
    }

    public int getDeviceGroupId() {
        return deviceGroupId.get();
    }

    public int getDeviceVendorId() {
        return deviceVendorId.get();
    }

    //Setters
    public void setId(int value) {
        id.set(value);
    }

    public void setDeviceGroupId(int value) {
        deviceGroupId.set(value);
    }

    public void setDeviceVendorId(int value) {
        deviceVendorId.set(value);
    }

    //Property value

    public IntegerProperty idProperty() {
        return id;
    }

    public IntegerProperty deviceGroupId() {
        return deviceGroupId;
    }

    public IntegerProperty deviceVendorId() {
        return deviceVendorId;
    }
}


