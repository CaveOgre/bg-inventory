package model;

import dbpackage.DbConnection;

import javafx.beans.property.StringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleIntegerProperty;

import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class Model {
    private final IntegerProperty id;
    private final IntegerProperty vendorId;
    private final StringProperty modelName;

    public Model(int id, int vendorId, String modelName) {
        this.id = new SimpleIntegerProperty(id);
        this.vendorId = new SimpleIntegerProperty(vendorId);
        this.modelName = new SimpleStringProperty(modelName);
    }

    //Getters
    public int getId() {
        return id.get();
    }

    public int getVendorId() {
        return vendorId.get();
    }

    public String getModelName() {
        return modelName.get();
    }

    //Setters
    public void setId(int value) {
        id.set(value);
    }

    public void setVendorId(int value) {
        vendorId.set(value);
    }

    public void setModelName(String value) {
        modelName.set(value);
    }

    //Property value
    public IntegerProperty idProperty() {
        return id;
    }

    public IntegerProperty vendorIdProperty() {
        return vendorId;
    }

    public StringProperty modelNameProperty() {
        return modelName;
    }

    @Override
    public String toString() {
        return getVendorName() + " " + getModelName();
    }

    private String getVendorName() {
        DbConnection dbConnection = new DbConnection();
        String vendorName = "";
        Connection conn = null;
        PreparedStatement preState = null;
        ResultSet resSet = null;
        String query = "SELECT*FROM vendor WHERE id = ?";
        try {
            conn = dbConnection.getDbConnection();
            preState = conn.prepareStatement(query);
            preState.setInt(1, getVendorId());
            resSet = preState.executeQuery();
            while (resSet.next()) {
                vendorName = resSet.getString("vendorName");
            }
        } catch (SQLException e) {
            dbConnection.setLog("Ошибка при поиске имени производителя", e);
        } finally {
            dbConnection.closeConnection(conn, preState, resSet);
        }
        return vendorName;
    }
}
