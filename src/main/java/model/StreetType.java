package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class StreetType {
    private final IntegerProperty id;
    private final StringProperty streetType;

    public StreetType(int id, String modelName) {
        this.id = new SimpleIntegerProperty(id);
        this.streetType = new SimpleStringProperty(modelName);
    }

    //Getters
    public int getId() {
        return id.get();
    }

    public String getStreetType() {
        return streetType.get();
    }

    //Setters
    public void setId(int value) {
        id.set(value);
    }

    public void setStreetType(String value) {
        streetType.set(value);
    }

    //Property value
    public IntegerProperty idProperty() {
        return id;
    }

    public StringProperty streetTypeProperty() {
        return streetType;
    }

    @Override
    public String toString() {
        return getStreetType();
    }
}
