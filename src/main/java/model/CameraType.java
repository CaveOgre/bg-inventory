package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CameraType {
    private final IntegerProperty id;
    private final StringProperty cameraType;

    public CameraType(int id, String cameraType){
        this.id = new SimpleIntegerProperty(id);
        this.cameraType = new SimpleStringProperty(cameraType);
    }

    //Getters
    public int getId() {
        return id.get();
    }

    public String getCameraType() {
        return cameraType.get();
    }

    //Setters
    public void setId(int value) {
        id.set(value);
    }

    public void setCameraType(String value) {
        cameraType.set(value);
    }

    //Proprety value
    public IntegerProperty idProperty() {
        return id;
    }

    public StringProperty cameraTypeProperty() {
        return cameraType;
    }

    @Override
    public String toString() {
        return getCameraType();
    }
}
