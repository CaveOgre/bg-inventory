package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DeviceGroup {
    private final IntegerProperty id;
    private final StringProperty groupName;

    public DeviceGroup(int id, String groupName){
        this.id = new SimpleIntegerProperty(id);
        this.groupName = new SimpleStringProperty(groupName);
    }

    //Getters
    public int getId() {
        return id.get();
    }

    public String getGroupName() {
        return groupName.get();
    }


    //Setters
    public void setId(int value) {
        id.set(value);
    }

    public void setGroupName(String value) {
        groupName.set(value);
    }

    //Property value
    public IntegerProperty idProperty() {
        return id;
    }

    public StringProperty groupNameProperty() {
        return groupName;
    }

    @Override
    public String toString() {
        return getGroupName();
    }
}
