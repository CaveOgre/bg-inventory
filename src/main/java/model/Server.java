package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Server {
    private final IntegerProperty id;
    private final StringProperty serverName;

    public Server(int id, String serverName){
        this.id = new SimpleIntegerProperty(id);
        this.serverName = new SimpleStringProperty(serverName);
    }

    //Getters
    public int getId() {
        return id.get();
    }

    public String getServerName() {
        return serverName.get();
    }

    //Setters
    public void setId(int value) {
        id.set(value);
    }

    public void setServerName(String value) {
        serverName.set(value);
    }

    //Property value
    public IntegerProperty idProperty() {
        return id;
    }

    public StringProperty serverNameProperty() {
        return serverName;
    }

    @Override
    public String toString() {
        return getServerName();
    }
}
