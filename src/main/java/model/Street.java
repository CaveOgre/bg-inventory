package model;

import dbpackage.DbConnection;

import javafx.beans.property.StringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleIntegerProperty;

import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class Street {
    private final IntegerProperty id;
    private final IntegerProperty cityId;
    private final StringProperty streetName;

    public Street(int id, int cityId, String streetName) {
        this.id = new SimpleIntegerProperty(id);
        this.cityId = new SimpleIntegerProperty(cityId);
        this.streetName = new SimpleStringProperty(streetName);
    }

    //Getters
    public int getId() {
        return id.get();
    }

    public int getCityId() {
        return cityId.get();
    }

    public String getStreetName() {
        return streetName.get();
    }

    //Setters
    public void setId(int value) {
        id.set(value);
    }

    public void setCityId(int value) {
        cityId.set(value);
    }

    public void setStreetName(String value) {
        streetName.set(value);
    }

    //Property value

    public IntegerProperty idProperty() {
        return id;
    }

    public IntegerProperty cityIdProperty() {
        return cityId;
    }

    public StringProperty streetNameProperty() {
        return streetName;
    }

    @Override
    public String toString() {
        return getStreetName();
    }
}
