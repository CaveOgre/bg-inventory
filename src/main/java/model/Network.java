package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Network {
    private final IntegerProperty id;
    private final IntegerProperty areaId;
    private final StringProperty subNetwork;
    private final IntegerProperty mask;
    private final StringProperty gateway;
    private final IntegerProperty vlanId;

    public Network(int id, int areaId, String subNetwork, byte mask, String gateway, short vlanId){
        this.id = new SimpleIntegerProperty(id);
        this.areaId = new SimpleIntegerProperty(areaId);
        this.subNetwork = new SimpleStringProperty(subNetwork);
        this.mask = new SimpleIntegerProperty(mask);
        this.gateway = new SimpleStringProperty(gateway);
        this.vlanId = new SimpleIntegerProperty(vlanId);
    }

    //Getters
    public int getId() {
        return id.get();
    }

    public int getAreaId() { return areaId.get(); }

    public String getSubNetwork() {
        return subNetwork.get();
    }

    public byte getMask() {
        return (byte)mask.get();
    }

    public String getGateway() {
        return gateway.get();
    }

    public short getVlanId() {
        return (short)vlanId.get();
    }

    //Setters
    public void setId(int value) {
        id.set(value);
    }

    public void setAreaId(int value) { areaId.set(value);}

    public void setSubNetwork(String value) {
        subNetwork.set(value);
    }

    public void setMask(byte value) {
        mask.set(value);
    }

    public void setGateway(String value) { gateway.set(value);}

    public void setVlanId(short value) {
        vlanId.set(value);
    }

    //Property value
    public IntegerProperty idProperty() {
        return id;
    }

    public IntegerProperty areaIdProperty() { return areaId; }

    public StringProperty subNetworkProperty() {
        return subNetwork;
    }

    public IntegerProperty maskProperty() {
        return mask;
    }

    public StringProperty gatewayProperty() {
        return gateway;
    }

    public IntegerProperty vlanIdProperty() {
        return vlanId;
    }

    @Override
    public String toString() {
        return subNetwork.getValue();
    }
}
