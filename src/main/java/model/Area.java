package model;

import javafx.beans.property.*;

public class Area {
    private final IntegerProperty id;
    private final StringProperty areaName;
    private final IntegerProperty cityId;
    private final IntegerProperty fStreetId;
    private final StringProperty fStreetBuilding;
    private final BooleanProperty crossroad;
    private final IntegerProperty sStreetId;
    private final StringProperty sStreetBuilding;
    private final IntegerProperty powerPointId;
    private final IntegerProperty bgStage;
    private final IntegerProperty year;
    private final IntegerProperty areaStateId;

    public Area(int id, String areaName, int cityId, int fStreetId, String fStreetBuilding,
                boolean crossroad, int sStreetId, String sStreetBuilding, int powerPointId,
                int bgStage, short year, int areaStateId) {
        this.id = new SimpleIntegerProperty(id);
        this.areaName = new SimpleStringProperty(areaName);
        this.cityId = new SimpleIntegerProperty(cityId);
        this.fStreetId = new SimpleIntegerProperty(fStreetId);
        this.fStreetBuilding = new SimpleStringProperty(fStreetBuilding);
        this.crossroad = new SimpleBooleanProperty(crossroad);
        this.sStreetId = new SimpleIntegerProperty(sStreetId);
        this.sStreetBuilding = new SimpleStringProperty(sStreetBuilding);
        this.powerPointId = new SimpleIntegerProperty(powerPointId);
        this.bgStage = new SimpleIntegerProperty(bgStage);
        this.year = new SimpleIntegerProperty(year);
        this.areaStateId = new SimpleIntegerProperty(areaStateId);
    }

    //Getter
    public int getId() {
        return id.get();
    }

    public String getAreaName() {
        return areaName.get();
    }

    public int getCityId() {
        return cityId.get();
    }

    public int getFStreetId() {
        return fStreetId.get();
    }

    public String getFStreetBuilding() {
        return fStreetBuilding.get();
    }

    public boolean getCrossroad() {
        return crossroad.get();
    }

    public int getSStreetId() {
        return sStreetId.get();
    }

    public String getSStreetBuilding() {
        return sStreetBuilding.get();
    }

    public int getBgStage() {
        return bgStage.get();
    }

    public int getPowerPointId() {
        return powerPointId.get();
    }

    public short getYear() {
        return (short) year.get();
    }

    public int getAreaStateId() {
        return areaStateId.get();
    }

    //Setter
    public void setId(int value) {
        id.set(value);
    }

    public void setAreaName(String value) {
        areaName.set(value);
    }

    public void setAreaStateId(int value) {
        areaStateId.set(value);
    }

    public void setCityId(int value) {
        cityId.set(value);
    }

    public void setFStreetId(int value) {
        fStreetId.set(value);
    }

    public void setFStreetBuilding(String value) {fStreetBuilding.set(value);}

    public void setCrossroad(boolean value) {
        crossroad.set(value);
    }

    public void setSStreetId(int value) { sStreetId.set(value);
    }

    public void setSStreetBuilding(String value) {sStreetBuilding.set(value);}

    public void setPowerPointId(int value) {
        powerPointId.set(value);
    }

    public void setBgStage(int value) {
        bgStage.set(value);
    }

    public void setYear(short value) {
        year.setValue(value);
    }


    //Property Value

    public IntegerProperty idProperty() {
        return id;
    }

    public StringProperty areaNameProperty() {
        return areaName;
    }

    public IntegerProperty cityIdProperty() {
        return cityId;
    }

    public IntegerProperty fStreetIdProperty() {
        return fStreetId;
    }

    public StringProperty fStreetBuildingProperty() {
        return fStreetBuilding;
    }

    public BooleanProperty crossroadProperty() {
        return crossroad;
    }

    public IntegerProperty sStreetIdProperty() {
        return sStreetId;
    }

    public StringProperty sStreetBuildingProperty() {
        return sStreetBuilding;
    }

    public IntegerProperty powerPointIdProperty() {
        return powerPointId;
    }

    public IntegerProperty areaStateIdProperty() {
        return areaStateId;
    }

    public IntegerProperty bgStageProperty() {
        return bgStage;
    }

    public IntegerProperty yearProperty() {
        return year;
    }

    //Represent area info at view
    @Override
    public String toString() {
        return getAreaName();
    }
}
