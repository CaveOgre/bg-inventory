package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Vendor {
    private final IntegerProperty id;
    private final StringProperty vendorName;

    public Vendor(int id, String vendorName){
        this.id = new SimpleIntegerProperty(id);
        this.vendorName = new SimpleStringProperty(vendorName);
    }

    //Getters
    public int getId() {
        return id.get();
    }

    public String getVendorName() {
        return vendorName.get();
    }

    //Setters
    public void setId(int value) {
        id.set(value);
    }

    public void setVendorName(String value) {
        vendorName.set(value);
    }

    //Property value

    public IntegerProperty idProperty() {
        return id;
    }

    public StringProperty vendorNameProperty() {
        return vendorName;
    }

    @Override
    public String toString() {
        return getVendorName();
    }
}
