package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class City {
    private final IntegerProperty id;
    private final StringProperty cityName;

    public City(int id, String cityName){
        this.id = new SimpleIntegerProperty(id);
        this.cityName = new SimpleStringProperty(cityName);
    }

    //Getter
    public int getId() {
        return id.get();
    }

    public String getCityName() {
        return cityName.get();
    }

    //Setter
    public void setId(int value) {
        id.set(value);
    }

    public void setCityName(String value) {
        cityName.set(value);
    }

    //Property Value
    public IntegerProperty idProperty() {
        return id;
    }

    public StringProperty cityNameProperty() {
        return cityName;
    }

    @Override
    public String toString() {
        return getCityName();
    }
}
