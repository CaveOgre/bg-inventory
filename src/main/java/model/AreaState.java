package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;

public class AreaState {
    private final IntegerProperty id;
    private final StringProperty areaState;

    public AreaState(int id, String areaState) {
        this.id = new SimpleIntegerProperty(id);
        this.areaState = new SimpleStringProperty(areaState);
    }

    //Getter
    public int getId() {
        return id.get();
    }

    public String getAreaState() {
        return areaState.get();
    }

    //Setters
    public void setId(int value) {
        id.set(value);
    }

    public void setAreaState(String value) {
        areaState.set(value);
    }

    //Property value
    public IntegerProperty idProperty() {
        return id;
    }

    public StringProperty areaStateProperty() {
        return areaState;
    }

    @Override
    public String toString() {
        return getAreaState();
    }
}
