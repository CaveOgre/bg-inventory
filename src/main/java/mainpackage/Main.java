package mainpackage;

import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.Modality;
import dbpackage.DbConnection;
import javafx.fxml.FXMLLoader;
import javafx.application.Application;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.AnchorPane;
import view.DeviceTreeController;
import view.cameratype.CameraTypeController;
import view.city.CityController;
import view.connection.ConnectionController;
import view.devicecontract.DeviceContractController;
import view.devicegroup.DeviceGroupController;
import view.devicestate.DeviceStateController;
import view.devicesupply.DeviceSupplyController;
import view.devicetype.DeviceTypeController;
import view.model.ModelController;
import view.network.NetworkController;
import view.power.PowerController;
import view.power_point.PowerPointController;
import view.provider.ProviderController;
import view.server.ServerController;
import view.street.StreetController;
import view.vendor.VendorController;
import view.vlan.VlanController;
import view.vlancontract.VlanContractController;

import java.net.URL;
import java.io.IOException;

//TODO 1: Area addition to add net if do not exist

public class Main extends Application {

    public static DeviceTreeController deviceTreeController;
    private Stage primaryStage;
    private BorderPane rootLayout;
    private static DbConnection dbConnection = new DbConnection();

    @Override
    public void start(Stage primaryStage){

        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("БГ таблица");

        initMainLayout();
        showDeviceTree();
    }

    private void initMainLayout() {
        try {
            FXMLLoader mainLoader = new FXMLLoader();
            mainLoader.setLocation(Main.class.getResource("/Main.fxml"));
            rootLayout = mainLoader.load();

            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showDeviceTree() {
        try {
            FXMLLoader treeDeviceLoader = new FXMLLoader();
            treeDeviceLoader.setLocation(Main.class.getResource("/DeviceTree.fxml"));
            AnchorPane treeDeviceView = treeDeviceLoader.load();

             deviceTreeController = treeDeviceLoader.getController();
            rootLayout.setCenter(treeDeviceView);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @FXML
    private void goToCameraTypeForm() {
        final String title = "Тип камер";
        final String view = "/CameraType.fxml";
        final String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        CameraTypeController.setStage(stage);
        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    private void goToCityForm() {
        final String title = "Города";
        final String view = "/City.fxml";
        final String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        CityController.setStage(stage);
        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    private void goToConnectionForm() {
        final String title = "Тип подключения";
        final String view = "/Connection.fxml";
        final String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        ConnectionController.setStage(stage);
        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    private void goToDeviceContractForm() {
        final String title = "Контракты на поставку устройств";
        final String view = "/DeviceContract.fxml";
        final String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        DeviceContractController.setStage(stage);
        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    private void goToDeviceGroupForm() {
        final String title = "Группа устройства";
        final String view = "/DeviceGroup.fxml";
        final String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        DeviceGroupController.setStage(stage);
        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    private void goToDeviceStateForm() {
        final String title = "Состояние устройства";
        final String view = "/DeviceState.fxml";
        final String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        DeviceStateController.setStage(stage);
        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    private void goToDeviceSupplyForm() {
        final String title = "Поставщики устройсв";
        final String view = "/DeviceSupply.fxml";
        final String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        DeviceSupplyController.setStage(stage);
        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    private void goToDeviceTypeForm() {
        final String title = "Тип устройств";
        final String view = "/DeviceType.fxml";
        final String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        DeviceTypeController.setStage(stage);
        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    private void goToModelForm() {
        final String title = "Модель устройств";
        final String view = "/Model.fxml";
        final String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        ModelController.setStage(stage);
        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    private void goToNetworkForm() {
        final String title = "Площадки";
        final String view = "/Network.fxml";
        final String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        NetworkController.setNetworkStage(stage);
        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    private void goToPowerForm() {
        final String title = "Тип питания";
        final String view = "/Power.fxml";
        final String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        PowerController.setStage(stage);
        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    private void goToPowerPointForm() {
        final String title = "Точки питания";
        final String view = "/PowerPoint.fxml";
        final String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        PowerPointController.setStage(stage);
        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    private void goToPowerSupplyForm() {
        final String title = "Точки питания";
        final String view = "/PowerSupply.fxml";
        final String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        PowerPointController.setStage(stage);
        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    private void goToProviderForm() {
        final String title = "Провайдеры";
        final String view = "/Provider.fxml";
        final String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        ProviderController.setStage(stage);
        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    private void goToServerForm() {
        final String title = "Сервера";
        final String view = "/Server.fxml";
        final String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        ServerController.setStage(stage);
        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    private void goToStreetForm() {
        final String title = "Улицы";
        final String view = "/Street.fxml";
        final String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        StreetController.setStage(stage);
        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    private void goToVendorForm() {
        final String title = "Производители устройств";
        final String view = "/Vendor.fxml";
        final String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        VendorController.setStage(stage);
        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    private void goToVlanForm() {
        final String title = "Vlan";
        final String view = "/Vlan.fxml";
        final String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        VlanController.setStage(stage);
        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    @FXML
    private void goToVlanContractForm() {
        final String title = "Конракты на Vlan";
        final String view = "/VlanContract.fxml";
        final String logText = "Ошибка при создании окна \"" + title + "\"";

        URL toView = Main.class.getResource(view);

        Stage stage = Main.loadStage(toView, title, logText);
        VlanContractController.setStage(stage);
        assert stage != null;
        stage.setResizable(false);
        stage.showAndWait();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static Stage loadStage(URL toView, String title, String errorText){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(toView);

            Stage stage = new Stage();
            stage.setTitle(title);
            stage.setScene(new Scene(fxmlLoader.load()));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);

            return stage;
        } catch (IOException e) {
            dbConnection.setLog(errorText, e);
        }
        return null;
    }
}


